<?php

namespace Database\Factories;

use App\Models\CARIReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class CARIReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CARIReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
