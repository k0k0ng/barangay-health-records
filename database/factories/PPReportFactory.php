<?php

namespace Database\Factories;

use App\Models\PPReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class PPReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PPReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
