<?php

namespace Database\Factories;

use App\Models\CDDReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class CDDReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CDDReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
