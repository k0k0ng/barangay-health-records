<?php

namespace Database\Factories;

use App\Models\MCHReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class MCHReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MCHReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
