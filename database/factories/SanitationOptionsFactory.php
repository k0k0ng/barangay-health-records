<?php

namespace Database\Factories;

use App\Models\SanitationOptions;
use Illuminate\Database\Eloquent\Factories\Factory;

class SanitationOptionsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SanitationOptions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
