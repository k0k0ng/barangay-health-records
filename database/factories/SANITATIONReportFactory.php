<?php

namespace Database\Factories;

use App\Models\SANITATIONReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class SANITATIONReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SANITATIONReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
