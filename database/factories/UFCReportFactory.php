<?php

namespace Database\Factories;

use App\Models\UFCReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class UFCReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UFCReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
