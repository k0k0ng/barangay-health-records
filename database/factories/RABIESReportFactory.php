<?php

namespace Database\Factories;

use App\Models\RABIESReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class RABIESReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RABIESReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
