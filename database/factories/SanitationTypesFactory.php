<?php

namespace Database\Factories;

use App\Models\SanitationTypes;
use Illuminate\Database\Eloquent\Factories\Factory;

class SanitationTypesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SanitationTypes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
