<?php

namespace Database\Factories;

use App\Models\TBSympReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class TBSympReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TBSympReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
