<?php

namespace Database\Factories;

use App\Models\GMSReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class GMSReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GMSReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
