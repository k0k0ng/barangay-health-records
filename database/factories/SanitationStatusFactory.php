<?php

namespace Database\Factories;

use App\Models\SanitationStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class SanitationStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SanitationStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
