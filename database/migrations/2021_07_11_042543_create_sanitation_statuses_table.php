<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanitationStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanitation_statuses', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('household_id');
            $table->integer('sanitation_opt_id')->nullable();
            $table->string('score', 100);
            $table->enum('flush_toilet', ['Yes', 'No']);
            $table->enum('closed_pit_pervy', ['Yes', 'No']);
            $table->enum('communal_toilet', ['Yes', 'No']);
            $table->enum('drop_overhung', ['Yes', 'No']);
            $table->enum('field_bodyOfWater', ['Yes', 'No']);
            $table->enum('community_water', ['Yes', 'No']);
            $table->enum('developed_spring', ['Yes', 'No']);
            $table->enum('protected_well', ['Yes', 'No']);
            $table->enum('truck_tanker_peddler', ['Yes', 'No']);
            $table->enum('bottled_water', ['Yes', 'No']);
            $table->enum('undeveloped_spring', ['Yes', 'No']);
            $table->enum('undeveloped_well', ['Yes', 'No']);
            $table->enum('rainwater', ['Yes', 'No']);
            $table->enum('river_stream_dam', ['Yes', 'No']);
            $table->enum('blind_drainage', ['Yes', 'No'])->default('No');
            $table->string('others', 191)->default(NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('sanitation_opt_id')->references('id')->on('sanitation_options')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('household_id')->references('id')->on('households')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanitation_statuses');
    }
}
