<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseholdMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('household_members', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('person_id');
            $table->integer('household_id');
            $table->string('birthplace', 191)->default(NULL)->nullable();
            $table->string('relationship', 191)->default('yes');
            $table->enum('family_head', ['Yes', 'No'])->default('No')->nullable();
            $table->string('philhealth_no', 191)->default(NULL)->nullable();
            $table->string('philhealth_expiration', 191)->default(NULL)->nullable();
            $table->string('health_status', 191)->default(NULL)->nullable();
            $table->string('fp_method', 191)->default(NULL)->nullable();
            $table->string('pregnant', 191)->default(NULL)->nullable();
            $table->string('nut_status', 191)->default(NULL)->nullable();
            $table->string('fic', 191)->default(NULL)->nullable();
            $table->string('training', 191)->default(NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('person_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('household_id')->references('id')->on('households')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('household_members');
    }
}
