<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_report', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('resident_id');
            $table->integer('age');
            $table->string('PlaceOfDelivery', 100);
            $table->string('attended_by', 100);
            $table->string('gender', 100);
            $table->string('fdg', 100);
            $table->decimal('weight', $precision = 8, $scale = 2);
            $table->date('date_of_pp');
            $table->date('vitamina');
            $table->date('dod');
            $table->string('F', 100);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resident_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_report');
    }
}
