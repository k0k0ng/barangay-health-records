<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFpReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fp_report', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('resident_id');
            $table->integer('age');
            $table->integer('num_child');
            $table->date('lmp');
            $table->string('client_type', 100);
            $table->string('method_accepted', 100);
            $table->longText('remarks');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resident_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fp_report');
    }
}
