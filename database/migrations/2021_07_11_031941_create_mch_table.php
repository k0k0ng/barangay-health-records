<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mch', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('resident_id');
            $table->integer('age');
            $table->string('g', 100);
            $table->string('p', 100);
            $table->string('rcode', 100);
            $table->string('level', 100);
            $table->string('range', 100);
            $table->date('lmp');
            $table->date('edc');
            $table->longText('remarks');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resident_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mch');
    }
}
