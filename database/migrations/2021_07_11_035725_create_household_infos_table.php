<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseholdInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('household_infos', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('brgy_id');
            $table->integer('brgy_chairman_id');
            $table->string('committee', 191)->nullable();
            $table->integer('midwife_ndp_assigned');
            $table->integer('purok_id');
            $table->date('date_profiled')->nullable();
            $table->integer('interviewed_by');
            $table->enum('nhts', ['Yes', 'No'])->default('Yes');
            $table->string('nhts_no', 191)->default(NULL)->nullable();
            $table->enum('ip', ['Yes', 'No'])->default('Yes');
            $table->enum('cct', ['Yes', 'No'])->default('Yes');
            $table->enum('non_nhts', ['Yes', 'No'])->default('Yes');
            $table->string('tribe', 191)->default(NULL)->nullable();
            $table->string('philhealth_no', 191)->default(NULL)->nullable();
            $table->string('ip_no', 191)->default(NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('brgy_id')->references('id')->on('barangay_info')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('brgy_chairman_id')->references('id')->on('barangay_workers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('midwife_ndp_assigned')->references('id')->on('barangay_workers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('purok_id')->references('id')->on('purok')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('interviewed_by')->references('id')->on('barangay_workers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('household_infos');
    }
}
