<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalColumnsToPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->decimal('height', $precision = 8, $scale = 2)->nullable();
            $table->decimal('weight', $precision = 8, $scale = 2)->nullable();
            $table->string('blood_type', 191)->nullable();
            $table->string('contact_number', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('street', 191)->nullable();
            $table->string('purok', 191)->nullable();
            $table->string('barangay', 191)->nullable();
            $table->string('city', 191)->nullable();
            $table->string('inname', 191)->nullable();
            $table->string('contact', 191)->nullable();
            $table->string('relationship', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn(['height', 'weight', 'blood_type', 'contact_number', 'email', 'created_at', 'updated_at', 'street', 'purok', 'barangay', 'city', 'inname', 'contact', 'relationship']);
        });
    }
}
