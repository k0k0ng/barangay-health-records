<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('households', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('district', 191);
            $table->string('province', 191);
            $table->integer('encoder');
            $table->integer('household_info_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('encoder')->references('id')->on('account')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('household_info_id')->references('id')->on('household_infos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('households');
    }
}
