<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanitationOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanitation_options', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('option', 191);
            $table->integer('sanitation_type_id');
            $table->foreign('sanitation_type_id')->references('id')->on('sanitation_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanitation_options');
    }
}
