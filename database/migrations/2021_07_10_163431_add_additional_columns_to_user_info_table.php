<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalColumnsToUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->decimal('height', $precision = 8, $scale = 2)->nullable();
            $table->decimal('weight', $precision = 8, $scale = 2)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('street', 191)->nullable();
            $table->string('inname', 191)->nullable();
            $table->string('contact', 191)->nullable();
            $table->string('relationship', 191)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn(['height', 'weight', 'email', 'created_at', 'updated_at', 'street', 'inname', 'contact', 'relationship']);
        });
    }
}
