<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMortalityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mortality', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('resident_id');
            $table->integer('age');
            $table->date('DOD');
            $table->string('COD', 255);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resident_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mortality');
    }
}
