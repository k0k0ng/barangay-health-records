<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRabiesReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rabies_report', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('resident_id');
            $table->integer('age');
            $table->date('date');
            $table->string('complaint_bite', 255);
            $table->longText('remarks');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resident_id')->references('id')->on('user_info')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rabies_report');
    }
}
