<?php

use App\Http\Controllers\Auth\ChangePasswordController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;

use App\Http\Controllers\Resident\ResidentsListController;
use App\Http\Controllers\Resident\ViewResidentProfileController;
use App\Http\Controllers\Resident\EditResidentProfileController;

use App\Http\Controllers\Records\BIPRecordController;
use App\Http\Controllers\Records\CariRecordController;
use App\Http\Controllers\Records\CDDRecordController;
use App\Http\Controllers\Records\EPIRecordController;
use App\Http\Controllers\Records\FPRecordController;
use App\Http\Controllers\Records\GMSRecordController;
use App\Http\Controllers\Records\WalkInRecordController;
use App\Http\Controllers\Records\MCHRecordController;
use App\Http\Controllers\Records\MortalityRecordController;
use App\Http\Controllers\Records\PPRecordController;
use App\Http\Controllers\Records\RabiesRecordController;
use App\Http\Controllers\Records\SanitationRecordController;
use App\Http\Controllers\Records\TBSympRecordController;
use App\Http\Controllers\Records\UFCRecordController;

use App\Http\Controllers\PrintingControllers\PrintResidentMedicalRecordController;
use App\Http\Controllers\PrintingControllers\PrintWalkInRecordController;
use App\Http\Controllers\PrintingControllers\PrintMCHRecordController;
use App\Http\Controllers\PrintingControllers\PrintPPRecordController;
use App\Http\Controllers\PrintingControllers\PrintEPIRecordController;
use App\Http\Controllers\PrintingControllers\PrintUFCRecordController;
use App\Http\Controllers\PrintingControllers\PrintFPRecordController;
use App\Http\Controllers\PrintingControllers\PrintCDDRecordController;
use App\Http\Controllers\PrintingControllers\PrintMortalityRecordController;
use App\Http\Controllers\PrintingControllers\PrintCARIRecordController;
use App\Http\Controllers\PrintingControllers\PrintGMSRecordController;
use App\Http\Controllers\PrintingControllers\PrintBIPRecordController;
use App\Http\Controllers\PrintingControllers\PrintTBSympRecordController;
use App\Http\Controllers\PrintingControllers\PrintRabiesRecordController;
use App\Http\Controllers\PrintingControllers\PrintSnitationRecordController;

use App\Http\Controllers\Household\HouseholdController;
use App\Http\Controllers\Household\HouseholdInfoController;
use App\Http\Controllers\Household\AppointHouseHeadController;
use App\Http\Controllers\Household\RemoveHouseMemberController;
use App\Http\Controllers\Household\AddHouseholdInfoController;
use App\Http\Controllers\Household\EditHouseholdController;
use App\Http\Controllers\Household\TrashHouseholdController;

use App\Http\Controllers\BarangayInfo\BarangayInfoController;
use App\Http\Controllers\BarangayInfo\AddNewWorkerController;
use App\Http\Controllers\BarangayInfo\EditBarangayWorkerController;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Household\AddHouseholdMemberController;
use App\Http\Controllers\PrintingControllers\PrintHouseholdInfoController;
use App\Http\Controllers\ReportsController;

use App\Http\Controllers\ViewUserProfileController;

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
Route::get('/', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');

Route::post('/profile', [ViewUserProfileController::class, 'index'])->name('viewProfile')->middleware('auth');

Route::get('/residents-list', [ResidentsListController::class, 'resident_list'])->name('resident_list')->middleware('auth');
Route::post('/residents-list', [ResidentsListController::class, 'searchResidentProfile'])->name('searchResidentProfile');
Route::get('/resident-profile', [ViewResidentProfileController::class, 'ViewResidentProfile'])->name('ViewResidentProfile')->middleware('auth');

Route::post('/resident-profile/edit', [EditResidentProfileController::class, 'edit'])->name('editResident')->middleware('auth');
Route::post('/resident-profile/change-password', [ChangePasswordController::class, 'index'])->name('changePassword')->middleware('auth');
Route::get('/resident-profile/add-walk-in', [WalkInRecordController::class, 'addRecord'])->name('addWalkIn')->middleware('auth');
Route::post('/resident-profile/edit-walk-in', [WalkInRecordController::class, 'editRecord'])->name('editWalkIn')->middleware('auth');
Route::get('/resident-profile/add-mch', [MCHRecordController::class, 'addRecord'])->name('addMCH')->middleware('auth');
Route::post('/resident-profile/edit-mch', [MCHRecordController::class, 'editRecord'])->name('editMCH')->middleware('auth');
Route::get('/resident-profile/add-pp', [PPRecordController::class, 'addRecord'])->name('addPP')->middleware('auth');
Route::post('/resident-profile/edit-pp', [PPRecordController::class, 'editRecord'])->name('editPP')->middleware('auth');
Route::get('/resident-profile/add-epi', [EPIRecordController::class, 'addRecord'])->name('addEPI')->middleware('auth');
Route::post('/resident-profile/edit-epi', [EPIRecordController::class, 'editRecord'])->name('editEPI')->middleware('auth');
Route::get('/resident-profile/add-ufc', [UFCRecordController::class, 'addRecord'])->name('addUFC')->middleware('auth');
Route::post('/resident-profile/edit-ufc', [UFCRecordController::class, 'editRecord'])->name('editUFC')->middleware('auth');
Route::get('/resident-profile/add-fp', [FPRecordController::class, 'addRecord'])->name('addFP')->middleware('auth');
Route::post('/resident-profile/edit-fp', [FPRecordController::class, 'editRecord'])->name('editFP')->middleware('auth');
Route::get('/resident-profile/add-cdd', [CDDRecordController::class, 'addRecord'])->name('addCDD')->middleware('auth');
Route::post('/resident-profile/edit-cdd', [CDDRecordController::class, 'editRecord'])->name('editCDD')->middleware('auth');
Route::get('/resident-profile/add-mortality', [MortalityRecordController::class, 'addRecord'])->name('addMortality')->middleware('auth');
Route::post('/resident-profile/edit-mortality', [MortalityRecordController::class, 'editRecord'])->name('editMortality')->middleware('auth');
Route::get('/resident-profile/add-cari', [CariRecordController::class, 'addRecord'])->name('addCari')->middleware('auth');
Route::post('/resident-profile/edit-cari', [CariRecordController::class, 'editRecord'])->name('editCari')->middleware('auth');
Route::get('/resident-profile/add-gms', [GMSRecordController::class, 'addRecord'])->name('addGMS')->middleware('auth');
Route::post('/resident-profile/edit-gms', [GMSRecordController::class, 'editRecord'])->name('editGMS')->middleware('auth');
Route::get('/resident-profile/add-bip', [BIPRecordController::class, 'addRecord'])->name('addBIP')->middleware('auth');
Route::post('/resident-profile/edit-bip', [BIPRecordController::class, 'editRecord'])->name('editBIP')->middleware('auth');
Route::get('/resident-profile/add-tb-symp', [TBSympRecordController::class, 'addRecord'])->name('addTBSymp')->middleware('auth');
Route::post('/resident-profile/edit-tb-symp', [TBSympRecordController::class, 'editRecord'])->name('editTBSymp')->middleware('auth');
Route::get('/resident-profile/add-rabies', [RabiesRecordController::class, 'addRecord'])->name('addRabies')->middleware('auth');
Route::post('/resident-profile/edit-rabies', [RabiesRecordController::class, 'editRecord'])->name('editRabies')->middleware('auth');
Route::get('/resident-profile/add-sanitation', [SanitationRecordController::class, 'addRecord'])->name('addSanitation')->middleware('auth');
Route::post('/resident-profile/edit-sanitation', [SanitationRecordController::class, 'editRecord'])->name('editSanitation')->middleware('auth');

Route::get('/resident-profile/print-records', [PrintResidentMedicalRecordController::class, 'index'])->name('printMedicalRecords')->middleware('auth');
Route::get('/resident-profile/print-walkin-records', [PrintWalkInRecordController::class, 'index'])->name('printWalkInRecords')->middleware('auth');
Route::get('/resident-profile/print-mch-records', [PrintMCHRecordController::class, 'index'])->name('printMCHRecords')->middleware('auth');
Route::get('/resident-profile/print-pp-records', [PrintPPRecordController::class, 'index'])->name('printPPRecords')->middleware('auth');
Route::get('/resident-profile/print-epi-records', [PrintEPIRecordController::class, 'index'])->name('printEPIRecords')->middleware('auth');
Route::get('/resident-profile/print-ufc-records', [PrintUFCRecordController::class, 'index'])->name('printUFCRecords')->middleware('auth');
Route::get('/resident-profile/print-fp-records', [PrintFPRecordController::class, 'index'])->name('printFPRecords')->middleware('auth');
Route::get('/resident-profile/print-cdd-records', [PrintCDDRecordController::class, 'index'])->name('printCDDRecords')->middleware('auth');
Route::get('/resident-profile/print-mortality-records', [PrintMortalityRecordController::class, 'index'])->name('printMortalityRecords')->middleware('auth');
Route::get('/resident-profile/print-cari-records', [PrintCARIRecordController::class, 'index'])->name('printCariRecords')->middleware('auth');
Route::get('/resident-profile/print-gms-records', [PrintGMSRecordController::class, 'index'])->name('printGMSRecords')->middleware('auth');
Route::get('/resident-profile/print-bip-records', [PrintBIPRecordController::class, 'index'])->name('printBIPRecords')->middleware('auth');
Route::get('/resident-profile/print-tb-symp-records', [PrintTBSympRecordController::class, 'index'])->name('printTBSympRecords')->middleware('auth');
Route::get('/resident-profile/print-rabies-records', [PrintRabiesRecordController::class, 'index'])->name('printRabiesRecords')->middleware('auth');
Route::get('/resident-profile/print-sanitation-records', [PrintSnitationRecordController::class, 'index'])->name('printSanitationRecords')->middleware('auth');

Route::get('/household', [HouseholdController::class, 'index'])->name('household')->middleware('auth');
Route::post('/household', [HouseholdController::class, 'searchHousehold'])->name('searchHousehold');
Route::post('/household/trash-household', [TrashHouseholdController::class, 'trashHouseholdFromHouseholdPage'])->name('trashHouseholdFromHouseholdPage')->middleware('auth');
Route::get('/household/add-household', [AddHouseholdInfoController::class, 'addRecord'])->name('addHousehold')->middleware('auth');
Route::get('/household/household-info', [HouseholdInfoController::class, 'viewRecord'])->name('viewHousehold')->middleware('auth');
Route::post('/household/household-info/appoint-head', [AppointHouseHeadController::class, 'appointHead'])->name('appointHead')->middleware('auth');
Route::post('/household/household-info/remove-member', [RemoveHouseMemberController::class, 'removeHouseMember'])->name('removeHouseMember')->middleware('auth');
Route::post('/household/household-info/trash-household', [TrashHouseholdController::class, 'trashHousehold'])->name('trashHousehold')->middleware('auth');
Route::post('/household/household-info/edit-household', [EditHouseholdController::class, 'editHousehold'])->name('editHousehold')->middleware('auth');
Route::post('/household/household-info/add-household-member', [AddHouseholdMemberController::class, 'addHouseholdMember'])->name('addHouseholdMember')->middleware('auth');
Route::get('/household/household-info/print-household-record', [PrintHouseholdInfoController::class, 'index'])->name('printHousehold')->middleware('auth');

Route::get('/barangay-info', [BarangayInfoController::class, 'index'])->name('barangay')->middleware('auth');
Route::get('/barangay-info/add-new-worker', [AddNewWorkerController::class, 'index'])->name('addNewWorker')->middleware('auth');

Route::post('/barangay-info/edit-worker', [EditBarangayWorkerController::class, 'editRecord'])->name('editWorker')->middleware('auth');
Route::get('/barangay-info/trash-worker', [EditBarangayWorkerController::class, 'trashRecord'])->name('trashWorker')->middleware('auth');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'signin']);
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/register-an-account', [RegisterController::class, 'index'])->name('register');
Route::post('/register-an-account', [RegisterController::class, 'store']);
