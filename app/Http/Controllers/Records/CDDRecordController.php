<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\CDDReport;
use Illuminate\Http\Request;

class CDDRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new CDDReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->complaints = $request->complaints;
        $item->num_OR = $request->or_number;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident CDD record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident CDD record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $CDD_Record = CDDReport::where('id', '=', $request->CDD_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $CDD_Record->age = $request->CDD_age;
        $CDD_Record->complaints = $request->CDD_complaints;
        $CDD_Record->num_OR = $request->CDD_or_number;
        $CDD_Record->remarks = $request->CDD_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($CDD_Record->save()) {
            return back()->with('SuccessNotification', 'Resident CDD record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident CDD record failed.');
        }
        // ========================== END ==========================
    }
}
