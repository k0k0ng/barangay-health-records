<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\EPIReport;
use Illuminate\Http\Request;

class EPIRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new EPIReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->mother_name = $request->mother_name;
        $item->father_name = $request->father_name;
        $item->fdg = $request->fdg;
        $item->weight = $request->weight;
        $item->r_code = $request->r_code;
        $item->vaccine = $request->vaccine;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident EPI record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident EPI record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $EPI_Record = EPIReport::where('id', '=', $request->EPI_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $EPI_Record->age = $request->EPI_age;
        $EPI_Record->mother_name = $request->EPI_mother_name;
        $EPI_Record->father_name = $request->EPI_father_name;
        $EPI_Record->fdg = $request->EPI_fdg;
        $EPI_Record->weight = $request->EPI_weight;
        $EPI_Record->r_code = $request->EPI_r_code;
        $EPI_Record->vaccine = $request->EPI_vaccine;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($EPI_Record->save()) {
            return back()->with('SuccessNotification', 'Resident EPI record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident EPI record failed.');
        }
        // ========================== END ==========================
    }
}
