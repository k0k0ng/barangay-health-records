<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\Mortality;
use Illuminate\Http\Request;

class MortalityRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new Mortality();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->DOD = $request->dod;
        $item->COD = $request->cod;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident Mortality record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident Mortality record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $Mortality_Record = Mortality::where('id', '=', $request->Mortality_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $Mortality_Record->age = $request->Mortality_age;
        $Mortality_Record->DOD = $request->Mortality_dod;
        $Mortality_Record->COD = $request->Mortality_cod;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($Mortality_Record->save()) {
            return back()->with('SuccessNotification', 'Resident Mortality record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident Mortality record failed.');
        }
        // ========================== END ==========================
    }
}
