<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\GMSReport;
use Illuminate\Http\Request;

class GMSRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new GMSReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->complaints = $request->complaints;
        $item->HO_advice = $request->ho_advice;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident GMS record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident GMS record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $GMS_Record = GMSReport::where('id', '=', $request->GMS_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $GMS_Record->age = $request->GMS_age;
        $GMS_Record->complaints = $request->GMS_complaints;
        $GMS_Record->HO_advice = $request->GMS_ho_advice;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($GMS_Record->save()) {
            return back()->with('SuccessNotification', 'Resident GMS record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident GMS record failed.');
        }
        // ========================== END ==========================
    }
}
