<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\SANITATIONReport;
use Illuminate\Http\Request;

class SanitationRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new SANITATIONReport();

        $item->resident_id = $request->ResidentID;
        $item->no_toilet = $request->no_toilet;
        $item->not_proper = $request->not_proper;
        $item->poor = $request->poor;
        $item->without = $request->without;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident Sanitation record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident Sanitation record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $Sanitation_Record = SANITATIONReport::where('id', '=', $request->Sanitation_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $Sanitation_Record->no_toilet = $request->Sanitation_no_toilet;
        $Sanitation_Record->not_proper = $request->Sanitation_not_proper;
        $Sanitation_Record->poor = $request->Sanitation_poor;
        $Sanitation_Record->without = $request->Sanitation_without;
        $Sanitation_Record->remarks = $request->Sanitation_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($Sanitation_Record->save()) {
            return back()->with('SuccessNotification', 'Resident Sanitation record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident Sanitation record failed.');
        }
        // ========================== END ==========================
    }
}
