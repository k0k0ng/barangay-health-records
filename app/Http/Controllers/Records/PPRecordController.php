<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\PPReport;
use Illuminate\Http\Request;

class PPRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new PPReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->PlaceOfDelivery = $request->place_delivery;
        $item->attended_by = $request->attended_by;
        $item->gender = $request->gender;
        $item->fdg = $request->fdg;
        $item->weight = $request->weight;
        $item->date_of_pp = $request->date_pp;
        $item->vitamina = $request->vitamina;
        $item->dod = $request->dod;
        $item->F = $request->f;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident PP record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident PP record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $PP_Record = PPReport::where('id', '=', $request->PP_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $PP_Record->age = $request->PP_age;
        $PP_Record->PlaceOfDelivery = $request->PP_place_delivery;
        $PP_Record->attended_by = $request->PP_attended_by;
        $PP_Record->gender = $request->PP_gender;
        $PP_Record->fdg = $request->PP_fdg;
        $PP_Record->weight = $request->PP_weight;
        $PP_Record->date_of_pp = $request->PP_date_pp;
        $PP_Record->vitamina = $request->PP_vitamina;
        $PP_Record->dod = $request->PP_dod;
        $PP_Record->F = $request->PP_f;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($PP_Record->save()) {
            return back()->with('SuccessNotification', 'Resident PP record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident PP record failed.');
        }
        // ========================== END ==========================
    }
}
