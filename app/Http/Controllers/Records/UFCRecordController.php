<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\UFCReport;
use Illuminate\Http\Request;

class UFCRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new UFCReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->mother_name = $request->mother_name;
        $item->father_name = $request->father_name;
        $item->fdg = $request->fdg;
        $item->weight = $request->weight;
        $item->r_code = $request->r_code;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident UFC record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident UFC record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $UFC_Record = UFCReport::where('id', '=', $request->UFC_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $UFC_Record->age = $request->UFC_age;
        $UFC_Record->mother_name = $request->UFC_mother_name;
        $UFC_Record->father_name = $request->UFC_father_name;
        $UFC_Record->fdg = $request->UFC_fdg;
        $UFC_Record->weight = $request->UFC_weight;
        $UFC_Record->r_code = $request->UFC_r_code;
        $UFC_Record->remarks = $request->UFC_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($UFC_Record->save()) {
            return back()->with('SuccessNotification', 'Resident UFC record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident UFC record failed.');
        }
        // ========================== END ==========================
    }
}
