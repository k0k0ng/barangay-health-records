<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\TBSympReport;
use Illuminate\Http\Request;

class TBSympRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new TBSympReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->DOX_ray = $request->dox_ray;
        $item->date_first = $request->date_first;
        $item->sputum = $request->sputum1;
        $item->submit3 = $request->submit3;
        $item->date_first2 = $request->date_second;
        $item->sputum2 = $request->sputum2;
        $item->result3 = $request->result;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident TB Symp record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident TB Symp record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $TBSymp_Record = TBSympReport::where('id', '=', $request->TBSymp_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $TBSymp_Record->age = $request->TBSymp_age;
        $TBSymp_Record->DOX_ray = $request->TBSymp_dox_ray;
        $TBSymp_Record->date_first = $request->TBSymp_date_first;
        $TBSymp_Record->sputum = $request->TBSymp_sputum1;
        $TBSymp_Record->submit3 = $request->TBSymp_submit3;
        $TBSymp_Record->date_first2 = $request->TBSymp_date_second;
        $TBSymp_Record->sputum2 = $request->TBSymp_sputum2;
        $TBSymp_Record->result3 = $request->TBSymp_result;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($TBSymp_Record->save()) {
            return back()->with('SuccessNotification', 'Resident TB Symp record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident TB Symp record failed.');
        }
        // ========================== END ==========================
    }
}
