<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\CARIReport;
use Illuminate\Http\Request;

class CariRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new CARIReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->complaints = $request->complaints;
        $item->HO_advice = $request->ho_advice;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident CARI record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident CARI record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }



    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $Cari_Record = CARIReport::where('id', '=', $request->Cari_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $Cari_Record->age = $request->Cari_age;
        $Cari_Record->complaints = $request->Cari_complaints;
        $Cari_Record->HO_advice = $request->Cari_ho_advice;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($Cari_Record->save()) {
            return back()->with('SuccessNotification', 'Resident CARI record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident CARI record failed.');
        }
        // ========================== END ==========================
    }
}
