<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\BIPReport;
use Illuminate\Http\Request;

class BIPRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new BIPReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->BP = $request->blood_pressure;
        $item->client_type = $request->client_type;
        $item->f_history = $request->f_history;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident BIP record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident BIP record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }



    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $BIP_Record = BIPReport::where('id', '=', $request->BIP_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $BIP_Record->age = $request->BIP_age;
        $BIP_Record->BP = $request->BIP_blood_pressure;
        $BIP_Record->client_type = $request->BIP_client_type;
        $BIP_Record->f_history = $request->BIP_f_history;
        $BIP_Record->remarks = $request->BIP_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($BIP_Record->save()) {
            return back()->with('SuccessNotification', 'Resident BIP record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident BIP record failed.');
        }
        // ========================== END ==========================
    }
}
