<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\Walkin;
use Illuminate\Http\Request;

class WalkInRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new Walkin();

        $item->resident_id = $request->ResidentID;
        $item->blood_pressure = $request->blood_pressure;
        $item->blood_sugar = $request->blood_sugar;
        $item->consultation = $request->consultation;
        $item->findings = $request->findings;
        $item->notes = $request->notes;
        $item->medicine = $request->medicine;
        $item->medicine_quantity = $request->quantity;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident Walk-In record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident Walk-In record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }



    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $WalkIn_Record = Walkin::where('id', '=', $request->WalkIn_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $WalkIn_Record->blood_pressure = $request->WalkIn_blood_pressure;
        $WalkIn_Record->blood_sugar = $request->WalkIn_blood_sugar;
        $WalkIn_Record->consultation = $request->WalkIn_consultation;
        $WalkIn_Record->findings = $request->WalkIn_findings;
        $WalkIn_Record->notes = $request->WalkIn_notes;
        $WalkIn_Record->medicine = $request->WalkIn_medicine;
        $WalkIn_Record->medicine_quantity = $request->WalkIn_quantity;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($WalkIn_Record->save()) {
            return back()->with('SuccessNotification', 'Resident Walk-In record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident Walk-In record failed.');
        }
        // ========================== END ==========================
    }
}
