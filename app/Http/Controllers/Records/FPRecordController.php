<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\FPReport;
use Illuminate\Http\Request;

class FPRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new FPReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->num_child = $request->num_child;
        $item->lmp = $request->lmp;
        $item->client_type = $request->client_type;
        $item->method_accepted = $request->method_accepted;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident FP record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident FP record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $FP_Record = FPReport::where('id', '=', $request->FP_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $FP_Record->age = $request->FP_age;
        $FP_Record->num_child = $request->FP_num_child;
        $FP_Record->lmp = $request->FP_lmp;
        $FP_Record->client_type = $request->FP_client_type;
        $FP_Record->method_accepted = $request->FP_method_accepted;
        $FP_Record->remarks = $request->FP_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($FP_Record->save()) {
            return back()->with('SuccessNotification', 'Resident FP record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident FP record failed.');
        }
        // ========================== END ==========================
    }
}
