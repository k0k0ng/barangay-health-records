<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\MCHReport;
use Illuminate\Http\Request;

class MCHRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new MCHReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->g = $request->g;
        $item->p = $request->p;
        $item->rcode = $request->rcode;
        $item->level = $request->level;
        $item->range = $request->range;
        $item->lmp = $request->lmp;
        $item->edc = $request->edc;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident MCH record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident MCH record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }


    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $MCH_Record = MCHReport::where('id', '=', $request->MCH_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $MCH_Record->age = $request->MCH_age;
        $MCH_Record->g = $request->MCH_g;
        $MCH_Record->p = $request->MCH_p;
        $MCH_Record->rcode = $request->MCH_rcode;
        $MCH_Record->level = $request->MCH_level;
        $MCH_Record->lmp = $request->MCH_lmp;
        $MCH_Record->range = $request->MCH_range;
        $MCH_Record->edc = $request->MCH_edc;
        $MCH_Record->remarks = $request->MCH_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($MCH_Record->save()) {
            return back()->with('SuccessNotification', 'Resident MCH record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident MCH record failed.');
        }
        // ========================== END ==========================
    }
}
