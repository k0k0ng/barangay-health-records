<?php

namespace App\Http\Controllers\Records;

use App\Http\Controllers\Controller;
use App\Models\RABIESReport;
use Illuminate\Http\Request;

class RabiesRecordController extends Controller
{
    public function addRecord(Request $request)
    {
        $item = new RABIESReport();

        $item->resident_id = $request->ResidentID;
        $item->age = $request->age;
        $item->date = $request->date;
        $item->complaint_bite = $request->complaint_bite;
        $item->remarks = $request->remarks;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('SuccessNotification', 'Adding resident Rabies record success.');
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Adding resident Rabies record failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }



    public function editRecord(Request $request)
    {

        // ----------- Get resident info --------------
        $Rabies_Record = RABIESReport::where('id', '=', $request->Rabies_Record_ID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $Rabies_Record->age = $request->Rabies_age;
        $Rabies_Record->date = $request->Rabies_date;
        $Rabies_Record->complaint_bite = $request->Rabies_complaint_bite;
        $Rabies_Record->remarks = $request->Rabies_remarks;
        // ============================ END ============================

        // ----------- Save the new informations to databse -----------------
        if ($Rabies_Record->save()) {
            return back()->with('SuccessNotification', 'Resident Rabies record successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident Rabies record failed.');
        }
        // ========================== END ==========================
    }
}
