<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:255',
            'password' => 'required|confirmed',
        ]);

        Schema::disableForeignKeyConstraints();
        Account::create([
            'info_id' => 0,
            'access_id' => 4,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'remember_token' => 'Temporary',
        ]);
        Schema::enableForeignKeyConstraints();

        Auth::attempt($request->only('username', 'password'));

        return redirect()->route('dashboard');
    }
}
