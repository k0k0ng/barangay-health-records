<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index(Request $request)
    {

        if (!Hash::check($request->current_password, Auth::user()->password)) return back()->with('EditFailed', 'Password entered does not match.');
        if ($request->new_password != $request->confirmation_password) return back()->with('EditFailed', 'Confirmation Password entered does not match.');

        $user = Account::where('id', Auth::user()->id)->first();
        $user->password = Hash::make($request->new_password);

        try {
            $user->save();
            return back()->with('EditSuccess', 'Changing password success.');
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating Sanitation record failed.');
        }
    }
}
