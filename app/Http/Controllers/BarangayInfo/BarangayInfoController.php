<?php

namespace App\Http\Controllers\BarangayInfo;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BarangayWorker;
use App\Models\UserInfo;
use Illuminate\Support\Facades\Auth;

class BarangayInfoController extends Controller
{

    public function index()
    {
        // -------------- Get Current Users Barangay ID -------------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // ============ END Get Current Users Barangay ID ============


        // ------------------- Get barangay official info -------------------
        $barangay_info = Barangay::where('id', $users_brgy_id)->first();

        $barangay_officials = array('', '', '', '', '', '', '', '', '');
        if ($barangay_info->info_cap != 0 || $barangay_info->info_cap != NULL) $barangay_officials[0] = UserInfo::where('id', $barangay_info->info_cap)->first();
        if ($barangay_info->info_wad1 != 0 || $barangay_info->info_wad1 != NULL) $barangay_officials[1] = UserInfo::where('id', $barangay_info->info_wad1)->first();
        if ($barangay_info->info_wad2 != 0 || $barangay_info->info_wad2 != NULL) $barangay_officials[2] = UserInfo::where('id', $barangay_info->info_wad2)->first();
        if ($barangay_info->info_wad3 != 0 || $barangay_info->info_wad3 != NULL) $barangay_officials[3] = UserInfo::where('id', $barangay_info->info_wad3)->first();
        if ($barangay_info->info_wad4 != 0 || $barangay_info->info_wad4 != NULL) $barangay_officials[4] = UserInfo::where('id', $barangay_info->info_wad4)->first();
        if ($barangay_info->info_wad5 != 0 || $barangay_info->info_wad5 != NULL) $barangay_officials[5] = UserInfo::where('id', $barangay_info->info_wad5)->first();
        if ($barangay_info->info_wad6 != 0 || $barangay_info->info_wad6 != NULL) $barangay_officials[6] = UserInfo::where('id', $barangay_info->info_wad6)->first();
        if ($barangay_info->info_wad7 != 0 || $barangay_info->info_wad7 != NULL) $barangay_officials[7] = UserInfo::where('id', $barangay_info->info_wad7)->first();
        if ($barangay_info->info_sec != 0 || $barangay_info->info_sec != NULL) $barangay_officials[8] = UserInfo::where('id', $barangay_info->info_sec)->first();
        // ================= END Get barangay official info =================


        // --- Get other barangay worker ---
        $barangay_workers = BarangayWorker::where('brgyID', '=', $users_brgy_id)->paginate(10);
        // === END ===

        $resident_infos = UserInfo::where('brgy_id', '=', $users_brgy_id)->get();

        return view('pages.barangay_info', [
            'barangay_info' => $barangay_info,
            'barangay_officials' => $barangay_officials,
            'barangay_workers' => $barangay_workers,
            'resident_infos' => $resident_infos,
            'users_image' => $users_image
        ]);
    }
}
