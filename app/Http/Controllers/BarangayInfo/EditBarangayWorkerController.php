<?php

namespace App\Http\Controllers\BarangayInfo;

use App\Http\Controllers\Controller;
use App\Models\BarangayWorker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EditBarangayWorkerController extends Controller
{
    public function editRecord(Request $request)
    {
        $worker = BarangayWorker::where([['id', $request->worker_ID], ['deleted_at', NULL]])->first();
        $worker->type = $request->edit_position;
        $worker->contactNumber = $request->edit_contactNumber;
        // ----------- Update worker info to database -----------------
        try {
            $worker->save();
            return back()->with('RecordingSucess', 'Adding household record success.');
        } catch (\Exception $e) {
            return back()->with('RecordingFailed', 'Adding Sanitation record failed.');
        }
        // ========================== END ==========================
    }


    public function trashRecord(Request $request)
    {
        if (Hash::check($request->confirmation_password, Auth::user()->password)) {
            $worker = BarangayWorker::where('id', $request->worker_ID2)->first();
            try {
                $worker->delete();
                return back()->with('RecordingSucess', 'Trashing record success.');
            } catch (\Exception $e) {
                return back()->with('RecordingFailed', 'Something went wrong, trashing info failed.');
            }
        } else {
            return back()->with('RecordingFailed', 'Password entered does not match.');
        }
    }
}
