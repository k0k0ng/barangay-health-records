<?php

namespace App\Http\Controllers\BarangayInfo;

use App\Http\Controllers\Controller;
use App\Models\BarangayWorker;
use Illuminate\Http\Request;

class AddNewWorkerController extends Controller
{
    public function index(Request $request)
    {
        $item = new BarangayWorker();
        //dd($request->BarangayID . ' ' . $request->FirstName . ' ' . $request->LastName . ' ' . $request->position . ' ' . $request->contactNumber);

        $item->brgyID = $request->BarangayID;
        $item->firstname = $request->FirstName;
        $item->lastname = $request->LastName;
        $item->type = $request->position;
        $item->contactNumber = $request->contactNumber;

        // ----------- Save the new informations to databse -----------------
        try {
            $item->save();
            return back()->with('RecordingSucess', 'Adding new barangay worker success.');
        } catch (\Exception $e) {
            return back()->with('RecordingFailed', 'Adding new barangay worker failed.');
            echo $e->getMessage();   // insert query
        }
        // ========================== END ==========================
    }
}
