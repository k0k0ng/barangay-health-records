<?php

namespace App\Http\Controllers\Resident;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\Purok;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditResidentProfileController extends Controller
{
    public function edit(Request $request)
    {
        // ----------- Get current users brgy ID -----------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_brgy_id = $users_brgy_id->brgy_id;
        // END

        // ----------- Get resident info --------------
        $resident_info = UserInfo::where('id', '=', $request->ResidentID)->first();
        // === END ===

        // ----------- Assign the updated information of the resident -----------------
        $resident_info->first_name = $request->efirstName;
        $resident_info->middle_name = $request->emiddleName;
        $resident_info->last_name = $request->elastName;

        if ($request->eBirthday != NULL) $resident_info->birthdate = $request->eBirthday;
        if ($request->eGender != NULL) $resident_info->gender = $request->eGender;
        if ($request->eHeight != NULL) $resident_info->height = $request->eHeight;
        if ($request->eWeight != NULL) $resident_info->weight = $request->eWeight;
        if ($request->eBlood_Type != NULL) $resident_info->blood_type = $request->eBlood_Type;
        if ($request->eReligion != NULL) $resident_info->religion = $request->eReligion;
        if ($request->eCell_No != NULL) $resident_info->cell_no = $request->eCell_No;
        if ($request->eEmail != NULL) $resident_info->email = $request->eEmail;
        if ($request->eTell_No != NULL) $resident_info->tell_no = $request->eTell_No;
        if ($request->eCivil_Status != NULL) $resident_info->status = $request->eCivil_Status;
        if ($request->eInname != NULL) $resident_info->inname = $request->eInname;
        if ($request->eContact != NULL) $resident_info->contact = $request->eContact;
        if ($request->eRelationship != NULL) $resident_info->relationship = $request->eRelationship;
        if ($request->eEducation != NULL) $resident_info->education = $request->eEducation;
        if ($request->eCurrently_Enrolled != NULL) $resident_info->cur_enrolled = $request->eCurrently_Enrolled;
        if ($request->eOccupation != NULL) $resident_info->occupation = $request->eOccupation;
        if ($request->eCurrently_Employed != NULL) $resident_info->cur_employed = $request->eCurrently_Employed;
        if ($request->eRegistered_Voter != NULL) $resident_info->is_voter = $request->eRegistered_Voter;
        if ($request->eVoters_ID != NULL) $resident_info->voter_id = $request->eVoters_ID;
        if ($request->eBarangay_ID != NULL) $resident_info->citizen_brgy_id = $request->eBarangay_ID;
        if ($request->eResidence_Type != NULL) $resident_info->residence_type = $request->eResidence_Type;
        if ($request->eSenior_Citizen != NULL) $resident_info->senior_citizen = $request->eSenior_Citizen;
        if ($request->ePWD != NULL) $resident_info->pwd = $request->ePWD;
        if ($request->eDeceased != NULL) $resident_info->deceased = $request->eDeceased;

        if ($request->eStreet != NULL) $resident_info->street = $request->eStreet;
        $resident_info->address = $request->eStreet;

        $barangay_info = Barangay::where('id', '=', $users_brgy_id)->first();
        $resident_info->brgy_id = $barangay_info->id;

        // ----- Change DP function -----
        try {

            $request->validate([
                'eResidentImage' => 'image|mimes:jpeg,png,jpg|max:2048'
            ]);

            if ($request->hasFile('eResidentImage')) {
                $resident_info->img_name = $request->ResidentID;
                $resident_info->img_extension = $request->eResidentImage->extension();
                $request->eResidentImage->move(public_path('img'), $request->ResidentID . '.' . $request->eResidentImage->extension());
            }
        } catch (\Exception $e) {
            return back()->with('FailedNotification', 'Something went wrong in uploading profile picture.');
            echo $e->getMessage();   // insert query
        }
        // === END ===


        if ($request->ePurok != NULL) {
            $purok_info = Purok::where('id', '=', $request->ePurok)->first();

            if ($resident_info->address == NULL) {
                $resident_info->address = $purok_info->name . ', ' . $barangay_info->brgy_name . ', ' . $barangay_info->city;
            } else {
                $resident_info->address .= ', ' . $purok_info->name . ', ' . $barangay_info->brgy_name . ', ' . $barangay_info->city;
            }

            $resident_info->purok_id = $purok_info->id;
        } else {
            if ($resident_info->address == NULL) {
                $resident_info->address = $barangay_info->brgy_name . ', ' . $barangay_info->city;
            } else {
                $resident_info->address .= ', ' . $barangay_info->brgy_name . ', ' . $barangay_info->city;
            }
        }
        // ============================ END ============================


        // ----------- Save the new informations to databse -----------------
        if ($resident_info->save()) {
            return back()->with('SuccessNotification', 'Resident info successfully updated.');
        } else {
            return back()->with('FailedNotification', 'Updating resident info failed.');
        }
        // ========================== END ==========================

    }
}
