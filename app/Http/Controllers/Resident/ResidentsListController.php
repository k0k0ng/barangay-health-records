<?php

namespace App\Http\Controllers\Resident;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Barangay;
use App\Models\Purok;
use App\Models\UserInfo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResidentsListController extends Controller
{
    public function resident_list(Request $request)
    {

        // ----------- Get current users brgy ID -----------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // END

        // ----------- Identify USeP admin IDs -----------
        $accounts = Account::where('access_id', '=', 1)->get();
        $adminIDs = [];
        foreach ($accounts as $user) {
            array_push($adminIDs, $user->info_id);
        }
        // ============ END Identify USeP admin IDs ============


        // ----------- Get all residence info excluding USEP admin and filtering based on searched key (if there is) -----------
        $persons = UserInfo::where('brgy_id', '=', $users_brgy_id)->whereNotIn(
            "id",
            $adminIDs
        )->where(function (Builder $query) use ($request) {
            return $query->where([
                ['first_name', '!=', Null],
                [function ($query) use ($request) {

                    if (($term = $request->term)) {
                        $query->orWhere('first_name', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ])->orWhere([
                ['last_name', '!=', Null],
                [function ($query) use ($request) {
                    if (($term = $request->term)) {
                        $query->orWhere('last_name', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ])->orWhere([
                ['address', '!=', Null],
                [function ($query) use ($request) {
                    if (($term = $request->term)) {
                        $query->orWhere('address', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ]);
        })->addSelect([
            'purok_ID' => Purok::select('id')
                ->whereColumn('purok.id', 'purok_id'),
            'purok_name' => Purok::select('name')
                ->whereColumn('purok.id', 'purok_id'),
            'brgy_ID' => Barangay::select('id')
                ->whereColumn('barangay_info.id', 'brgy_id'),
            'brgy_name' => Barangay::select('brgy_name')
                ->whereColumn('barangay_info.id', 'brgy_id'),
            'city_name' => Barangay::select('city')
                ->whereColumn('barangay_info.id', 'brgy_id')
        ])
            ->sortable()
            ->paginate(15);
        // ====================== End Get all residence info ======================


        // ------------------- Get barangay and purok info -------------------
        $purok_infos = Purok::where('brgy_id', '=', $users_brgy_id)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================

        return view('pages.residents_list', [
            'persons' => $persons,
            'purok_infos' => $purok_infos,
            'users_image' => $users_image
        ]);
    }


    public function searchResidentProfile(Request $request)
    {

        // ----------- Identify Users ID -----------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // END

        // ----------- Identify USeP admin IDs -----------
        $accounts = Account::where('access_id', '=', 1)->get();
        $adminIDs = [];
        foreach ($accounts as $user) {
            array_push($adminIDs, $user->info_id);
        }
        // ============ END Identify USeP admin IDs ============


        // ----------- Get all residence info excluding USEP admin and filtering based on searched key (if there is) -----------
        $persons = UserInfo::where('brgy_id', '=', $users_brgy_id)->whereNotIn(
            "id",
            $adminIDs
        )->where(function (Builder $query) use ($request) {
            return $query->where([
                ['first_name', '!=', Null],
                [function ($query) use ($request) {

                    if (($term = $request->term)) {
                        $query->orWhere('first_name', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ])->orWhere([
                ['last_name', '!=', Null],
                [function ($query) use ($request) {
                    if (($term = $request->term)) {
                        $query->orWhere('last_name', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ])->orWhere([
                ['address', '!=', Null],
                [function ($query) use ($request) {
                    if (($term = $request->term)) {
                        $query->orWhere('address', 'LIKE', '%' . $term . '%')->get();
                    }
                }]
            ]);
        })->addSelect([
            'purok_ID' => Purok::select('id')
                ->whereColumn('purok.id', 'purok_id'),
            'purok_name' => Purok::select('name')
                ->whereColumn('purok.id', 'purok_id'),
            'brgy_ID' => Barangay::select('id')
                ->whereColumn('barangay_info.id', 'brgy_id'),
            'brgy_name' => Barangay::select('brgy_name')
                ->whereColumn('barangay_info.id', 'brgy_id'),
            'city_name' => Barangay::select('city')
                ->whereColumn('barangay_info.id', 'brgy_id')
        ])
            ->sortable()
            ->paginate(15);
        // ====================== End Get all residence info ======================


        // ------------------- Get barangay and purok info -------------------
        $purok_infos = Purok::where('brgy_id', '=', $users_brgy_id)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================

        return view('pages.residents_list', [
            'persons' => $persons,
            'purok_infos' => $purok_infos,
            'users_image' => $users_image
        ]);
    }
}
