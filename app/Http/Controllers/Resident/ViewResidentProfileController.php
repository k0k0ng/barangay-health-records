<?php

namespace App\Http\Controllers\Resident;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BIPReport;
use App\Models\CARIReport;
use App\Models\CDDReport;
use App\Models\EPIReport;
use App\Models\FPReport;
use App\Models\GMSReport;
use App\Models\Household;
use App\Models\HouseholdInfo;
use App\Models\HouseholdMember;
use App\Models\MCHReport;
use App\Models\Mortality;
use App\Models\PPReport;
use App\Models\Purok;
use App\Models\RABIESReport;
use App\Models\SANITATIONReport;
use App\Models\TBSympReport;
use App\Models\UFCReport;
use App\Models\UserInfo;
use App\Models\Walkin;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ViewResidentProfileController extends Controller
{
    public function ViewResidentProfile(Request $request)
    {

        // ----------- Get current user info -----------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        // === END ===

        // ----------- Get all residence info excluding USEP admin and filtering based on searched key (if there is) -----------
        $person = UserInfo::where('id', '=', $request->residentProfileSelectedID)
            ->where(function (Builder $query) use ($request) {
                return $query->where([
                    ['first_name', '!=', Null],
                    [function ($query) use ($request) {

                        if (($term = $request->term)) {
                            $query->orWhere('first_name', 'LIKE', '%' . $term . '%')->get();
                        }
                    }]
                ])->orWhere([
                    ['last_name', '!=', Null],
                    [function ($query) use ($request) {
                        if (($term = $request->term)) {
                            $query->orWhere('last_name', 'LIKE', '%' . $term . '%')->get();
                        }
                    }]
                ])->orWhere([
                    ['address', '!=', Null],
                    [function ($query) use ($request) {
                        if (($term = $request->term)) {
                            $query->orWhere('address', 'LIKE', '%' . $term . '%')->get();
                        }
                    }]
                ]);
            })->addSelect([
                'purok_ID' => Purok::select('id')
                    ->whereColumn('purok.id', 'purok_id'),
                'purok_name' => Purok::select('name')
                    ->whereColumn('purok.id', 'purok_id'),
                'brgy_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'brgy_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'city_name' => Barangay::select('city')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])
            ->first();
        // ====================== End Get all residence info ======================

        // --------------- Get Age using Birthday function ---------------
        //date in mm/dd/yyyy format; or it can be in other formats as well
        $birthDate = date('m/d/Y', strtotime($person->birthdate));
        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        $person->age = $age;
        // ============== END Get Age using Birthday function ==============

        // ------------------- Get barangay and purok info -------------------
        $purok_infos = Purok::where('brgy_id', '=', $person->brgy_ID)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================

        // ----------- Get all health records of resident -----------
        $all_records = [[]];
        $record_count = 0;

        $WalkIn_Record = Walkin::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($WalkIn_Record as $record) {
            $all_records[$record_count]['type'] = "Walk In";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $MCH_Record = MCHReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($MCH_Record as $record) {
            $all_records[$record_count]['type'] = "MCH";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $PP_Record = PPReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($PP_Record as $record) {
            $all_records[$record_count]['type'] = "PP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $EPI_Record = EPIReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($EPI_Record as $record) {
            $all_records[$record_count]['type'] = "EPI";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $UFC_Record = UFCReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($UFC_Record as $record) {
            $all_records[$record_count]['type'] = "UFC";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $FP_Record = FPReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($FP_Record as $record) {
            $all_records[$record_count]['type'] = "FP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $CDD_Record = CDDReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($CDD_Record as $record) {
            $all_records[$record_count]['type'] = "CDD";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Mortality_Record = Mortality::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Mortality_Record as $record) {
            $all_records[$record_count]['type'] = "Mortality";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $CARI_Record = CARIReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($CARI_Record as $record) {
            $all_records[$record_count]['type'] = "CARI";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $GMS_Record = GMSReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($GMS_Record as $record) {
            $all_records[$record_count]['type'] = "GMS";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $BIP_Record = BIPReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($BIP_Record as $record) {
            $all_records[$record_count]['type'] = "BIP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $TB_Symp_Record = TBSympReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($TB_Symp_Record as $record) {
            $all_records[$record_count]['type'] = "TB Symp";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Rabies_Record = RABIESReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Rabies_Record as $record) {
            $all_records[$record_count]['type'] = "Rabies";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Sanitation_Record = SANITATIONReport::where([['resident_id', $person->id], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Sanitation_Record as $record) {
            $all_records[$record_count]['type'] = "Sanitation";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        // Sort All Records in Descending order
        if (!empty($all_records[0])) {
            $update = array_column($all_records, 'updated_at');
            array_multisort($update, SORT_DESC, $all_records);
        }
        // === END Sort ===

        // ====== END Get all health records of resident =======


        $resident_household_belongs = HouseholdMember::where('person_id', $request->residentProfileSelectedID)
            ->addSelect([
                'household_ID' => Household::select('id')
                    ->whereColumn('households.id', 'household_id')
                    ->withTrashed(),
                'household_Status' => Household::select('deleted_at')
                    ->whereColumn('households.id', 'household_id')
                    ->withTrashed(),
                'household_Info_ID' => Household::select('household_info_id')
                    ->whereColumn('households.id', 'household_id')
                    ->withTrashed(),
                'household_Barangay_ID' => HouseholdInfo::select('brgy_id')
                    ->whereColumn('household_infos.id', 'household_Info_ID')
                    ->withTrashed(),
                'household_Barangay_Name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'household_Barangay_ID'),
                'household_Purok_ID' => HouseholdInfo::select('purok_id')
                    ->whereColumn('household_infos.id', 'household_Info_ID')
                    ->withTrashed(),
                'household_Purok_Name' => Purok::select('name')
                    ->whereColumn('purok.id', 'household_Purok_ID')
            ])
            ->withTrashed()
            ->orderBy('created_at', 'desc')
            ->get();

        return view('pages.resident_profile', [
            'person' => $person,
            'WalkIn_Record' => $WalkIn_Record,
            'MCH_Record' => $MCH_Record,
            'PP_Record' => $PP_Record,
            'EPI_Record' => $EPI_Record,
            'UFC_Record' => $UFC_Record,
            'FP_Record' => $FP_Record,
            'CDD_Record' => $CDD_Record,
            'Mortality_Record' => $Mortality_Record,
            'CARI_Record' => $CARI_Record,
            'GMS_Record' => $GMS_Record,
            'BIP_Record' => $BIP_Record,
            'TB_Symp_Record' => $TB_Symp_Record,
            'Rabies_Record' => $Rabies_Record,
            'Sanitation_Record' => $Sanitation_Record,
            'purok_infos' => $purok_infos,
            'all_records' => $all_records,
            'resident_household_belongs' => $resident_household_belongs,
            'users_image' => $users_image
        ]);
    }
}
