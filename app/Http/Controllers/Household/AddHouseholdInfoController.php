<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\Household;
use App\Models\HouseholdInfo;
use App\Models\HouseholdMember;
use App\Models\SANITATIONReport;
use App\Models\SanitationStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddHouseholdInfoController extends Controller
{
    public function addRecord(Request $request)
    {

        if ($request->dateProfiled === NULL) return back()->with('addRecordFailed', 'Date profiled info is empty.');

        $household_info = new HouseholdInfo();
        $household_info->brgy_id = $request->Barangay_ID;
        $household_info->brgy_chairman_id = $request->barangayChairman;
        $household_info->midwife_ndp_assigned  = $request->midwife;
        $household_info->purok_id = $request->purok;
        $household_info->interviewed_by = $request->interviewedBy;
        $household_info->committee = $request->committeeHealth;
        $household_info->date_profiled = $request->dateProfiled;
        $household_info->nhts = $request->nhtsRadioOptions;
        $household_info->nhts_no = $request->nhtsID;
        $household_info->ip = $request->ipRadioOptions;
        $household_info->non_nhts = $request->nonNhtsRadioOptions;
        $household_info->tribe = $request->tribe;
        $household_info->philhealth_no = $request->philhealth;
        $household_info->ip_no = $request->ipID;

        try {
            $household_info->save();
        } catch (\Exception $e) {
            return back()->with('addRecordFailed', 'Adding Household_Info failed.');
        }


        $household = new Household();
        $household->district = $request->municipalDistrict;
        $household->province = $request->provinceCity;
        $household->encoder = Auth::user()->id;
        $household->household_info_id = HouseholdInfo::latest()->first()->id;

        try {
            $household->save();
        } catch (\Exception $e) {
            return back()->with('addRecordFailed', 'Adding Household failed.');
        }



        $addedMembers = explode(',', $request->membersAdded);

        for ($index = 0; $index < count($addedMembers) - 1; $index++) {

            $personToAdd = explode('-', $addedMembers[$index]);
            $household_members = new HouseholdMember();
            $household_members->person_id = $personToAdd[0];
            $household_members->household_id = Household::latest()->first()->id;
            $household_members->relationship = $personToAdd[1];
            $household_members->family_head = ($personToAdd[2] === "true") ? 'Yes' : 'No';
            try {
                $household_members->save();
            } catch (\Exception $e) {
                return back()->with('addRecordFailed', 'Adding Household_Member failed.');
            }
        }


        $sanitation = new SanitationStatus();
        $sanitation->household_id = Household::latest()->first()->id;
        $sanitation->flush_toilet = (isset($request->flushedToilet)) ? 'Yes' : 'No';
        $sanitation->closed_pit_pervy = (isset($request->closedPitPrivy)) ? 'Yes' : 'No';
        $sanitation->communal_toilet = (isset($request->communalToilet)) ? 'Yes' : 'No';
        $sanitation->drop_overhung = (isset($request->dropOverhung)) ? 'Yes' : 'No';
        $sanitation->field_bodyOfWater = (isset($request->fieldBodyOfWater)) ? 'Yes' : 'No';
        $sanitation->community_water = (isset($request->communityWater)) ? 'Yes' : 'No';
        $sanitation->developed_spring = (isset($request->developedSpring)) ? 'Yes' : 'No';
        $sanitation->protected_well = (isset($request->protectedWell)) ? 'Yes' : 'No';
        $sanitation->truck_tanker_peddler = (isset($request->trunkPeddler)) ? 'Yes' : 'No';
        $sanitation->bottled_water = (isset($request->bottledWater)) ? 'Yes' : 'No';
        $sanitation->undeveloped_spring = (isset($request->undevelopedSpring)) ? 'Yes' : 'No';
        $sanitation->undeveloped_well = (isset($request->unprotectedWell)) ? 'Yes' : 'No';
        $sanitation->rainwater = (isset($request->rainwater)) ? 'Yes' : 'No';
        $sanitation->river_stream_dam = (isset($request->riverStreamDam)) ? 'Yes' : 'No';
        $sanitation->blind_drainage = $request->withBlindDrainageRadioOptions;
        $sanitation->score = $request->score;
        $sanitation->others = $request->others;

        // ----------- Save the new informations to databse -----------------
        try {
            $sanitation->save();
            return back()->with('addRecordSucess', 'Adding household record success.');
        } catch (\Exception $e) {
            return back()->with('addRecordFailed', 'Adding Sanitation record failed.');
        }
        // ========================== END ==========================
    }
}
