<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BarangayWorker;
use App\Models\Household;
use App\Models\HouseholdInfo;
use App\Models\HouseholdMember;
use App\Models\Purok;
use App\Models\SanitationStatus;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class TrashHouseholdController extends Controller
{
    public function trashHousehold(Request $request)
    {

        if (Hash::check($request->confirmation_password, Auth::user()->password)) {
            $sanitation = SanitationStatus::where([['household_id', $request->toRemoveHouseholdID], ['deleted_at', '=', NULL]])->first();
            try {
                $sanitation->delete();
            } catch (\Exception $e) {
                return back()->with('EditFailed', 'Something went wrong, trashing sanitation info failed.');
            }

            $household = Household::where([['id', $request->toRemoveHouseholdID], ['deleted_at', '=', NULL]])->first();
            try {
                $household->delete();
            } catch (\Exception $e) {
                return back()->with('EditFailed', 'Something went wrong, trashing household failed.');
            }

            $household_info = HouseholdInfo::where([['id', $request->toRemoveHouseholdInfoID], ['deleted_at', '=', NULL]])->first();
            try {
                $household_info->delete();
            } catch (\Exception $e) {
                return back()->with('EditFailed', 'Something went wrong, household info failed.');
            }
        } else {
            return back()->with('EditFailed', 'Password entered does not match.');
        }

        return redirect()->route('household', ['message' => 'Deleting household success.']);
    }

    public function trashHouseholdFromHouseholdPage(Request $request)
    {

        if (Hash::check($request->confirmation_password, Auth::user()->password)) {
            $sanitation = SanitationStatus::where([['household_id', $request->toRemoveHouseholdID], ['deleted_at', '=', NULL]])->first();
            try {
                $sanitation->delete();
            } catch (\Exception $e) {
                return back()->with('addRecordFailed', 'Something went wrong, trashing sanitation info failed.');
            }

            $household = Household::where([['id', $request->toRemoveHouseholdID], ['deleted_at', '=', NULL]])->first();
            try {
                $household->delete();
            } catch (\Exception $e) {
                return back()->with('addRecordFailed', 'Something went wrong, trashing household failed.');
            }

            $household_info = HouseholdInfo::where([['id', $request->toRemoveHouseholdInfoID], ['deleted_at', '=', NULL]])->first();
            try {
                $household_info->delete();
            } catch (\Exception $e) {
                return back()->with('addRecordFailed', 'Something went wrong, household info failed.');
            }
        } else {
            return back()->with('addRecordFailed', 'Password entered does not match.');
        }

        return redirect()->route('household', ['message' => 'Deleting household success.']);
    }
}
