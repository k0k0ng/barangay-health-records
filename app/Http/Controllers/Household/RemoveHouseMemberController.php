<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;

use App\Models\HouseholdMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RemoveHouseMemberController extends Controller
{
    public function removeHouseMember(Request $request)
    {
        if (!Hash::check($request->confirmation_password, Auth::user()->password)) return back()->with('EditFailed', 'Password entered does not match.');

        // Removing family member
        $memberToRemove = HouseholdMember::where([['household_id', $request->toRemoveMemberHouseholdID], ['person_id', $request->toRemoveMemberID]])->first();

        try {
            $memberToRemove->delete();
            return back()->with('updateSucess', 'Removing family member success.');
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Removing family member error.');
        }
        // === END ===
    }
}
