<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BarangayWorker;
use App\Models\Household;
use App\Models\HouseholdInfo;
use App\Models\HouseholdMember;
use App\Models\Purok;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HouseholdController extends Controller
{

    public function index(Request $request)
    {

        // -------------- Get Current Users Barangay ID -------------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // ============ END Get Current Users Barangay ID ============


        // --------- Get all household info -----------
        $household_infos = Household::addSelect([
            'head_ID' => HouseholdMember::select('person_id')
                ->whereColumn('households.id', '=', 'household_id')
                ->where('family_head', 'Yes'),
            'head_FirstName' => UserInfo::select('first_name')
                ->whereColumn('head_ID', 'id'),
            'head_MiddleName' => UserInfo::select('middle_name')
                ->whereColumn('head_ID', 'id'),
            'head_LastName' => UserInfo::select('last_name')
                ->whereColumn('head_ID', 'id'),
            'household_Info_ID' => HouseholdInfo::select('id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'barangay_ID' => HouseholdInfo::select('brgy_id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'purok_ID' => HouseholdInfo::select('purok_id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'barangay_name' => Barangay::select('brgy_name')
                ->whereColumn('barangay_info.id', 'barangay_ID'),
            'purok_name' => Purok::select('name')
                ->whereColumn('purok.id', 'purok_ID'),
            'interviewer_ID' => HouseholdInfo::select('interviewed_by')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'interviewer_FirstName' => BarangayWorker::select('firstname')
                ->whereColumn('barangay_workers.id', "=", 'interviewer_ID')
                ->withTrashed(),
            'interviewer_LastName' => BarangayWorker::select('lastname')
                ->whereColumn('barangay_workers.id', 'interviewer_ID')
                ->withTrashed(),
            'midwife_ID' => HouseholdInfo::select('midwife_ndp_assigned')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'midwife_FirstName' => BarangayWorker::select('firstname')
                ->whereColumn('barangay_workers.id', 'midwife_ID')
                ->withTrashed(),
            'midwife_LastName' => BarangayWorker::select('lastname')
                ->whereColumn('barangay_workers.id', 'midwife_ID')
                ->withTrashed()
        ])->sortable()->paginate(12);
        // === END ===

        // ------------------- Get barangay and purok info -------------------
        $barangay_infos = Purok::where('brgy_id', '=', $users_brgy_id)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================


        // ------------------- Get barangay official info -------------------
        $brgy_official_IDs = Barangay::select('info_wad1', 'info_wad2', 'info_wad3', 'info_wad4', 'info_wad5', 'info_wad6', 'info_wad7', 'info_sec')->where('id', $users_brgy_id)->get();


        $barangay_chairman = "";
        if (!$barangay_infos->isEmpty() && ($barangay_infos[0]->barangay_chairman_ID != 0 || $barangay_infos[0]->barangay_chairman_ID != NULL)) {
            $barangay_chairman = UserInfo::where('id', $barangay_infos[0]->barangay_chairman_ID)->first();
        }

        $barangay_officials = array($barangay_chairman, '', '', '', '', '', '', '', '');
        if ($brgy_official_IDs[0]->info_wad1 != 0 || $brgy_official_IDs[0]->info_wad1 != NULL) $barangay_officials[1] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad1)->first();
        if ($brgy_official_IDs[0]->info_wad2 != 0 || $brgy_official_IDs[0]->info_wad2 != NULL) $barangay_officials[2] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad2)->first();
        if ($brgy_official_IDs[0]->info_wad3 != 0 || $brgy_official_IDs[0]->info_wad3 != NULL) $barangay_officials[3] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad3)->first();
        if ($brgy_official_IDs[0]->info_wad4 != 0 || $brgy_official_IDs[0]->info_wad4 != NULL) $barangay_officials[4] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad4)->first();
        if ($brgy_official_IDs[0]->info_wad5 != 0 || $brgy_official_IDs[0]->info_wad5 != NULL) $barangay_officials[5] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad5)->first();
        if ($brgy_official_IDs[0]->info_wad6 != 0 || $brgy_official_IDs[0]->info_wad6 != NULL) $barangay_officials[6] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad6)->first();
        if ($brgy_official_IDs[0]->info_wad7 != 0 || $brgy_official_IDs[0]->info_wad7 != NULL) $barangay_officials[7] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad7)->first();
        if ($brgy_official_IDs[0]->info_sec != 0 || $brgy_official_IDs[0]->info_sec != NULL) $barangay_officials[8] = UserInfo::where('id', $brgy_official_IDs[0]->info_sec)->first();
        // ================= END Get barangay official info =================

        // ------------------- Get other barangay workers -------------------
        $barangay_workers = BarangayWorker::where('brgyID', '=', $users_brgy_id)->get();
        // === END ===

        // ------------------- Get all residence profile of current barangay for add household member ------------------- 
        $resident_infos = UserInfo::where('brgy_id', '=', $users_brgy_id)->get();
        // === END ===

        return view('pages.household', [
            'resident_infos' => $resident_infos,
            'household_infos' => $household_infos,
            'barangay_infos' => $barangay_infos,
            'barangay_chairman' => $barangay_chairman,
            'barangay_officials' => $barangay_officials,
            'barangay_workers' => $barangay_workers,
            'message' => $request->message,
            'users_image' => $users_image
        ]);
    }


    public function searchHousehold(Request $request)
    {
        // -------------- Get Current Users Barangay ID -------------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // ============ END Get Current Users Barangay ID ============


        // --------- Get all household info -----------
        $household_infos = Household::addSelect([
            'head_ID' => HouseholdMember::select('person_id')
                ->whereColumn('households.id', '=', 'household_id')
                ->where('family_head', 'Yes'),
            'head_FirstName' => UserInfo::select('first_name')
                ->whereColumn('head_ID', 'id'),
            'head_MiddleName' => UserInfo::select('middle_name')
                ->whereColumn('head_ID', 'id'),
            'head_LastName' => UserInfo::select('last_name')
                ->whereColumn('head_ID', 'id'),
            'household_Info_ID' => HouseholdInfo::select('id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'barangay_ID' => HouseholdInfo::select('brgy_id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'purok_ID' => HouseholdInfo::select('purok_id')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'barangay_name' => Barangay::select('brgy_name')
                ->whereColumn('barangay_info.id', 'barangay_ID'),
            'purok_name' => Purok::select('name')
                ->whereColumn('purok.id', 'purok_ID'),
            'interviewer_ID' => HouseholdInfo::select('interviewed_by')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'interviewer_FirstName' => BarangayWorker::select('firstname')
                ->whereColumn('barangay_workers.id', "=", 'interviewer_ID')
                ->withTrashed(),
            'interviewer_LastName' => BarangayWorker::select('lastname')
                ->whereColumn('barangay_workers.id', 'interviewer_ID')
                ->withTrashed(),
            'midwife_ID' => HouseholdInfo::select('midwife_ndp_assigned')
                ->whereColumn('household_infos.id', 'household_info_id'),
            'midwife_FirstName' => BarangayWorker::select('firstname')
                ->whereColumn('barangay_workers.id', 'midwife_ID')
                ->withTrashed(),
            'midwife_LastName' => BarangayWorker::select('lastname')
                ->whereColumn('barangay_workers.id', 'midwife_ID')
                ->withTrashed()
        ])->orderBy('created_at', 'asc')->paginate(12);
        // === END ===


        // ------------------- Get barangay and purok info -------------------
        $barangay_infos = Purok::where('brgy_id', '=', $users_brgy_id)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================


        // ------------------- Get barangay official info -------------------
        $brgy_official_IDs = Barangay::select('info_wad1', 'info_wad2', 'info_wad3', 'info_wad4', 'info_wad5', 'info_wad6', 'info_wad7', 'info_sec')->where('id', $users_brgy_id)->get();


        $barangay_chairman = "";
        if (!$barangay_infos->isEmpty() && ($barangay_infos[0]->barangay_chairman_ID != 0 || $barangay_infos[0]->barangay_chairman_ID != NULL)) {
            $barangay_chairman = UserInfo::where('id', $barangay_infos[0]->barangay_chairman_ID)->first();
        }

        $barangay_officials = array($barangay_chairman, '', '', '', '', '', '', '', '');
        if ($brgy_official_IDs[0]->info_wad1 != 0 || $brgy_official_IDs[0]->info_wad1 != NULL) $barangay_officials[1] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad1)->first();
        if ($brgy_official_IDs[0]->info_wad2 != 0 || $brgy_official_IDs[0]->info_wad2 != NULL) $barangay_officials[2] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad2)->first();
        if ($brgy_official_IDs[0]->info_wad3 != 0 || $brgy_official_IDs[0]->info_wad3 != NULL) $barangay_officials[3] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad3)->first();
        if ($brgy_official_IDs[0]->info_wad4 != 0 || $brgy_official_IDs[0]->info_wad4 != NULL) $barangay_officials[4] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad4)->first();
        if ($brgy_official_IDs[0]->info_wad5 != 0 || $brgy_official_IDs[0]->info_wad5 != NULL) $barangay_officials[5] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad5)->first();
        if ($brgy_official_IDs[0]->info_wad6 != 0 || $brgy_official_IDs[0]->info_wad6 != NULL) $barangay_officials[6] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad6)->first();
        if ($brgy_official_IDs[0]->info_wad7 != 0 || $brgy_official_IDs[0]->info_wad7 != NULL) $barangay_officials[7] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad7)->first();
        if ($brgy_official_IDs[0]->info_sec != 0 || $brgy_official_IDs[0]->info_sec != NULL) $barangay_officials[8] = UserInfo::where('id', $brgy_official_IDs[0]->info_sec)->first();
        // ================= END Get barangay official info =================

        // ------------------- Get other barangay workers -------------------
        $barangay_workers = BarangayWorker::where('brgyID', '=', $users_brgy_id)->get();
        // === END ===

        // ------------------- Get all residence profile of current barangay for add household member ------------------- 
        $resident_infos = UserInfo::where('brgy_id', '=', $users_brgy_id)->get();
        // === END ===

        return view('pages.household', [
            'resident_infos' => $resident_infos,
            'household_infos' => $household_infos,
            'barangay_infos' => $barangay_infos,
            'barangay_chairman' => $barangay_chairman,
            'barangay_officials' => $barangay_officials,
            'barangay_workers' => $barangay_workers,
            'message' => $request->message,
            'users_image' => $users_image
        ]);
    }
}
