<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\HouseholdMember;
use Illuminate\Http\Request;

class AppointHouseHeadController extends Controller
{
    public function appointHead(Request $request)
    {
        // Demoting family head
        $memberToDemote = HouseholdMember::where([['household_id', $request->selectedHouseholdID], ['family_head', 'Yes']])->first();

        try {
            $memberToDemote->family_head = "No";
            $memberToDemote->save();
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating old head error.');
        }
        // === END ===

        // Updating new family head
        $memberToAppoint = HouseholdMember::where([['person_id', $request->memberID], ['household_id', $request->selectedHouseholdID]])->first();

        try {
            $memberToAppoint->family_head = "Yes";
            $memberToAppoint->save();
            return back()->with('updateSucess', 'Updating new head success.');
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating new head error.');
        }
        // === END ===
    }
}
