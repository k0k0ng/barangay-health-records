<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\Household;
use App\Models\HouseholdMember;
use Illuminate\Http\Request;

class AddHouseholdMemberController extends Controller
{
    public function addHouseholdMember(Request $request)
    {

        $toAddMembers = explode(',', $request->membersToAdd);

        for ($index = 0; $index < count($toAddMembers) - 1; $index++) {

            $personToAdd = explode('-', $toAddMembers[$index]);
            $household_members = new HouseholdMember();
            $household_members->person_id = $personToAdd[0];
            $household_members->household_id = $request->memberToAddHouseholdID;
            $household_members->relationship = $personToAdd[1];
            $household_members->family_head = 'No';
            try {
                $household_members->save();
            } catch (\Exception $e) {
                return back()->with('EditFailed', 'Adding Household Member failed.');
            }
        }
        return back()->with('updateSucess', 'Adding Household Member success.');
    }
}
