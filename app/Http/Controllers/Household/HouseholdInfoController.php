<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BarangayWorker;
use App\Models\Purok;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isEmpty;

class HouseholdInfoController extends Controller
{

    public function viewRecord(Request $request)
    {

        // -------------- Get Current Users Barangay ID -------------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;
        // ============ END Get Current Users Barangay ID ============

        $household_infos = DB::table('household_infos')
            ->where([['household_infos.id', $request->selectedHouseholdID], ['household_infos.deleted_at', '=', NULL]])
            ->join('households', 'household_infos.id', '=', 'households.household_info_id')
            ->where('households.deleted_at', '=', NULL)
            ->join('household_members', 'households.id', '=', 'household_members.household_id')
            ->where('household_members.deleted_at', '=', NULL)
            ->join('sanitation_statuses', 'households.id', '=', 'sanitation_statuses.household_id')
            ->join('user_info', 'household_members.person_id', '=', 'user_info.id')
            ->join('purok', 'household_infos.purok_id', '=', 'purok.id')
            ->join('barangay_info', 'household_infos.brgy_id', '=', 'barangay_info.id')
            ->select(
                'household_infos.*',
                'household_infos.id as HouseholdInfo_ID',
                'households.id as Household_ID',
                'households.encoder',
                'households.district',
                'households.province',
                'household_members.family_head',
                'household_members.relationship',
                'sanitation_statuses.*',
                'user_info.id AS Person_ID',
                'user_info.first_name',
                'user_info.middle_name',
                'user_info.last_name',
                'user_info.occupation',
                'user_info.gender',
                'purok.name as purok_name',
                'barangay_info.brgy_name'

            )
            ->orderBy('household_members.family_head')
            ->get();


        if (!$household_infos->isEmpty()) {
            $midwife_info = BarangayWorker::where('id', $household_infos[0]->midwife_ndp_assigned)->withTrashed()->first();
            $interviewer_info = BarangayWorker::where('id', $household_infos[0]->interviewed_by)->withTrashed()->first();
            $encoder_info = DB::table('account')
                ->where('account.id', $household_infos[0]->encoder)
                ->join('user_info', 'account.info_id', '=', 'user_info.id')
                ->select('account.id', 'user_info.id as Encoder_Person_ID', 'user_info.first_name', 'user_info.middle_name', 'user_info.last_name')
                ->first();

            // ------------------- Get barangay and purok info -------------------
            $barangay_infos = Purok::where('brgy_id', '=', $users_brgy_id)
                ->addSelect([
                    'barangay_ID' => Barangay::select('id')
                        ->whereColumn('barangay_info.id', 'brgy_id'),
                    'barangay_name' => Barangay::select('brgy_name')
                        ->whereColumn('barangay_info.id', 'brgy_id'),
                    'barangay_chairman_ID' => Barangay::select('info_cap')
                        ->whereColumn('barangay_info.id', 'brgy_id')
                ])->get();
            // ================= END Get barangay and purok info ==================

            // ------------------- Get barangay official info -------------------
            $brgy_official_IDs = Barangay::select('info_wad1', 'info_wad2', 'info_wad3', 'info_wad4', 'info_wad5', 'info_wad6', 'info_wad7', 'info_sec')->where('id', $users_brgy_id)->get();


            $barangay_chairman = "";
            if (!$barangay_infos->isEmpty() && ($barangay_infos[0]->barangay_chairman_ID != 0 || $barangay_infos[0]->barangay_chairman_ID != NULL)) {
                $barangay_chairman = UserInfo::where('id', $barangay_infos[0]->barangay_chairman_ID)->first();
            }

            $barangay_officials = array($barangay_chairman, '', '', '', '', '', '', '', '');
            if ($brgy_official_IDs[0]->info_wad1 != 0 || $brgy_official_IDs[0]->info_wad1 != NULL) $barangay_officials[1] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad1)->first();
            if ($brgy_official_IDs[0]->info_wad2 != 0 || $brgy_official_IDs[0]->info_wad2 != NULL) $barangay_officials[2] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad2)->first();
            if ($brgy_official_IDs[0]->info_wad3 != 0 || $brgy_official_IDs[0]->info_wad3 != NULL) $barangay_officials[3] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad3)->first();
            if ($brgy_official_IDs[0]->info_wad4 != 0 || $brgy_official_IDs[0]->info_wad4 != NULL) $barangay_officials[4] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad4)->first();
            if ($brgy_official_IDs[0]->info_wad5 != 0 || $brgy_official_IDs[0]->info_wad5 != NULL) $barangay_officials[5] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad5)->first();
            if ($brgy_official_IDs[0]->info_wad6 != 0 || $brgy_official_IDs[0]->info_wad6 != NULL) $barangay_officials[6] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad6)->first();
            if ($brgy_official_IDs[0]->info_wad7 != 0 || $brgy_official_IDs[0]->info_wad7 != NULL) $barangay_officials[7] = UserInfo::where('id', $brgy_official_IDs[0]->info_wad7)->first();
            if ($brgy_official_IDs[0]->info_sec != 0 || $brgy_official_IDs[0]->info_sec != NULL) $barangay_officials[8] = UserInfo::where('id', $brgy_official_IDs[0]->info_sec)->first();
            // ================= END Get barangay official info =================

            // ------------------- Get other barangay workers -------------------
            $barangay_workers = BarangayWorker::where('brgyID', '=', $users_brgy_id)->get();
            // === END ===

            // ------------------- Get all residence profile of current barangay for add household member ------------------- 
            $household_Member_IDs = [];
            foreach ($household_infos as $household_info) {
                array_push($household_Member_IDs, $household_info->Person_ID);
            }

            $not_member_residents = UserInfo::where('brgy_id', '=', $users_brgy_id)
                ->whereNotIn(
                    "id",
                    $household_Member_IDs
                )->get();
            // === END ===

            return view('pages.household_info', [
                'household_infos' => $household_infos,
                'encoder_info' => $encoder_info,
                'midwife_info' => $midwife_info,
                'interviewer_info' => $interviewer_info,
                'barangay_infos' => $barangay_infos,
                'barangay_workers' => $barangay_workers,
                'not_member_residents' => $not_member_residents,
                'users_image' => $users_image
            ]);
        } else {
            return back()->with('addRecordFailed', 'Getting household info failed. Household info is incomplete.');
        }
    }
}
