<?php

namespace App\Http\Controllers\Household;

use App\Http\Controllers\Controller;
use App\Models\BarangayWorker;
use App\Models\Household;
use App\Models\HouseholdInfo;
use App\Models\SanitationStatus;
use Illuminate\Http\Request;

class EditHouseholdController extends Controller
{
    public function editHousehold(Request $request)
    {

        $sanitation_status = SanitationStatus::where('household_id', $request->household_ID)->first();
        $sanitation_status->flush_toilet = (isset($request->flushedToilet)) ? 'Yes' : 'No';
        $sanitation_status->closed_pit_pervy = (isset($request->closedPitPrivy)) ? 'Yes' : 'No';
        $sanitation_status->communal_toilet = (isset($request->communalToilet)) ? 'Yes' : 'No';
        $sanitation_status->drop_overhung = (isset($request->dropOverhung)) ? 'Yes' : 'No';
        $sanitation_status->field_bodyOfWater = (isset($request->fieldBodyOfWater)) ? 'Yes' : 'No';
        $sanitation_status->community_water = (isset($request->communityWater)) ? 'Yes' : 'No';
        $sanitation_status->developed_spring = (isset($request->developedSpring)) ? 'Yes' : 'No';
        $sanitation_status->protected_well = (isset($request->protectedWell)) ? 'Yes' : 'No';
        $sanitation_status->truck_tanker_peddler = (isset($request->trunkPeddler)) ? 'Yes' : 'No';
        $sanitation_status->bottled_water = (isset($request->bottledWater)) ? 'Yes' : 'No';
        $sanitation_status->undeveloped_spring = (isset($request->undevelopedSpring)) ? 'Yes' : 'No';
        $sanitation_status->undeveloped_well = (isset($request->unprotectedWell)) ? 'Yes' : 'No';
        $sanitation_status->rainwater = (isset($request->rainwater)) ? 'Yes' : 'No';
        $sanitation_status->river_stream_dam = (isset($request->riverStreamDam)) ? 'Yes' : 'No';
        $sanitation_status->blind_drainage = $request->withBlindDrainageRadioOptions;
        $sanitation_status->score = $request->score;
        $sanitation_status->others = $request->others;

        // ----------- Update sanitation status info to database -----------------
        try {
            $sanitation_status->save();
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating Sanitation record failed.');
        }
        // ========================== END ==========================


        $household = Household::where('id', $request->household_ID)->first();
        $household->district = $request->municipalDistrict;
        $household->province = $request->provinceCity;

        // ----------- Update sanitation status info to database -----------------
        try {
            $household->save();
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating household record failed.');
        }
        // ========================== END ==========================


        $household_info = HouseholdInfo::where('id', $request->household_Info_ID)->first();
        $household_info->brgy_id = $request->Barangay_ID;
        $household_info->brgy_chairman_id = $request->barangayChairman;
        $household_info->midwife_ndp_assigned  = $request->midwife;
        $household_info->purok_id = $request->purok;
        $household_info->interviewed_by = $request->interviewedBy;
        $household_info->committee = $request->committeeHealth;
        $household_info->date_profiled = $request->dateProfiled;
        $household_info->nhts = $request->nhtsRadioOptions;
        $household_info->nhts_no = $request->nhtsID;
        $household_info->ip = $request->ipRadioOptions;
        $household_info->non_nhts = $request->nonNhtsRadioOptions;
        $household_info->tribe = $request->tribe;
        $household_info->philhealth_no = $request->philhealth;
        $household_info->ip_no = $request->ipID;

        // ----------- Update sanitation status info to database -----------------
        try {
            $household_info->save();
        } catch (\Exception $e) {
            return back()->with('EditFailed', 'Updating household record failed.');
        }
        // ========================== END ==========================


        return back()->with('updateSucess', 'Updating household record success.');
    }
}
