<?php

namespace App\Http\Controllers\PrintingControllers;

use App\Http\Controllers\Controller;
use App\Models\SANITATIONReport;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use \PDF;

class PrintSnitationRecordController extends Controller
{
    public function index(Request $request)
    {
        $resident_info = UserInfo::where('id', $request->Print_Records_ResidentID)->first();

        $Sanitation_Record = SANITATIONReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();

        $snappy = PDF::loadView('toPrint.print_resident_sanitation_records', ["resident" => $resident_info, 'all_records' => $Sanitation_Record, 'type' => "Sanitation"]);

        $snappy->setOptions([
            'margin-top' => 15,
            'margin-bottom' => 27,
            'margin-left' => 11.5,
            'margin-right' => 11.5,
            'footer-left' => 'Page [page] of [toPage]',
            'footer-right' => '[isodate]',
            'footer-font-size' => 10,
            'footer-spacing' => 6
        ]);

        $snappy->setOrientation('landscape');


        return $snappy->download($resident_info->first_name . ' ' . $resident_info->last_name . ' Sanitation Record.pdf');
    }
}
