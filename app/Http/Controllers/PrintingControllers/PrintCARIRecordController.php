<?php

namespace App\Http\Controllers\PrintingControllers;

use App\Http\Controllers\Controller;
use App\Models\CARIReport;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use \PDF;

class PrintCARIRecordController extends Controller
{
    public function index(Request $request)
    {
        $resident_info = UserInfo::where('id', $request->Print_Records_ResidentID)->first();

        $Cari_Record = CARIReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();

        $snappy = PDF::loadView('toPrint.print_resident_cari_records', ["resident" => $resident_info, 'all_records' => $Cari_Record, 'type' => "CARI"]);

        $snappy->setOptions([
            'margin-top' => 15,
            'margin-bottom' => 27,
            'margin-left' => 11.5,
            'margin-right' => 11.5,
            'footer-left' => 'Page [page] of [toPage]',
            'footer-right' => '[isodate]',
            'footer-font-size' => 10,
            'footer-spacing' => 6
        ]);

        $snappy->setOrientation('landscape');


        return $snappy->download($resident_info->first_name . ' ' . $resident_info->last_name . ' CARI Record.pdf');
    }
}
