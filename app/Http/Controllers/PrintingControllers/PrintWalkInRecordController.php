<?php

namespace App\Http\Controllers\PrintingControllers;

use App\Http\Controllers\Controller;
use App\Models\UserInfo;
use App\Models\Walkin;
use Illuminate\Http\Request;
use \PDF;

class PrintWalkInRecordController extends Controller
{
    public function index(Request $request)
    {
        $resident_info = UserInfo::where('id', $request->Print_Records_ResidentID)->first();

        $WalkIn_Record = Walkin::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();


        $snappy = PDF::loadView('toPrint.print_resident_walkin_records', ["resident" => $resident_info, 'all_records' => $WalkIn_Record, 'type' => "WalkIn"]);

        $snappy->setOptions([
            'margin-top' => 15,
            'margin-bottom' => 27,
            'margin-left' => 11.5,
            'margin-right' => 11.5,
            'footer-left' => 'Page [page] of [toPage]',
            'footer-right' => '[isodate]',
            'footer-font-size' => 10,
            'footer-spacing' => 6
        ]);

        $snappy->setOrientation('landscape');


        return $snappy->download($resident_info->first_name . ' ' . $resident_info->last_name . ' Walk-In Record.pdf');
    }
}
