<?php

namespace App\Http\Controllers\PrintingControllers;

use App\Http\Controllers\Controller;
use App\Models\Barangay;
use App\Models\BarangayWorker;
use App\Models\Purok;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \PDF;

class PrintHouseholdInfoController extends Controller
{
    public function index(Request $request)
    {
        // -------------- Get Current Users Barangay ID -------------
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_brgy_id = $users_brgy_id->brgy_id;
        // ============ END Get Current Users Barangay ID ============

        $household_infos = DB::table('household_infos')
            ->where([['household_infos.id', $request->Household_ID_To_Print], ['household_infos.deleted_at', '=', NULL]])
            ->join('households', 'household_infos.id', '=', 'households.household_info_id')
            ->where('households.deleted_at', '=', NULL)
            ->join('household_members', 'households.id', '=', 'household_members.household_id')
            ->where('household_members.deleted_at', '=', NULL)
            ->join('sanitation_statuses', 'households.id', '=', 'sanitation_statuses.household_id')
            ->join('user_info', 'household_members.person_id', '=', 'user_info.id')
            ->join('purok', 'household_infos.purok_id', '=', 'purok.id')
            ->join('barangay_info', 'household_infos.brgy_id', '=', 'barangay_info.id')
            ->select(
                'household_infos.*',
                'household_infos.id as HouseholdInfo_ID',
                'households.id as Household_ID',
                'households.encoder',
                'households.district',
                'households.province',
                'household_members.family_head',
                'household_members.relationship',
                'sanitation_statuses.*',
                'user_info.id AS Person_ID',
                'user_info.first_name',
                'user_info.middle_name',
                'user_info.last_name',
                'user_info.occupation',
                'user_info.gender',
                'purok.name as purok_name',
                'barangay_info.brgy_name'

            )
            ->orderBy('household_members.family_head')
            ->get();


        $midwife_info = BarangayWorker::where('id', $household_infos[0]->midwife_ndp_assigned)->withTrashed()->first();
        $interviewer_info = BarangayWorker::where('id', $household_infos[0]->interviewed_by)->withTrashed()->first();
        $encoder_info = DB::table('account')
            ->where('account.id', $household_infos[0]->encoder)
            ->join('user_info', 'account.info_id', '=', 'user_info.id')
            ->select('account.id', 'user_info.id as Encoder_Person_ID', 'user_info.first_name', 'user_info.middle_name', 'user_info.last_name')
            ->first();

        // ------------------- Get barangay and purok info -------------------
        $barangay_infos = Purok::where('brgy_id', '=', $users_brgy_id)
            ->addSelect([
                'barangay_ID' => Barangay::select('id')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_name' => Barangay::select('brgy_name')
                    ->whereColumn('barangay_info.id', 'brgy_id'),
                'barangay_chairman_ID' => Barangay::select('info_cap')
                    ->whereColumn('barangay_info.id', 'brgy_id')
            ])->get();
        // ================= END Get barangay and purok info ==================


        // ------------------- Get all residence profile of current barangay for add household member ------------------- 
        $household_Member_IDs = [];
        foreach ($household_infos as $household_info) {
            array_push($household_Member_IDs, $household_info->Person_ID);
        }

        $not_member_residents = UserInfo::where('brgy_id', '=', $users_brgy_id)
            ->whereNotIn(
                "id",
                $household_Member_IDs
            )->get();
        // === END ===

        $snappy = PDF::loadView('toPrint.print_household_info', [
            'household_infos' => $household_infos,
            'encoder_info' => $encoder_info,
            'midwife_info' => $midwife_info,
            'interviewer_info' => $interviewer_info
        ]);

        $snappy->setOptions([
            'margin-top' => 15,
            'margin-bottom' => 27,
            'margin-left' => 11.5,
            'margin-right' => 11.5,
            'footer-left' => 'Page [page] of [toPage]',
            'footer-right' => '[isodate]',
            'footer-font-size' => 10,
            'footer-spacing' => 6
        ]);

        return $snappy->download('Household Record.pdf');
    }
}
