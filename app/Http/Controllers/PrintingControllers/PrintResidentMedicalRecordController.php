<?php

namespace App\Http\Controllers\PrintingControllers;

use App\Http\Controllers\Controller;
use App\Models\BIPReport;
use App\Models\CARIReport;
use App\Models\CDDReport;
use App\Models\EPIReport;
use App\Models\FPReport;
use App\Models\GMSReport;
use App\Models\MCHReport;
use App\Models\Mortality;
use App\Models\PPReport;
use App\Models\RABIESReport;
use App\Models\SANITATIONReport;
use App\Models\TBSympReport;
use App\Models\UFCReport;
use App\Models\UserInfo;
use App\Models\Walkin;
use Illuminate\Http\Request;
use \PDF;

class PrintResidentMedicalRecordController extends Controller
{
    public function index(Request $request)
    {
        $resident_info = UserInfo::where('id', $request->Print_Records_ResidentID)->first();

        $all_records = [[]];
        $record_count = 0;

        $WalkIn_Record = Walkin::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($WalkIn_Record as $record) {
            $all_records[$record_count]['type'] = "Walk In";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $MCH_Record = MCHReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($MCH_Record as $record) {
            $all_records[$record_count]['type'] = "MCH";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $PP_Record = PPReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($PP_Record as $record) {
            $all_records[$record_count]['type'] = "PP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $EPI_Record = EPIReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($EPI_Record as $record) {
            $all_records[$record_count]['type'] = "EPI";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $UFC_Record = UFCReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($UFC_Record as $record) {
            $all_records[$record_count]['type'] = "UFC";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $FP_Record = FPReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($FP_Record as $record) {
            $all_records[$record_count]['type'] = "FP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $CDD_Record = CDDReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($CDD_Record as $record) {
            $all_records[$record_count]['type'] = "CDD";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Mortality_Record = Mortality::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Mortality_Record as $record) {
            $all_records[$record_count]['type'] = "Mortality";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $CARI_Record = CARIReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($CARI_Record as $record) {
            $all_records[$record_count]['type'] = "CARI";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $GMS_Record = GMSReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($GMS_Record as $record) {
            $all_records[$record_count]['type'] = "GMS";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $BIP_Record = BIPReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($BIP_Record as $record) {
            $all_records[$record_count]['type'] = "BIP";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $TB_Symp_Record = TBSympReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($TB_Symp_Record as $record) {
            $all_records[$record_count]['type'] = "TB Symp";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Rabies_Record = RABIESReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Rabies_Record as $record) {
            $all_records[$record_count]['type'] = "Rabies";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        $Sanitation_Record = SANITATIONReport::where([['resident_id', $request->Print_Records_ResidentID], ['deleted_at', NULL]])->orderBy('updated_at', 'desc')->get();
        foreach ($Sanitation_Record as $record) {
            $all_records[$record_count]['type'] = "Sanitation";
            $all_records[$record_count]['created_at'] = date('m/d/Y H:i', strtotime($record->created_at));
            $all_records[$record_count]['updated_at'] = date('m/d/Y H:i', strtotime($record->updated_at));
            $all_records[$record_count]['ID'] = $record->id;
            $record_count++;
        }

        // Sort All Records in Descending order
        if (!empty($all_records[0])) {
            $update = array_column($all_records, 'updated_at');
            array_multisort($update, SORT_DESC, $all_records);
        }
        // === END ===

        $snappy = PDF::loadView('toPrint.resident_medical_records', ["resident" => $resident_info, 'all_records' => $all_records]);

        $snappy->setOptions([
            'margin-top' => 15,
            'margin-bottom' => 27,
            'margin-left' => 11.5,
            'margin-right' => 11.5,
            'footer-left' => 'Page [page] of [toPage]',
            'footer-right' => '[isodate]',
            'footer-font-size' => 10,
            'footer-spacing' => 6
        ]);


        return $snappy->download($resident_info->first_name . ' ' . $resident_info->last_name . ' Medical Record.pdf');
    }
}
