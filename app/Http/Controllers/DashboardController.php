<?php

namespace App\Http\Controllers;

use App\Models\Mortality;
use App\Models\UserInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {
        $users_brgy_id = UserInfo::where('id', '=', Auth::user()->info_id)->first();
        $users_image = $users_brgy_id->img_name . "." . $users_brgy_id->img_extension;
        $users_brgy_id = $users_brgy_id->brgy_id;

        $residents = UserInfo::where('brgy_id', '=', $users_brgy_id)->get();

        $numofperson = 0;
        $numofMale = 0;
        $numofFemale = 0;
        $numofUndifined = 0;

        foreach ($residents as $resident) {
            $numofperson++;
            if ($resident->gender == 'Male') {
                $numofMale++;
            } elseif ($resident->gender == 'Female') {
                $numofFemale++;
            } else {
                $numofUndifined++;
            }
        };

        $households = DB::table('household_infos')
            ->join('households', 'household_infos.id', '=', 'households.household_info_id')
            ->join('purok', 'household_infos.purok_id', '=', 'purok.id')
            ->join('barangay_info', 'purok.brgy_id', '=', 'barangay_info.id')
            ->join('household_members', 'households.id', '=', 'household_members.household_id')->where('household_members.family_head', '=', 'Yes')
            ->join('user_info', 'household_members.person_id', '=', 'user_info.id')
            ->select('purok.name', 'barangay_info.brgy_name', 'user_info.first_name', 'user_info.last_name')
            ->where('household_infos.brgy_id', '=', $users_brgy_id)
            ->orderByDesc('household_infos.id')
            ->limit(8)
            ->get();

        $from = date("Y-m-1", strtotime("-5 months"));
        $to = date("Y-m-d");

        $deathRecords = DB::table('user_info')
            ->join('mortality', 'user_info.id', '=', 'mortality.resident_id')
            ->where('user_info.brgy_id', '=', $users_brgy_id)
            ->where('mortality.deleted_at', '=', NULL)
            ->whereBetween('mortality.DOD', [$from, $to])
            ->get();

        $deathPerMonth = [0, 0, 0, 0, 0, 0];
        $startMonth = date("n", strtotime($from));

        for ($index = 0; $index < 6; $index++) {
            foreach ($deathRecords as $deathRecord) {
                if ($startMonth == date("n", strtotime($deathRecord->DOD))) {
                    $deathPerMonth[$index]++;
                }
            };
            $startMonth++;
        }






        return view('pages.dashboard', [
            'residents' => $numofperson,
            'male' => $numofMale,
            'female' => $numofFemale,
            'undifined' => $numofUndifined,
            'households' => $households,
            'deathPerMonth' => $deathPerMonth,
            'users_image' => $users_image
        ]);
    }
}
