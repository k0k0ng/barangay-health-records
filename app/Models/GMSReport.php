<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GMSReport extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'gms_report';
}
