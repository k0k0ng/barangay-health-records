<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SanitationOptions extends Model
{
    use HasFactory;



    public $table = 'sanitation_options';
}
