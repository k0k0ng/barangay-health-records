<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class HouseholdMember extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Sortable;

    public $table = 'household_members';

    public $Sortable = ['id', 'person_id', 'first_name', 'lastName', 'gender', 'address'];
}
