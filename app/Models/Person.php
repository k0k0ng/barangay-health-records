<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Person extends Model
{
    use HasFactory;
    use Sortable;

    public $table = "persons";

    public $Sortable = ['id', 'firstName', 'midName', 'lastName', 'gender', 'address'];
}
