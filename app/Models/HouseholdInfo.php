<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class HouseholdInfo extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Sortable;

    public $table = 'household_infos';

    public $Sortable = ['id', 'brgy_chairman_id', 'midwife_ndp_assigned	', 'interviewed_by'];
}
