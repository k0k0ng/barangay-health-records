<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class UserInfo extends Model
{
    use HasFactory;
    use Sortable;

    public $table = "user_info";

    public $Sortable = ['first_name', 'last_name', 'gender', 'address'];


    public function admin()
    {
        return $this->hasOne(Account::class, 'info_id', 'id');
    }
}
