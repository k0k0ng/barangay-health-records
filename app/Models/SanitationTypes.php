<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SanitationTypes extends Model
{
    use HasFactory;



    public $table = 'sanitation_types';
}
