<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Household extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Sortable;

    public $table = 'households';

    public $Sortable = ['id', 'district', 'province', 'encoder'];
}
