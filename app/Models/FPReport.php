<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FPReport extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'fp_report';
}
