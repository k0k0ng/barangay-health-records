<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL('img/favicon.png')}}">
    <title>Barangay Health Records</title> 
    
    

    
    {{-- Bootstrap --}}
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
        {{-- Bootstrap JS for collapsable side bar --}}
    <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
        {{-- Bootstrap JS for toggling tabs in Add Info --}}
    <script src="{{ asset('js/bootstrap-4.1.3.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- Bootstrap --}}

    {{-- Custom Made CSS --}}
    <link href="{{ asset('css/custom-style.css') }}" rel="stylesheet">


    {{-- Link Google Font --}}
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />


    {{-- Icons Link --}}
    {{-- Used in tables and Creating Household Steps --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />

    {{-- Date picker dependencies --}}
    <link href="{{ asset('css/date-picker-styles.css') }}" rel="stylesheet">
    {{-- END Date picker dependencies --}}
    


    {{-- ChartJs --}}
    <script src="{{ asset('js/chartjs.min.js') }}"></script>

</head>
<body class="bg-secondary bg-opacity-10">
    
    @auth
        
        <nav class="fixed-top d-flex justify-content-between z-50 h-20 bg-white px-4">
            <div>
                <a href="{{ route('dashboard') }}"><img src="{{URL('img/nav-logo.png')}}" alt=""></a>

                <button type="button" id="sidebarCollapse" class="burger-btn">
                    <div></div>
                    <div></div>
                    <div></div>
                </button>
            </div>

            <div class="d-flex align-items-center">
                <div class="dropdown">
                    <button class="btn btn-light rounded-circle p-0 overflow-hidden" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        @php
                            $path = public_path().'/img/'.$users_image;
                        @endphp

                        @if (File::exists($path) && $users_image != ".")
                            <img src="{{ URL('img/'.$users_image) }}" alt="profile photo" style="width:50px;">
                        @else
                            <img src="{{ URL('img/avatar-vector-icon-boy.png') }}" alt="profile photo" style="width:50px;">
                        @endif

                        
                    </button>
                    <ul class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton1">
                      <div class="py-2 text-center font-semibold bg-gray-400">Settings</div>
                      <hr class="m-0">
                      <li class="logout-hover">
                        <form id="ViewProfileForm" method="GET" action="{{ route('ViewResidentProfile') }}" role="View Resident Profile">
                            <input class="visually-hidden" id="residentProfileSelectedID" name="residentProfileSelectedID" value='{{ Auth::user()->info_id }}'>
                            <button type="submit" class="btn btn-light d-flex justify-content-center px-4 pt-3 pb-2 text-sm font-semibold text-center w-100">
                                <p class="signout-text m-0 text-sm text-gray-600">
                                    View Profile
                                </p>
                            </button>
                        </form>
                      </li>
                      <li class="logout-hover">
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-light d-flex justify-content-center px-4 pb-3 text-sm font-semibold text-center w-100">
                                <p class="signout-text m-0 text-sm text-gray-600">
                                    Sign Out
                                </p>
                            </button>
                        </form>
                      </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="wrapper">
            <!-- Sidebar -->
            <nav id="sidebar" class="pt-20 bg-gray-800 z-10 h-100">
                <ul class="list-unstyled components side-nav">
                    <li id="dashboard-nav" class="nav-menu">
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <p class="text-xs">OPTIONS</p>
                    <li class="nav-menu">
                        <a href="{{ route('resident_list') }}">Resident Profile</a>
                    </li>
                    <li id="household-nav" class="nav-menu">
                        <a href="{{ route('household') }}">Household</a>
                    </li>
                    {{-- <li class="nav-menu">
                        <a href="{{ route('reports') }}">Reports</a>
                    </li> --}}
                    <li class="nav-menu">
                        <a href="{{ route('barangay') }}">Barangay Info</a>
                    </li>
                </ul>
            </nav>

            <div id="content">
                <div>
                    @yield('content')
                </div>
            </div>
        
        </div>

        <div class="cd-fixed bottom-0 w-100 bg-white d-flex justify-content-center py-2 z-50 shadow-lg">
            <div class="text-sm text-center text-gray-800 font-bold">
                University of Southeastern Philippines  •  Systems and Data Management Division
            </div>
        </div>

    @endauth


    @guest
        <div class="d-flex flex-wrap h-screen w-100 justify-content-center align-items-center">
            @yield('content')
        </div>
    @endguest
    
    {{-- Script for SideBar Collapse --}}
    <script>
        $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
            $('#content').toggleClass('active');
        });

        $('#dashboard-nav').on('click', function () {
            this.toggleClass('active');
        });

        });
    </script>
    {{-- End Script for SideBar Collapse --}}
    
</body>
</html>