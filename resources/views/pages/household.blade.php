@extends('layouts.app')

@section('content')

    <div class="h-full mb-5">
        @if (session('addRecordSucess'))
            <div class="alert alert-success d-flex justify-content-between" role="alert">
                <p class="text-sm mb-0">{{ session('addRecordSucess') }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div> 
        @endif
        @if (session('addRecordFailed'))
            <div class="alert alert-danger d-flex justify-content-between" role="alert">
                <p class="text-sm mb-0">{{ session('addRecordFailed') }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div> 
        @endif
        @if ( isset($message) )
            <div class="alert alert-success d-flex justify-content-between" role="alert">
                <p class="text-sm mb-0">{{ $message }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div> 
        @endif
        

        <div class="w-full bg-white p-3 rounded border-2 h-full">
            
            <div class="d-flex justify-content-between py-2">
                <div class="px-1 pt-2 font-semibold text-gray-600 text-sm">
                    Household List
                </div>
            </div>

            <div class="d-flex flex-column">
                <div class="overflow-auto">
                    <div class="py-2 inline-block min-w-full">
                  
                        <div class="d-flex justify-content-between py-2 ps-1 mb-3">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary text-white CreateHouseholdBTN" data-bs-toggle="modal" data-bs-target="#createHousehold">
                                <span class="text-lg align-middle">+</span> Create Household
                            </button>

                            {{-- Search Household Functionality (incomplete)
                            <form method="POST" action="{{ route('searchHousehold') }}" role="search" class="d-flex w-30p px-2 search-household-form" autocomplete="off">
                                @csrf
                                <input type="search" name="searchedKey" id="searchedKey" class="form-control" placeholder="Search Resident" aria-label="Search Resident" aria-describedby="search-name-btn">
                                <button class="btn btn-primary text-white" type="submit" id="search-name-btn">Search</button>
                            </form> --}}
                            
                        </div>
                        <div class="d-block overflow-auto">
                            <table id="household_table" class="w-100 table table-bordered table-hover table-responsive">
                                <thead class="bg-gray-800 text-white">
                                <tr>
                                    <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                       Household Head
                                    </th>
                                    <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                        Interviewed by
                                    </th>
                                    <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                       Address
                                    </th>
                                    <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                        Date Encoded
                                    </th>
                                    <th scope="col" class="px-3 py-3 text-center text-xs font-medium text-gray-500 tracking-wider">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                
                                @foreach ($household_infos as $household_info)
                                    <tr @php echo "id='".$household_info->id."'"; @endphp>
                                        @if ($household_info->head_ID)
                                            <td class="px-3 align-middle text-nowrap">{{ $household_info->head_FirstName }} {{ $household_info->head_MiddleName[0] }}. {{ $household_info->head_LastName }}</td>
                                        @else
                                            <td class="px-3 align-middle font-semibold text-gray-600">None</td>
                                        @endif
                                        
                                        <td class="px-3 align-middle text-nowrap">{{ $household_info->interviewer_FirstName.' '.$household_info->interviewer_LastName }}</td>
                                        <td class="px-3 align-middle text-nowrap">Purok {{ $household_info->purok_name.', '.$household_info->barangay_name.', '.$household_info->district.', '.$household_info->province }}</td>
                                        <td class="px-3 align-middle text-nowrap">{{ $household_info->created_at }}</td>
                                        <td class="d-flex justify-content-center text-sm font-medium">
                                            <button type="button" @php echo "value='".$household_info->household_Info_ID."'"; @endphp class="px-4 btn btn-primary View_Household_Btn">
                                                View
                                            </button>
                                            @if (Auth::user()->access_id == 2)
                                                <button @php echo "value='".$household_info->household_Info_ID.'-'.$household_info->id."'"; @endphp class="ms-3 btn btn-outline-danger Trash_Household_Btn" data-bs-toggle="modal" data-bs-target="#TrashHouseholdModal">
                                                    Delete 
                                                </button>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach

                                {{-- Store Household ID in a hidden input tag for data transfey --}}
                                    <form method="GET" action="{{ route('viewHousehold') }}" id="Household_ID_Storage" role="View Household" class="visually-hidden">
                                        <input type="text" class="visually-hidden" id="selectedHouseholdID" name="selectedHouseholdID">
                                    </form>    

                                </tbody>
                            </table>
                        
                        </div>
                        <div class="mt-3 d-flex justify-content-evenly">
                            {!! $household_infos->appends(\Request::except('page'))->render() !!}
                        </div>
                        @if ($household_infos->isEmpty())
                            <div class="m-5 text-center text-gray-600 font-bold">
                                Household list is empty.
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- ------------------- Create Household Modal ------------------- -->
    <div class="modal fade" id="createHousehold" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CreateHouseholdTitle" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen p-5">
            <div class="modal-content">
                <div class="modal-header bg-gray-800 text-white">
                    <h5 class="modal-title" id="CreateHouseholdTitle">Modal title</h5>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div>
                        <!-- ------------ MultiStep Form ------------ -->
                        <div class="container-fluid" >
                            <div class="row justify-content-center mt-0">
        
                                    <div class="text-center card px-0 pt-4 pb-0 mb-3 mt-2">
                                        <h2><strong>Create Household</strong></h2>
                                        <p class="text-gray-600">Fill all form field to go to next step</p>
                                        <div class="row">
                                            <div class="col-md-12 mx-0">
                                                <form id="msform" method="GET" action="{{ route('addHousehold') }}" onsubmit="return doBeforeSubmit()" role="Add Household" autocomplete="off">
                                                    <!-- progressbar -->
                                                    <ul id="progressbar">
                                                        <li class="active" id="basicInfo"><strong>Basic Information</strong></li>
                                                        <li id="sanitation"><strong>Sanitaion</strong></li>
                                                        <li id="members"><strong>Household Members</strong></li>
                                                    </ul> 
                                                    
                                                    <!-- -------------------------------- Basic Information fieldsets -------------------------------- -->
                                                    <fieldset>
                                                        <div class="form-card mb-5 create-household-step-one">                                                    
                                                            
                                                            <div>
                                                                <div>
                                                                    <label for="purok" class="form-label ms-1 text-gray-800">Purok</label>
                                                                    @if (!$barangay_infos->isEmpty())
                                                                        <select class="form-select" id="purok" name="purok">
                                                                            @foreach ($barangay_infos as $barangay_info)
                                                                                <option @php echo "value='".$barangay_info->id."'"; @endphp>Purok {{ $barangay_info->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <input type="text" id="purok" name="purok" class="form-control border border-danger" placeholder="Barangay has no purok info." readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                                <div>
                                                                    <label for="municipalDistrict" class="form-label ms-1 text-gray-800">Municipality/City/District</label>
                                                                    <input type="text" id="municipalDistrict" name="municipalDistrict" class="form-control" value="Davao City" aria-label="Municipality/City/District">
                                                                </div>
                                                                <div>
                                                                    <label for="provinceCity" class="form-label ms-1 text-gray-800">Province/City</label>
                                                                    <input type="text" id="provinceCity" name="provinceCity" class="form-control" value="Davao Del Sur" aria-label="Province/City">
                                                                </div>

                                                                <div>
                                                                    <label for="midwife" class="form-label ms-1 text-gray-800">Midwife/NDP Assigned</label>
                                                                    @if (!$barangay_workers->isEmpty())
                                                                        <select class="form-select" id="midwife" name="midwife">
                                                                            @foreach ($barangay_workers as $barangay_worker)
                                                                                <option @php echo "value='".$barangay_worker->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <input type="text" id="midwife" name="midwife" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                                <div>
                                                                    <label for="interviewedBy" class="form-label ms-1 text-gray-800">Profiled/Interviewed By: </label>
                                                                    @if (!$barangay_workers->isEmpty())
                                                                        <select class="form-select" id="interviewedBy" name="interviewedBy">
                                                                            @foreach ($barangay_workers as $barangay_worker)
                                                                                <option @php echo "value='".$barangay_worker->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <input type="text" id="interviewedBy" name="interviewedBy" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                                <div>
                                                                    <label for="barangayChairman" class="form-label ms-1 text-gray-800">Barangay Chairman</label>
                                                                    @if (!$barangay_workers->isEmpty())
                                                                        <select class="form-select" id="barangayChairman" name="barangayChairman">
                                                                            @foreach ($barangay_workers as $barangay_worker)
                                                                                <option @php echo "value='".$barangay_worker->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <input type="text" id="barangayChairman" name="barangayChairman" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                                                                    @endif
                                                                </div>

                                                                <div>
                                                                    <label for="committeeHealth" class="form-label ms-1 text-gray-800">Committee on Health</label>
                                                                    <input type="text" id="committeeHealth" name="committeeHealth" class="form-control" aria-label="Committee on Health">
                                                                </div>
                                                                <div>
                                                                    <label for="dateProfiled" class="form-label ms-1 text-gray-800">Date Profiled <span class="text-xs">(YYYY/MM/DD)</span></label>
                                                                    <input type="text" id="dateProfiled" name="dateProfiled" class="form-control date" aria-label="Date of Death">
                                                                    <p id="dateProfiledError" class="visually-hidden">Please populate this field.</p>
                                                                </div>
                                                                <div>
                                                                    <label for="tribe" class="form-label ms-1 text-gray-800">Tribe</label>
                                                                    <input type="text" id="tribe" name="tribe" class="form-control" aria-label="Tribe">
                                                                </div>

                                                                <div>
                                                                    <label for="philhealth" class="form-label ms-1 text-gray-800">Philhealth No.</label>
                                                                    <input type="text" id="philhealth" name="philhealth" class="form-control" aria-label="Philhealth No">
                                                                </div>
                                                                <div>
                                                                    <label for="nhtsID" class="form-label ms-1 text-gray-800">NHTS ID No.</label>
                                                                    <input type="text" id="nhtsID" name="nhtsID" class="form-control" aria-label="NHTS ID">
                                                                </div>
                                                                <div>
                                                                    <label for="ipID" class="form-label ms-1 text-gray-800">IP ID No.</label>
                                                                    <input type="text" id="ipID" name="ipID" class="form-control" aria-label="IP ID No">
                                                                </div>
                                                                
                                                                <div class="input-group">
                                                                    <label for="nhts" class="form-label ms-1 text-gray-800 me-3">NHTS</label>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="nhtsRadioOptions" id="nhtsYes" value="Yes">
                                                                        <label class="form-check-label" for="nhtsYes">Yes</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="nhtsRadioOptions" id="nhtsNo" value="No" checked="checked">
                                                                        <label class="form-check-label" for="nhtsNo">No</label>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group">
                                                                    <label for="nonNhts" class="form-label ms-1 text-gray-800 me-3">NON NHTS</label>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="nonNhtsRadioOptions" id="nonNhtsYes" value="Yes">
                                                                        <label class="form-check-label" for="nonNhtsYes">Yes</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="nonNhtsRadioOptions" id="nonNhtsNo" value="No" checked="checked">
                                                                        <label class="form-check-label" for="nonNhtsNo">No</label>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group">
                                                                    <label for="ip" class="form-label ms-1 text-gray-800 me-3">IP</label>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="ipRadioOptions" id="ipYes" value="Yes">
                                                                        <label class="form-check-label" for="ipYes">Yes</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="ipRadioOptions" id="ipNo" value="No" checked="checked">
                                                                        <label class="form-check-label" for="ipNo">No</label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div> 
                                                        
                                                        @if (!$barangay_workers->isEmpty() && !$barangay_infos->isEmpty())
                                                            <input type="button" name="previous" class="previous btn btn-secondary me-2 disabled" value="Previous" />
                                                            <input type="button" name="next" class="next btn btn-primary" value="Next" />
                                                        @else
                                                            <input type="button" name="previous" class="previous btn btn-secondary me-2 disabled" value="Previous" />
                                                            <input type="button" name="next" class="next btn btn-primary disabled text-white" value="Next" />
                                                            <p class="text-danger text-sm mt-4">Cannot proceed, some info are neccessary.</p>
                                                        @endif
                                                        
                                                    </fieldset>
                                                    {{-- END --}}
                                                    
        
                                                    <!-- -------------------------------- Sanitation fieldsets -------------------------------- -->
                                                    <fieldset>
                                                        <div class="form-card row justify-content-center mb-5">                                                    
        
                                                            <div class="col">
                                                                <div class="mb-5">
                                                                    <label for="" class="form-label ms-1 text-gray-800 me-3">Source of Water</label>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="communityWater" name="communityWater">
                                                                        <label class="form-check-label text-black" for="communityWater">
                                                                            Community Water System
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="developedSpring" name="developedSpring">
                                                                        <label class="form-check-label text-black" for="developedSpring">
                                                                            Developed Spring
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="protectedWell" name="protectedWell">
                                                                        <label class="form-check-label text-black" for="protectedWell">
                                                                            Protected Well
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="trunkPeddler" name="trunkPeddler">
                                                                        <label class="form-check-label text-black" for="trunkPeddler">
                                                                            Truck/tanker Peddler
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="bottledWater" name="bottledWater">
                                                                        <label class="form-check-label text-black" for="bottledWater">
                                                                            Bottled water
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="undevelopedSpring" name="undevelopedSpring">
                                                                        <label class="form-check-label text-black" for="undevelopedSpring">
                                                                            Undeveloped Spring
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="unprotectedWell" name="unprotectedWell">
                                                                        <label class="form-check-label text-black" for="unprotectedWell">
                                                                            Unprotected Well
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="rainwater" name="rainwater">
                                                                        <label class="form-check-label text-black" for="rainwater">
                                                                            Rainwater
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="riverStreamDam" name="riverStreamDam">
                                                                        <label class="form-check-label text-black" for="riverStreamDam">
                                                                            River, stream or dam
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group mb-4 create-household-blind-drainage-group">
                                                                    <label for="nonNhts" class="form-label ms-1 text-gray-800 me-3">With Blind Drainage</label>
                                                                    <br>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="withBlindDrainageRadioOptions" id="blindDrainageYes" value="Yes" checked="checked">
                                                                        <label class="form-check-label" for="blindDrainageYes">Yes</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input bigger-check" type="radio" name="withBlindDrainageRadioOptions" id="blindDrainageNo" value="No">
                                                                        <label class="form-check-label" for="blindDrainageNo">No</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col">
                                                                <div class="mb-4">
                                                                    <label for="" class="form-label ms-1 text-gray-800 me-3">Types of Toilet</label>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="flushedToilet" name="flushedToilet">
                                                                        <label class="form-check-label text-black" for="flushedToilet">
                                                                            Water sealed/flush toilet
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="closedPitPrivy" name="closedPitPrivy">
                                                                        <label class="form-check-label text-black" for="closedPitPrivy">
                                                                            Closed Pit Privy
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="communalToilet" name="communalToilet">
                                                                        <label class="form-check-label text-black" for="communalToilet">
                                                                            Communal Toilet
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="dropOverhung" name="dropOverhung">
                                                                        <label class="form-check-label text-black" for="dropOverhung">
                                                                            Drop/overhung
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="fieldBodyOfWater" name="fieldBodyOfWater">
                                                                        <label class="form-check-label text-black" for="fieldBodyOfWater">
                                                                            Field/body of water
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2 pointer-hover">
                                                                        <input class="form-check-input bigger-check" type="checkbox" id="noToilet" name="noToilet">
                                                                        <label class="form-check-label text-black" for="noToilet">
                                                                            No toilet
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="mb-3">
                                                                    <label for="others" class="form-label ms-1 text-black">Others</label>
                                                                    <input class="form-control" id="others" name="others">
                                                                </div>
                                                                <div class="mb-5 col-3">
                                                                    <label for="score" class="form-label ms-1 text-black">Score</label>
                                                                    <input class="form-control score" id="score" name="score">
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <input type="button" name="previous" class="previous btn btn-secondary me-2" value="Previous" />
                                                        <input type="button" name="next" class="next btn btn-primary" value="Next" />
                                                
                                                    </fieldset>
                                                    {{-- END --}}

                                                    <!-- -------------------------------- ADD Household Member fieldsets -------------------------------- -->
                                                    <fieldset>
                                                        <div class="form-card mb-5">  
                                                            <div class="alert alert-danger d-flex justify-content-between Add_House_Member_Error visually-hidden" role="alert">
                                                                <p class="text-sm mb-0 font-semibold Alert_Message"></p>
                                                                <button type="button" class="btn-close Add_House_Member_Alert_Close" aria-label="Close"></button>
                                                            </div>
                                                            <form autocomplete="off">
                                                                @csrf
                                                                <div class="d-flex">
                                                                    <input type="search" name="searchedToAdd" id="searchedToAdd" class="form-control" placeholder="Search Profile" aria-label="Search Profile" aria-describedby="add-worker-btn" list="residenceList">
                                                                </div>
                                                            </form>   
                                                            <div class="position-absolute w-95p pt-3">
                                                                <table class="w-100 table table-bordered table-hover table-responsive">
                                                                    <tbody class="bg-white" id="suggested_on_search">
                                                                        {{-- Rows shall be added here via jquery --}}
                                                                    </tbody>
                                                                </table>
                                                            </div>              
                                                            <div class="overflow-hidden mt-3">
                                                                <table id="persons_table" class="w-100 table table-bordered table-hover table-responsive">
                                                                    <thead class="table-secondary">
                                                                    <tr>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            First Name
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Middle Initial
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Last Name
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Gender
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Relationship
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Family Head
                                                                        </th>
                                                                        <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                                            Action
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="bg-white" id="added_profile_list">
                                                                        {{-- Rows shall be added here via jquery --}}
                                                                    </tbody>
                                                                </table>
                                                            
                                                            </div>
                                                                    
                                                        </div> 
                                                        

                                                        {{-- Hidden tag for data storing only --}}
                                                        @if (!$barangay_infos->isEmpty())
                                                            <input id="Barangay_ID" name="Barangay_ID" @php echo "value='".$barangay_infos[0]->barangay_ID."'"; @endphp class="visually-hidden">
                                                        @endif

                                                        <input id="membersAdded" name="membersAdded" value="" class="visually-hidden">
                                                        {{-- === END === --}}

                                                        

                                                        <input type="button" name="previous" class="previous btn btn-secondary me-2" value="Previous" />
                                                        <input type="submit" id="saveHousehold" name="save" class="save btn btn-primary text-white me-2" value="Save" />
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ====================== END Create Household Modal ====================== -->


    <!-- --------------------- Trash Household Modal ------------------------- -->
    <div class="modal fade" id="TrashHouseholdModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0">
            <div class="modal-header bg-gray-800 text-white">
                <h5 class="modal-title" id="staticBackdropLabel">Trash Household</h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('trashHouseholdFromHouseholdPage') }}" method="POST" autocomplete="off" role="Remove Household Member">
                    @csrf
                    <div class="col pt-2">
                        <label for="confirmation_password" class="form-label ms-1">Please enter password to continue</label>
                        <input type="password" name="confirmation_password" class="form-control" aria-label="confirmation_password" required>
                    </div>

                    {{-- Hidden tag for data storing only --}}
                    <input type="text" class="visually-hidden" id="toRemoveHouseholdInfoID" name="toRemoveHouseholdInfoID">
                    <input type="text" class="visually-hidden" id="toRemoveHouseholdID" name="toRemoveHouseholdID">
                    <div class="modal-footer row mt-4">
                        <div class="col d-flex flex-row-reverse p-0">
                            <button type="submit" class="btn btn-danger text-white me-4 w-24">Continue</button>
                        </div>
                    </div>
                </form>
            
            </div>
        </div>
        </div>
    </div>
    <!-- ======================== END Trash Household Modal ======================== -->



  
    {{-- Script for navigating Creating Household --}}
    <script type="text/javascript">

        // ----------- Date Picker ------------ 
            $('.date').datepicker({  
                format: 'yyyy-mm-dd'
            });
        // ============== END ==============

        $(document).ready(function(){

        // ---------------------------------- Navigating Next/Prev when adding household ----------------------------------
            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current_Page = 1;
            
            $(".next").click(function(){

                if( current_Page == 2 && !document.getElementById('score').value){
                    $(".score").css('border','1px solid red');
                }else{
                    if(document.getElementById('dateProfiled').value){
                        current_fs = $(this).parent();
                        next_fs = $(this).parent().next();

                        //Add Class Active
                        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                        //show the next fieldset
                        next_fs.show();
                        //hide the current fieldset with style
                        current_fs.animate({opacity: 0}, {
                            step: function(now) {
                                // for making fielset appear animation
                                opacity = 1 - now;

                                current_fs.css({
                                    'display': 'none',
                                    'position': 'relative'
                                });
                                next_fs.css({
                                    'opacity': opacity
                                });
                            },
                            duration: 600
                        });
                        current_Page++;
                    }else{
                        document.getElementById('dateProfiled').setAttribute("class", "form-control date border border-danger");
                        document.getElementById('dateProfiledError').setAttribute("class", "text-xs text-danger m-0 mt-2");
                    }
                }

                

            });

            $(".previous").click(function(){

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                'display': 'none',
                'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
                },
                duration: 600
                });
                current_Page--;
            });

            $(".submit").click(function(){
                return false;
            })
        // ================================= END Navigating Next/Prev when adding household =================================


        // ---------------------------------- Show Suggestion when searching function ----------------------------------
            const searchedResident = document.getElementById('searchedToAdd');
            const suggestions = document.getElementById('suggested_on_search');
            const addedList = document.getElementById('added_profile_list');
            let residence_info = {!! json_encode($resident_infos, JSON_HEX_TAG) !!};
            let addedProfileIDs = [];

            const searchProfile = async searchText => {

                let matches = residence_info.filter(person => {
                    const regex = new RegExp(`^${searchText}`, 'gi');
                    if(person.first_name.match(regex) || person.last_name.match(regex)){
                        return person.first_name.match(regex) || person.last_name.match(regex);
                    }else{
                        suggestions.innerHTML = '';
                    }
                })

                if (searchText.length === 0){
                    matches = [];
                    suggestions.innerHTML = '';
                }

                showSuggestions(matches);

            };

            const showSuggestions = matches =>{
                if(matches.length > 0){
                    const html = matches.map(match =>`
                    <tr class="bg-gray-800 text-white">
                        <td class="align-middle whitespace-nowrap border-end-0">
                            ${match.first_name} ${match.last_name}
                        </td>
                        <td class="w-16 border-start-0">
                            <button type="button" value='${match.id}-${match.first_name}-${match.last_name}-${match.middle_name}-${match.gender}-${match.address}' id="addBTN" class="text-white btn btn-primary addBTN w-24">
                                + Add
                            </button>
                        </td>
                    </tr>
                    `).join('');

                    suggestions.innerHTML = html;
                }

                // ---------------------------- Add Selected Profile to AddedList ----------------------------
                $(".addBTN").click(function(){

                    let temporaryArray = this.value.split('-');
                    let personID = temporaryArray[0];

                    if(!addedProfileIDs.includes(personID)){
                        let personFirstName = temporaryArray[1];
                        let personLastName = temporaryArray[2];
                        let personMiddleName = temporaryArray[3];
                        let personGender = temporaryArray[4];
                        let personAddress = temporaryArray[5];

                        let rowToAdd = addedList.innerHTML + `<tr id="${personID}">
                            <td class="px-3 align-middle">${personFirstName}</td>
                            <td class="px-3 align-middle">${personMiddleName[0]}.</td>
                            <td class="px-3 align-middle">${personLastName}</td>
                            <td class="px-3 align-middle">${personGender}</td>
                            <td class="px-3 align-middle">
                                <select class="form-select relationship" id="relationship" name="relationship">
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandfather">Grandfather</option>
                                    <option value="Grandmother">Grandmother</option>
                                    <option value="Siblings">Siblings</option>
                                    <option value="Aunt">Aunt</option>
                                    <option value="Uncle">Uncle</option>
                                    <option value="Cousin">Cousin</option>
                                </select>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input bigger-check" type="radio" name="familyHead" id="familyHead" value="Yes">
                                    <label class="form-check-label" for="familyHead">Yes</label>
                                </div>
                            </td>
                            <td class="d-flex justify-content-center text-sm font-medium">
                                <button type="button" value="${personID}-${personFirstName}-${personMiddleName}-${personLastName}}" class="btn btn-outline-danger RemoveMemberBTN">
                                    Remove
                                </button>
                            </td>
                            <td class="visually-hidden">${personID}</td>
                        </tr><span></span>`;

                        addedProfileIDs.push(personID);
                        addedList.innerHTML = rowToAdd;
                        suggestions.innerHTML = '';

                        // -------------- Remove selected profile from the Added List --------------
                            $(".RemoveMemberBTN").click(function(){
                                let idToRemove = this.value.split('-')[0];
                                document.getElementById(idToRemove).remove();
                                addedProfileIDs = removeAddedID(addedProfileIDs, idToRemove);
                            });
                        // ========== END Remove selected profile from the Added List ==========

                    }else{
                        $(".Alert_Message").text("Selected profile is already added.");
                        $(".Add_House_Member_Error").removeClass('visually-hidden');
                    }
                });
                // =========================== END Add Selected Profile to AddedList ===========================
            }

            searchedResident.addEventListener('input', () => searchProfile(searchedResident.value));
            searchedResident.addEventListener('focus', () => searchProfile(searchedResident.value));
        // ===================================== END Show Suggestion when searching function =====================================
            

        // --------- Removes ID from added profile ids list --------------------
            function removeAddedID(addedIDs, toRemoveID) { 
                return addedIDs.filter(function(ele){ 
                    return ele != toRemoveID; 
                });
            }
        //  === END ===


        // -------------------- Checks if Household has family members and head -------------------
            document.getElementById('msform').onsubmit = function() {

                if(addedProfileIDs.length > 0){
                    let addedMembers = document.getElementById('membersAdded');
                    let hasHead = false;
                    
                    for(let rowIndex = 0; rowIndex < addedProfileIDs.length; rowIndex++){
                        addedMembers.value = addedMembers.value + addedList.rows[rowIndex].cells[7].innerHTML + '-' + addedList.rows[rowIndex].cells[4].getElementsByTagName('select')[0].value + '-' + addedList.rows[rowIndex].cells[5].getElementsByTagName('input')[0].checked + ',';
                        if(addedList.rows[rowIndex].cells[5].getElementsByTagName('input')[0].checked){
                            hasHead = true;
                        }
                    }
                    if(hasHead){
                        return true;
                    }else{
                        addedMembers.value = "";
                        $(".Alert_Message").text("Please add head of household.");
                        $(".Add_House_Member_Error").removeClass('visually-hidden');
                        return false;
                    }
                }else{
                    $(".Alert_Message").text("Please add household member.");
                    $(".Add_House_Member_Error").removeClass('visually-hidden');
                    return false;
                }
            };
        //  =========================== END ==============================
        
        // ------------------ Hide Add Household Member Error Alrt ------------------
            $(".Add_House_Member_Alert_Close").click(function(){
                $(".Add_House_Member_Error").addClass('visually-hidden');
            });
        //  =========================== END ==============================

        
        // ------------------------ View Household Info ------------------------
            $(".View_Household_Btn").click(function(){
                document.getElementById("selectedHouseholdID").value = this.value;
                document.getElementById("Household_ID_Storage").submit();
            });
        // =========================== END ==============================


        // ------------------------ Trash Household Info ------------------------
            $(".Trash_Household_Btn").click(function(){
                let holder = this.value.split('-');
                document.getElementById("toRemoveHouseholdInfoID").value = holder[0];
                document.getElementById("toRemoveHouseholdID").value = holder[1];
            });
        // =========================== END ==============================



        // -------------------------- Refreshes the variables used in Adding Household Member when modal is closed --------------------------
            $(".CreateHouseholdBTN").click(function(){
                document.getElementById('searchedToAdd').value = "";
                document.getElementById('membersAdded').value = "";
                suggestions.innerHTML = '';
                addedList.innerHTML = '';
                addedProfileIDs = [];
            });
        // =========================== END ===========================

            
            
        });
    </script>
    {{-- END Script for navigating Creating Household --}}

    
@endsection