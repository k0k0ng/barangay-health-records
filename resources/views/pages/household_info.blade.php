@extends('layouts.app')

@section('content')
    
<div class="d-block h-full">
    @if ( session('updateSucess') )
        <div class="alert alert-success d-flex justify-content-between" role="alert">
            <p class="text-sm mb-0">{{ session('updateSucess') }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div> 
        
    @endif
    @if ( session('EditFailed') )
        <div class="alert alert-danger d-flex justify-content-between" role="alert">
            <p class="text-sm mb-0">{{ session('EditFailed') }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div> 
    @endif
    <div class="w-full bg-white p-3 rounded border-2 h-full pb-5 mb-5">
        
        {{-- Hidden Forms --}}
        {{-- *** View Household Member Profile Form --}}
        <form id="ViewHouseholdMemberInfoForm" class="visually-hidden" method="GET" action="{{ route('ViewResidentProfile') }}" role="View Resident Profile">
            <input id="selectedHouseholdMemberToViewID" name="residentProfileSelectedID">
        </form>

        {{-- *** Appoint Household Head Form --}}
        <form id="AppointHouseholdHeadForm" class="visually-hidden" method="POST" action="{{ route('appointHead') }}" role="Appoint Household Head">
            @csrf
            <input type="text" id="selectedHouseholdInfoID" name="selectedHouseholdInfoID">
            <input type="text" id="selectedHouseholdID" name="selectedHouseholdID">
            <input type="text" id="memberID" name="memberID">
        </form>
        {{-- END Hidden Forms --}}
        
        
        <div class="row px-4 py-2">
            <div class="d-flex col">
                <p class="p-0 pt-2 m-0 align-text-bottom me-3">Household Info</p>
                <form action="{{ route('printHousehold') }}" method="GET">
                    {{-- Hidden tag for data storing only --}}
                    <input id="Household_ID_To_Print" name="Household_ID_To_Print" @php echo"value='".$household_infos[0]->id."'" @endphp class="visually-hidden">
                    <button type="submit" class="btn btn-primary">Print Household</button>
                </form>  
                
            </div>
            <div class="d-flex justify-content-end col">
                <input class="visually-hidden" name="household_ID" @php echo "value='".$household_infos[0]->id."'"; @endphp>
                <button class="btn btn-outline-primary EditHouseholdModalBtn" data-bs-toggle="modal" data-bs-target="#EditHouseholdModal">Edit</button>
                
                @if (Auth::user()->access_id == 2)
                    <button class="btn btn-outline-danger ms-3" data-bs-toggle="modal" data-bs-target="#TrashHouseholdModal">Delete </button>
                @endif
            </div>
        </div>
        
        <div class="mx-4 my-2 p-4 border">
            <div class="household-info-div">
                <div>
                    <p class="text-gray-600 text-xs font-semibold">Address: <span id="Age" class="text-lg font-medium text-gray-800 ms-1">Purok {{ $household_infos[0]->purok_name.', '.$household_infos[0]->brgy_name.', '.$household_infos[0]->district }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Date profiled: <span id="Birthday" class="text-lg font-medium text-gray-800 ms-1">{{ date("M d, Y", strtotime($household_infos[0]->date_profiled)) }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Midwife: <span id="Gender" class="text-lg font-medium text-gray-800 ms-1">{{ $midwife_info->firstname.' '.$midwife_info->lastname }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Interviewed by: <span id="Height" class="text-lg font-medium text-gray-800 ms-1">{{ $interviewer_info->firstname.' '.$interviewer_info->lastname }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Encoder: <span id="Weight" class="text-lg font-medium text-gray-800 ms-1">{{ $encoder_info->first_name.' '.$encoder_info->last_name }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Philhealth No.: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->philhealth_no }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">CCT: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->cct }}</span> </p>
                </div>
                <div>
                    <p class="text-gray-600 text-xs font-semibold">Sanitation Score: <span id="Cell_No" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->score }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Water source: 
                        <span id="Email" class="text-lg font-medium text-gray-800 ms-1">
                            @php
                                $counter = 0;
                            @endphp
                            @if ($household_infos[0]->community_water == "Yes")
                                Community Water
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->developed_spring == "Yes")
                                @if ($counter>0) - @endif
                                Developed Spring
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->protected_well == "Yes")
                                @if ($counter>0) - @endif
                                Protected Well
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->truck_tanker_peddler == "Yes")
                                @if ($counter>0) - @endif
                                Truck/Tanker Peddler
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->bottled_water == "Yes")
                                @if ($counter>0) - @endif
                                Bottled Water
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->undeveloped_spring == "Yes")
                                @if ($counter>0) - @endif
                                Undeveloped Spring
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->undeveloped_well == "Yes")
                                @if ($counter>0) - @endif
                                Undeveloped Well
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->rainwater == "Yes")
                                @if ($counter>0) - @endif
                                Rainwater
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->river_stream_dam == "Yes")
                                @if ($counter>0) - @endif
                                River/Stream/Dam
                            @endif
                        </span>
                    <p class="text-gray-600 text-xs font-semibold">Toilet: 
                        <span id="Tell_No" class="text-lg font-medium text-gray-800 ms-1">
                            @php
                                $counter = 0;
                            @endphp
                            @if ($household_infos[0]->flush_toilet == "Yes")
                                Water Flush Toilet
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->closed_pit_pervy == "Yes")
                                @if ($counter>0) - @endif
                                Closed Pit Pervy
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->communal_toilet == "Yes")
                                @if ($counter>0) - @endif
                                Communal Toilet
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->drop_overhung == "Yes")
                                @if ($counter>0) - @endif
                                Drop/Overhung
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->field_bodyOfWater == "Yes")
                                @if ($counter>0) - @endif
                                Field/Body of Water
                                @php $counter++; @endphp
                            @endif
                        </span> </p>
                    <p class="text-gray-600 text-xs font-semibold">NHTS: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->nhts }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">NHTS No.: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->nhts_no }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">IP: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->ip }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Tribe: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->tribe }}</span> </p>
                </div>
            </div>
        </div>

        <div class="row px-4 py-2 mt-4">
            <p class="p-0 pt-2 m-0 align-text-bottom col">Household Members</p>
            <div class="d-flex justify-content-end col">
                <button class="btn btn-outline-primary me-3 AddHouseholdMemberModalBtn" data-bs-toggle="modal" data-bs-target="#AddHouseholdMemberModal">+ Add Member</button>
            </div>
        </div>
        <div class="px-4 d-block overflow-auto">
            <table id="household_member_table" class="w-100 table table-bordered table-hover table-responsive">
                <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Name
                    </th>
                    <th class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Occupation
                    </th>
                    <th class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Gender
                    </th>
                    <th class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Role
                    </th>
                    <th class="py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @php
                    $member_IDs = [];
                @endphp
                @foreach ($household_infos as $household_info)
                    @php
                        array_push($member_IDs,$household_info->Person_ID);
                    @endphp
                    <tr @php echo "id='".$household_info->id."'"; @endphp>
                        <td class="px-3 align-middle whitespace-nowrap border-0 border-start text-nowrap">{{ $household_info->last_name.', '.$household_info->first_name.' '.$household_info->middle_name }}</td>
                        <td class="px-3 align-middle border-0">{{ $household_info->occupation }}</td>
                        <td class="px-3 align-middle border-0">{{ $household_info->gender }}</td>
                        <td class="px-3 align-middle border-0">{{ $household_info->relationship }}
                            @if ($household_info->family_head == "Yes")
                                (Head)
                            @endif
                        </td>
                        <td class="d-flex justify-content-center border-0 border-end">
                            
                            <button type="button" class='btn btn-primary me-4 ViewHouseholdMemberInfoBtn' @php echo "value='".$household_info->Person_ID."'"; @endphp>
                                View
                            </button>

                            <button type="submit" 
                                @php  
                                    if($household_info->family_head == "Yes"){ 
                                        echo "class='btn btn-secondary text-nowrap' disabled";
                                    }else{ 
                                        echo"class='btn btn-primary text-nowrap AppointHouseholdHeadBtn' value='".$household_info->id."-".$household_info->Household_ID."-".$household_info->Person_ID."'";
                                    }  
                                @endphp>
                                Appoint Head
                            </button>

                            @if (Auth::user()->access_id == 2)
                                <button type="button" data-bs-toggle="modal" data-bs-target="#RemoveMemberConfirmationModal"
                                    @php  
                                        if($household_info->family_head == "Yes"){ 
                                            echo "class='btn btn-secondary ms-4' disabled";
                                        }else{ 
                                            echo"class='btn btn-outline-danger Remove_House_Member_Btn ms-4' value='".$household_info->Household_ID.'-'.$household_info->Person_ID."'";
                                        }  
                                    @endphp>
                                    Remove
                                </button> 
                            @endif
                                              
                            
                        </td>
                    </tr>
                @endforeach   
                 
                </tbody>
            </table>
        </div>

    </div>
</div>

<!-- --------------------- Remove House Member Modal ------------------------- -->
<div class="modal fade" id="RemoveMemberConfirmationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content border-0">
        <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Remove Household Member</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ route('removeHouseMember') }}" method="POST" autocomplete="off" role="Remove Household Member">
                @csrf
                <div class="col pt-2">
                    <label for="confirmation_password" class="form-label ms-1">Please enter password to continue</label>
                    <input type="password" name="confirmation_password" class="form-control" aria-label="confirmation_password" required>
                </div>

                {{-- Hidden tag for data storing only --}}
                <input type="text" class="visually-hidden" id="toRemoveMemberHouseholdID" name="toRemoveMemberHouseholdID" >
                <input type="text" class="visually-hidden" id="toRemoveMemberID" name="toRemoveMemberID" >
                <div class="modal-footer row mt-4">
                    <div class="col d-flex flex-row-reverse p-0">
                        <button type="submit" class="btn btn-danger text-white me-4 w-24">Continue</button>
                    </div>
                </div>
            </form>
        
        </div>
    </div>
    </div>
</div>
<!-- ======================== END Remove House Member Modal ======================== -->


<!-- --------------------- Edit Household Modal ------------------------- -->
<div class="modal fade" id="EditHouseholdModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content border-0">
        <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Edit Household</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="EditHouseholdModalForm" action="{{ route('editHousehold') }}" method="POST" autocomplete="off" role="Remove Household Member">
                @csrf
                <p class="text-xl p-2 mt-1">Basic Information</p>
                <div class="row mb-5 mt-1 px-4 edit-household-step-one">
                    <div class="col">
                        <label for="purok" class="form-label ms-1 text-gray-800">Purok</label>
                        @if (!$barangay_infos->isEmpty())
                            <select class="form-select" id="purok" name="purok">
                                @foreach ($barangay_infos as $barangay_info)
                                    <option @php echo "value='".$barangay_info->id."'"; @endphp>Purok {{ $barangay_info->name }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" id="purok" name="purok" class="form-control border border-danger" placeholder="Barangay has no purok info." readonly>
                        @endif
                        
                    </div>
                    <div class="col">
                        <label for="municipalDistrict" class="form-label ms-1 text-gray-800">Municipality/City/District</label>
                        <input type="text" id="municipalDistrict" name="municipalDistrict" class="form-control" value="Davao City" aria-label="Municipality/City/District" required>
                    </div>
                    <div class="col">
                        <label for="provinceCity" class="form-label ms-1 text-gray-800">Province/City</label>
                        <input type="text" id="provinceCity" name="provinceCity" class="form-control" value="Davao Del Sur" aria-label="Province/City" required>
                    </div>

                    <div class="col">
                        <label for="midwife" class="form-label ms-1 text-gray-800">Midwife/NDP Assigned</label>
                        @if (!$barangay_workers->isEmpty())
                            <select class="form-select" id="midwife" name="midwife" required>
                                @foreach ($barangay_workers as $barangay_worker)
                                    <option @php echo "value='".$midwife_info->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" id="midwife" name="midwife" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                        @endif
                        
                    </div>
                    <div class="col">
                        <label for="interviewedBy" class="form-label ms-1 text-gray-800">Profiled/Interviewed By: </label>
                        @if (!$barangay_workers->isEmpty())
                            <select class="form-select" id="interviewedBy" name="interviewedBy" required>
                                @foreach ($barangay_workers as $barangay_worker)
                                    <option @php echo "value='".$interviewer_info->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" id="interviewedBy" name="interviewedBy" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                        @endif
                        
                    </div>
                    <div class="col">
                        <label for="barangayChairman" class="form-label ms-1 text-gray-800">Barangay Chairman</label>
                        @if (!$barangay_workers->isEmpty())
                            <select class="form-select" id="barangayChairman" name="barangayChairman" required>
                                @foreach ($barangay_workers as $barangay_worker)
                                    <option @php echo "value='".$barangay_worker->id."'"; @endphp>{{ $barangay_worker->type }}: {{ $barangay_worker->lastname }}, {{ $barangay_worker->firstname }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" id="barangayChairman" name="barangayChairman" class="form-control border border-danger" placeholder="Barangay workers is empty." readonly>
                        @endif
                    </div>

                    <div class="col">
                        <label for="committeeHealth" class="form-label ms-1 text-gray-800">Committee on Health</label>
                        <input type="text" id="committeeHealth" name="committeeHealth" class="form-control" aria-label="Committee on Health" required>
                    </div>
                    <div class="col">
                        <label for="dateProfiled" class="form-label ms-1 text-gray-800">Date Profiled <span class="text-xs">(YYYY/MM/DD)</span></label>
                        <input type="text" id="dateProfiled" name="dateProfiled" class="form-control date" aria-label="Date of Death" required>
                        <p id="dateProfiledError" class="visually-hidden">Please populate this field.</p>
                    </div>
                    <div class="col">
                        <label for="tribe" class="form-label ms-1 text-gray-800">Tribe</label>
                        <input type="text" id="tribe" name="tribe" class="form-control" aria-label="Tribe">
                    </div>

                    <div class="col">
                        <label for="philhealth" class="form-label ms-1 text-gray-800">Philhealth No.</label>
                        <input type="text" id="philhealth" name="philhealth" class="form-control" aria-label="Philhealth No">
                    </div>
                    <div class="col">
                        <label for="nhtsID" class="form-label ms-1 text-gray-800">NHTS ID No.</label>
                        <input type="text" id="nhtsID" name="nhtsID" class="form-control" aria-label="NHTS ID">
                    </div>
                    <div class="col">
                        <label for="ipID" class="form-label ms-1 text-gray-800">IP ID No.</label>
                        <input type="text" id="ipID" name="ipID" class="form-control" aria-label="IP ID No">
                    </div>
                    
                    <div class="input-group col">
                        <label for="nhts" class="form-label ms-1 text-gray-800 me-3">NHTS</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="nhtsRadioOptions" id="nhtsYes" value="Yes">
                            <label class="form-check-label" for="nhtsYes">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="nhtsRadioOptions" id="nhtsNo" value="No" checked="checked">
                            <label class="form-check-label" for="nhtsNo">No</label>
                        </div>
                    </div>
                    <div class="input-group col">
                        <label for="nonNhts" class="form-label ms-1 text-gray-800 me-3">NON NHTS</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="nonNhtsRadioOptions" id="nonNhtsYes" value="Yes">
                            <label class="form-check-label" for="nonNhtsYes">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="nonNhtsRadioOptions" id="nonNhtsNo" value="No" checked="checked">
                            <label class="form-check-label" for="nonNhtsNo">No</label>
                        </div>
                    </div>
                    <div class="input-group col">
                        <label for="ip" class="form-label ms-1 text-gray-800 me-3">IP</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="ipRadioOptions" id="ipYes" value="Yes">
                            <label class="form-check-label" for="ipYes">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input bigger-check" type="radio" name="ipRadioOptions" id="ipNo" value="No" checked="checked">
                            <label class="form-check-label" for="ipNo">No</label>
                        </div>
                    </div>
                </div>
                <hr>
                <p class="text-xl p-2 mt-4">Sanitation Status</p>
                <div class="row mb-5 mt-3 px-4">                                                    
                    <div class="col">
                        <div class="mb-5">
                            <label for="" class="form-label ms-1 text-gray-800 me-3">Source of Water</label>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="communityWater" name="communityWater">
                                <label class="form-check-label text-black" for="communityWater">
                                    Community Water System
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="developedSpring" name="developedSpring">
                                <label class="form-check-label text-black" for="developedSpring">
                                    Developed Spring
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="protectedWell" name="protectedWell">
                                <label class="form-check-label text-black" for="protectedWell">
                                    Protected Well
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="trunkPeddler" name="trunkPeddler">
                                <label class="form-check-label text-black" for="trunkPeddler">
                                    Truck/tanker Peddler
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="bottledWater" name="bottledWater">
                                <label class="form-check-label text-black" for="bottledWater">
                                    Bottled water
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="undevelopedSpring" name="undevelopedSpring">
                                <label class="form-check-label text-black" for="undevelopedSpring">
                                    Undeveloped Spring
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="unprotectedWell" name="unprotectedWell">
                                <label class="form-check-label text-black" for="unprotectedWell">
                                    Unprotected Well
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="rainwater" name="rainwater">
                                <label class="form-check-label text-black" for="rainwater">
                                    Rainwater
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="riverStreamDam" name="riverStreamDam">
                                <label class="form-check-label text-black" for="riverStreamDam">
                                    River, stream or dam
                                </label>
                            </div>
                        </div>
                        <div class="input-group mb-4">
                            <label for="nonNhts" class="form-label ms-1 text-gray-800 me-3">With Blind Drainage</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input bigger-check" type="radio" name="withBlindDrainageRadioOptions" id="blindDrainageYes" value="Yes" checked="checked">
                                <label class="form-check-label" for="blindDrainageYes">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input bigger-check" type="radio" name="withBlindDrainageRadioOptions" id="blindDrainageNo" value="No">
                                <label class="form-check-label" for="blindDrainageNo">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-4">
                            <label for="" class="form-label ms-1 text-gray-800 me-3">Types of Toilet</label>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="flushedToilet" name="flushedToilet">
                                <label class="form-check-label text-black" for="flushedToilet">
                                    Water sealed/flush toilet
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="closedPitPrivy" name="closedPitPrivy">
                                <label class="form-check-label text-black" for="closedPitPrivy">
                                    Closed Pit Privy
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="communalToilet" name="communalToilet">
                                <label class="form-check-label text-black" for="communalToilet">
                                    Communal Toilet
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="dropOverhung" name="dropOverhung">
                                <label class="form-check-label text-black" for="dropOverhung">
                                    Drop/overhung
                                </label>
                            </div>
                            <div class="form-check mt-2 pointer-hover">
                                <input class="form-check-input bigger-check" type="checkbox" id="fieldBodyOfWater" name="fieldBodyOfWater">
                                <label class="form-check-label text-black" for="fieldBodyOfWater">
                                    Field/body of water
                                </label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="others" class="form-label ms-1 text-black">Others</label>
                            <input class="form-control" id="others" name="others">
                        </div>
                        <div class="mb-5 col-4">
                            <label for="score" class="form-label ms-1 text-black">Score</label>
                            <input class="form-control score" id="score" name="score" required>
                        </div>
                    </div>
                    
                </div>

                {{-- Hidden tag for data storing only --}}
                <input name="household_Info_ID" @php echo "value='".$household_infos[0]->HouseholdInfo_ID."'"; @endphp class="visually-hidden">
                <input name="household_ID" @php echo "value='".$household_infos[0]->Household_ID."'"; @endphp class="visually-hidden">
                <input name="Barangay_ID" @php echo "value='".$barangay_infos[0]->brgy_id."'"; @endphp class="visually-hidden">

                <div class="modal-footer row mt-4">
                    <div class="col d-flex flex-row-reverse p-0">
                        <button type="submit" class="btn btn-primary text-white me-4 w-24">Save</button>
                    </div>
                </div>
            </form>
        
        </div>
    </div>
    </div>
</div>
<!-- ======================== END Edit Household Modal ======================== -->


<!-- --------------------- Add Household Member Modal ------------------------- -->
<div class="modal fade" id="AddHouseholdMemberModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content border-0">
        <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Add Household Member</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger d-flex justify-content-between Add_House_Member_Error visually-hidden" role="alert">
                <p class="text-sm mb-0 font-semibold Alert_Message"></p>
                <button type="button" class="btn-close Add_House_Member_Alert_Close" aria-label="Close"></button>
            </div>
            <form autocomplete="off">
                <div class="d-flex">
                    <input type="search" name="searchedToAdd" id="searchedToAdd" class="form-control" placeholder="Search Profile" aria-label="Search Profile" aria-describedby="add-worker-btn" list="residenceList">
                </div>
            </form>
             
            <div class="position-absolute w-95p pt-3">
                <table class="w-100 table table-bordered table-hover table-responsive">
                    <tbody class="bg-white" id="suggested_on_search">
                        {{-- Rows shall be added here via jquery --}}
                    </tbody>
                </table>
            </div>
            <form id="AddHouseholdMemberModalForm" action="{{ route('addHouseholdMember') }}" method="POST" autocomplete="off" role="Add Household Member">
                @csrf           
                <div class="overflow-hidden mt-3">
                    <table id="persons_table" class="w-100 table table-bordered table-hover table-responsive">
                        <thead class="table-secondary">
                        <tr>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                First Name
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Middle Initial
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Last Name
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Gender
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Relationship
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white" id="added_profile_list">
                            {{-- Rows shall be added here via jquery --}}
                        </tbody>
                    </table>
                </div>

                {{-- Hidden tag for data storing only --}}
                <input id="membersToAdd" name="membersToAdd" class="visually-hidden">
                <input id="memberToAddHouseholdID" name="memberToAddHouseholdID" class="visually-hidden" @php echo "value='".$household_infos[0]->Household_ID."'"; @endphp>

                <div class="modal-footer row mt-4">
                    <div class="col d-flex flex-row-reverse p-0">
                        <button type="submit" class="btn btn-primary text-white me-4 w-24">Save</button>
                    </div>
                </div>
            </form>
        
        </div>
    </div>
    </div>
</div>
<!-- ======================== END Add Household Member Modal ======================== -->


<!-- --------------------- Trash Household Modal ------------------------- -->
<div class="modal fade" id="TrashHouseholdModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content border-0">
        <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Trash Household</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ route('trashHousehold') }}" method="POST" autocomplete="off" role="Remove Household Member">
                @csrf
                <div class="col pt-2">
                    <label for="confirmation_password" class="form-label ms-1">Please enter password to continue</label>
                    <input type="password" name="confirmation_password" class="form-control" aria-label="confirmation_password" required>
                </div>

                {{-- Hidden tag for data storing only --}}
                <input type="text" class="visually-hidden" id="toRemoveHouseholdInfoID" name="toRemoveHouseholdInfoID" @php echo "value='".$household_infos[0]->id."'"; @endphp>
                <input type="text" class="visually-hidden" id="toRemoveHouseholdID" name="toRemoveHouseholdID" @php echo "value='".$household_infos[0]->Household_ID."'"; @endphp>
                <div class="modal-footer row mt-4">
                    <div class="col d-flex flex-row-reverse p-0">
                        <button type="submit" class="btn btn-danger text-white me-4 w-24">Continue</button>
                    </div>
                </div>
            </form>
        
        </div>
    </div>
    </div>
</div>
<!-- ======================== END Trash Household Modal ======================== -->



<script type="text/javascript">

    // ----------- Date Picker ------------ 
        $('.date').datepicker({  
            format: 'yyyy-mm-dd'
        });
    // ============== END ==============

    $(document).ready(function(){
        
        // -------------- Edit Household Modal Btn clicked --------------
            $(".EditHouseholdModalBtn").click(function(){

                let infos = {!! json_encode($household_infos) !!};
                infos = infos[0];
                console.log(infos);
                document.getElementById('purok').value = infos['purok_id'];
                document.getElementById('municipalDistrict').value = infos['district'];
                document.getElementById('provinceCity').value = infos['province'];
                document.getElementById('midwife').value = infos['midwife_ndp_assigned'];
                document.getElementById('interviewedBy').value = infos['interviewed_by'];
                document.getElementById('barangayChairman').value = infos['brgy_chairman_id'];
                document.getElementById('committeeHealth').value = infos['committee'];
                document.getElementById('dateProfiled').value = infos['date_profiled'];
                document.getElementById('tribe').value = infos['tribe'];
                document.getElementById('philhealth').value = infos['philhealth_no'];
                document.getElementById('nhtsID').value = infos['nhts_no'];
                document.getElementById('ipID').value = infos['ip_no'];

                if(infos['nhts'] == "Yes") document.getElementById('nhtsYes').checked = true;
                if(infos['non_nhts'] == "Yes") document.getElementById('nonNhtsYes').checked = true;
                if(infos['ip'] == "Yes") document.getElementById('ipYes').checked = true;

                if(infos['community_water'] == "Yes") document.getElementById('communityWater').checked = true;
                if(infos['developed_spring'] == "Yes") document.getElementById('developedSpring').checked = true;
                if(infos['protected_well'] == "Yes") document.getElementById('protectedWell').checked = true;
                if(infos['truck_tanker_peddler'] == "Yes") document.getElementById('trunkPeddler').checked = true;
                if(infos['bottled_water'] == "Yes") document.getElementById('bottledWater').checked = true;
                if(infos['undeveloped_spring'] == "Yes") document.getElementById('undevelopedSpring').checked = true;
                if(infos['undeveloped_well'] == "Yes") document.getElementById('unprotectedWell').checked = true;
                if(infos['rainwater'] == "Yes") document.getElementById('rainwater').checked = true;
                if(infos['river_stream_dam'] == "Yes") document.getElementById('riverStreamDam').checked = true;

                if(infos['community_water'] == "Yes") document.getElementById('communityWater').checked = true;

                if(infos['flush_toilet'] == "Yes") document.getElementById('flushedToilet').checked = true;
                if(infos['closed_pit_pervy'] == "Yes") document.getElementById('closedPitPrivy').checked = true;
                if(infos['communal_toilet'] == "Yes") document.getElementById('communalToilet').checked = true;
                if(infos['drop_overhung'] == "Yes") document.getElementById('dropOverhung').checked = true;
                if(infos['field_bodyOfWater'] == "Yes") document.getElementById('fieldBodyOfWater').checked = true;

                document.getElementById('others').value = infos['others'];
                document.getElementById('score').value = infos['score'];
            });
        // ========== END Edit Household Modal Btn clicked ==========



        // ---------------------------------- Show Suggestion when searching function ----------------------------------
            const searchedResident = document.getElementById('searchedToAdd');
            const suggestions = document.getElementById('suggested_on_search');
            const addedList = document.getElementById('added_profile_list');
            let residence_info = {!! json_encode($not_member_residents, JSON_HEX_TAG) !!};
            let addedProfileIDs = [];

            const searchProfile = async searchText => {

                let matches = residence_info.filter(person => {
                    const regex = new RegExp(`^${searchText}`, 'gi');
                    if(person.first_name.match(regex) || person.last_name.match(regex)){
                        return person.first_name.match(regex) || person.last_name.match(regex);
                    }else{
                        suggestions.innerHTML = '';
                    }
                })

                if (searchText.length === 0){
                    matches = [];
                    suggestions.innerHTML = '';
                }
                
                showSuggestions(matches);

            };

            const showSuggestions = matches =>{
                if(matches.length > 0){
                    const html = matches.map(match =>`
                    <tr class="bg-gray-800 text-white">
                        <td class="align-middle whitespace-nowrap border-end-0">
                            ${match.first_name} ${match.last_name}
                        </td>
                        <td class="w-16 border-start-0">
                            <button type="button" value='${match.id}-${match.first_name}-${match.last_name}-${match.middle_name}-${match.gender}-${match.address}' id="addMemberToListBtn" class="text-white btn btn-primary addMemberToListBtn w-24">
                                + Add
                            </button>
                        </td>
                    </tr>
                    `).join('');

                    suggestions.innerHTML = html;
                }

                // ---------------------------- Add Selected Profile to AddedList ----------------------------
                $(".addMemberToListBtn").click(function(){

                    let temporaryArray = this.value.split('-');
                    let personID = temporaryArray[0];

                    if(!addedProfileIDs.includes(personID)){
                        let personFirstName = temporaryArray[1];
                        let personLastName = temporaryArray[2];
                        let personMiddleName = temporaryArray[3];
                        let personGender = temporaryArray[4];
                        let personAddress = temporaryArray[5];

                        let rowToAdd = addedList.innerHTML + `<tr id="${personID}">
                            <td class="px-3 align-middle">${personFirstName}</td>
                            <td class="px-3 align-middle">${personMiddleName[0]}.</td>
                            <td class="px-3 align-middle">${personLastName}</td>
                            <td class="px-3 align-middle">${personGender}</td>
                            <td class="px-3 align-middle">
                                <select class="form-select relationship" id="relationship" name="relationship">
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandfather">Grandfather</option>
                                    <option value="Grandmother">Grandmother</option>
                                    <option value="Siblings">Siblings</option>
                                    <option value="Aunt">Aunt</option>
                                    <option value="Uncle">Uncle</option>
                                    <option value="Cousin">Cousin</option>
                                </select>
                            </td>
                            <td class="d-flex justify-content-center text-sm font-medium">
                                <button type="button" value="${personID}-${personFirstName}-${personMiddleName}-${personLastName}}" class="btn btn-outline-danger RemoveMemberToListBtn">
                                    Remove
                                </button>
                            </td>
                            <td class="visually-hidden">${personID}</td>
                        </tr><span></span>`;

                        addedProfileIDs.push(personID);
                        addedList.innerHTML = rowToAdd;
                        suggestions.innerHTML = '';

                        // -------------- Remove selected profile from the Added List --------------
                        $(".RemoveMemberToListBtn").click(function(){
                            let idToRemove = this.value.split('-')[0];
                            document.getElementById(idToRemove).remove();

                            addedProfileIDs = removeAddedID(addedProfileIDs, idToRemove);
                        });
                        // ========== END Remove selected profile from the Added List ==========
                    }else{
                        $(".Alert_Message").text("Selected profile is already added.");
                        $(".Add_House_Member_Error").removeClass('visually-hidden');
                    }
                });
                // =========================== END Add Selected Profile to AddedList ===========================
            }

            searchedResident.addEventListener('input', () => searchProfile(searchedResident.value));
            searchedResident.addEventListener('focus', () => searchProfile(searchedResident.value));
        // ===================================== END Show Suggestion when searching function =====================================
            

        // --------- Removes ID from added profile ids list --------------------
            function removeAddedID(addedIDs, toRemoveID) { 
                return addedIDs.filter(function(ele){ 
                    return ele != toRemoveID; 
                });
            }
        //  === END ===


        // -------------------- Checks if Household has family members and head -------------------
            document.getElementById('AddHouseholdMemberModalForm').onsubmit = function() {
                if(addedProfileIDs.length > 0){
                    let addedMembers = document.getElementById('membersToAdd');
                    
                    for(let rowIndex = 0; rowIndex < addedProfileIDs.length; rowIndex++){
                        addedMembers.value = addedMembers.value + addedList.rows[rowIndex].cells[6].innerHTML + '-' + addedList.rows[rowIndex].cells[4].getElementsByTagName('select')[0].value + ',';
                    }

                    return true;

                }else{
                    $(".Alert_Message").text("Please add household member.");
                    $(".Add_House_Member_Error").removeClass('visually-hidden');
                    return false;
                }
            };
        //  =========================== END ==============================


        // ------------------ Hide Add Household Member Error Alrt ------------------
            $(".Add_House_Member_Alert_Close").click(function(){
                $(".Add_House_Member_Error").addClass('visually-hidden');
            });
        //  =========================== END ==============================

        // -------------------------- Refreshes the variables used in Adding Household Member when modal is closed --------------------------
        $(".AddHouseholdMemberModalBtn").click(function(){
                document.getElementById('searchedToAdd').value = "";
                document.getElementById('membersToAdd').value = "";
                suggestions.innerHTML = '';
                addedList.innerHTML = '';
                addedProfileIDs = [];
            });
        // =========================== END ===========================


        // -------------- Remove household member Btn clicked --------------
            $(".Remove_House_Member_Btn").click(function(){
                let holder = this.value.split('-');
                document.getElementById('toRemoveMemberHouseholdID').value = holder[0];
                document.getElementById('toRemoveMemberID').value = holder[1];
            });
        // ========== END Remove household member Btn clicked ==========
        

        // ------------------------ View Household Member Info ------------------------
            $(".ViewHouseholdMemberInfoBtn").click(function(){
                document.getElementById("selectedHouseholdMemberToViewID").value = this.value;
                document.getElementById("ViewHouseholdMemberInfoForm").submit();
            });
        // =========================== END ==============================

        // ------------------------ View Household Member Info ------------------------
            $(".AppointHouseholdHeadBtn").click(function(){
                let buttonIDHolder = this.value.split('-');
                document.getElementById("selectedHouseholdInfoID").value = buttonIDHolder[0];
                document.getElementById("selectedHouseholdID").value = buttonIDHolder[1];
                document.getElementById("memberID").value = buttonIDHolder[2];
                document.getElementById("AppointHouseholdHeadForm").submit();
            });
        // =========================== END ==============================

    });

</script>
@endsection