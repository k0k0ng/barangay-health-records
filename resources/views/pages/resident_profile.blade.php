@extends('layouts.app')

@section('content')
    
    <div class="d-block pb-5">

        @if (session('SuccessNotification'))
            <div class="alert alert-success d-flex justify-content-between" role="alert">
                <p class="text-sm mb-0">{{ session('SuccessNotification') }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div> 
        @endif
        @if (session('FailedNotification'))
            <div class="alert alert-danger d-flex justify-content-between" role="alert">
                <p class="text-sm mb-0">{{ session('FailedNotification') }}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div> 
        @endif
            
            <div class="d-flex flex-column">
                    <!-- Nav tabs -->
                    <div class="card">
                        <div class="card-header mt-1 resident-profile-record-main-nav">
                            <ul class="nav nav-tabs justify-content-center" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#residents-info" role="tab">
                                        Resident's Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#walk-in" role="tab">
                                        Walk-In
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#mch" role="tab">
                                        MCH
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#pp" role="tab">
                                        PP
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#epi" role="tab">
                                        EPI
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ufc" role="tab">
                                        UFC
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#fp" role="tab">
                                        FP
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#cdd" role="tab">
                                        CDD
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#mortality" role="tab">
                                        Mortality
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#cari" role="tab">
                                        CARI
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#gms" role="tab">
                                        GMS
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#bip" role="tab">
                                        BIP
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tb-symp" role="tab">
                                        TB SYMP
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#rabies" role="tab">
                                        RABIES
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#sanitation" role="tab">
                                        SANITATION
                                    </a>
                                </li>
                            </ul>
                        </div>

                        {{-- Drop Down Nav for SM Screen --}}
                        <div class="card-header mt-1 resident-profile-record-nav-sm">
                            <div class="dropdown px-3 pt-2 pb-1">
                                <button class="btn btn-primary showDropDownNaveBtn dropdown-toggle" type="button" id="showDropDownNaveBtn" data-bs-toggle="dropdown" aria-expanded="false">
                                    Add Record
                                </button>
                                <ul class="nav nav-tabs justify-content-center dropdown-menu visually-hidden DropDownNavUL" id="DropDownNavUL" aria-labelledby="second-nav" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#residents-info" role="tab">
                                            Resident's Profile
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#walk-in" role="tab">
                                            Walk-In
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#mch" role="tab">
                                            MCH
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#pp" role="tab">
                                            PP
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#epi" role="tab">
                                            EPI
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#ufc" role="tab">
                                            UFC
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#fp" role="tab">
                                            FP
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#cdd" role="tab">
                                            CDD
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#mortality" role="tab">
                                            Mortality
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#cari" role="tab">
                                            CARI
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#gms" role="tab">
                                            GMS
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#bip" role="tab">
                                            BIP
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#tb-symp" role="tab">
                                            TB SYMP
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#rabies" role="tab">
                                            RABIES
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link drop-down-nav-link text-white" data-toggle="tab" href="#sanitation" role="tab">
                                            SANITATION
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{-- END Drop Down Nav for SM Screen --}}                        


                        <hr class="my-2">
                        <div class="card-body mx-2 py-4 AddInfo-card-body pb-5">
                        <!-- Tab panes -->
                            <div class="tab-content ">

                                {{-- Residents Profile --}}
                                <div class="tab-pane active" id="residents-info" role="tabpanel">
                                    <div class="residents-info-pane">
                                        <div>
                                            @php
                                                $path = public_path().'/img/'.$person->img_name.'.'.$person->img_extension;
                                            @endphp

                                            @if (File::exists($path) && $person->img_name != NULL)
                                                <img src="{{URL('img/'.$person->img_name.'.'.$person->img_extension)}}" alt="Resident Image" class="d-block mb-3 mx-auto" style="width:150px;">
                                            @else
                                                @if ($person->gender == "Male")
                                                    <img src="{{ URL('img/male-avatar.png') }}" alt="Resident Image" class="d-block mb-3 mx-auto" style="width:180px;">
                                                @elseif($person->gender == "Female")
                                                    <img src="{{ URL('img/female-avatar.png') }}" alt="Resident Image" class="d-block mb-3 mx-auto" style="width:180px;">
                                                @else
                                                    <img src="{{ URL('img/undefined-avatar.png') }}" alt="Resident Image" class="d-block mb-3 mx-auto" style="width:180px;">
                                                @endif
                                                
                                            @endif

                                            <p id="Residents_Name" class="text-center font-medium text-lg p-0 m-0">{{ $person->first_name.' '.$person->middle_name[0].'. '.$person->last_name }}</p>
                                            <p class="text-xs text-center text-gray-600">Name</p>
                                            <div>
                                                <div class="d-flex justify-content-center mb-3">
                                                    <button type="button" id="EditInfo" class="btn w-32 btn-sm btn-outline-primary Edit_Info_Btn" data-bs-toggle="modal" data-bs-target="#EditInfoModal">
                                                        Edit Profile
                                                    </button>
                                                </div>
                                                <div class="d-flex justify-content-center mb-3">
                                                    <button type="button" id="ViewMedicalRecords" class="btn w-32 btn-sm btn-outline-primary View_Personal_Records_Btn" data-bs-toggle="modal" data-bs-target="#ViewPersonalRecordModal">
                                                        View Records
                                                    </button>
                                                </div>

                                                <div class="d-flex justify-content-center mb-3">
                                                    <button type="button" id="ViewHouseholdRecords" class="btn w-32 btn-sm btn-outline-primary View_Residents_Household_Btn" data-bs-toggle="modal" data-bs-target="#ViewResidentsHouseholdModal">
                                                        Household
                                                    </button>
                                                </div>
                                            </div>

                                            @if ($person->id == Auth::user()->info_id)
                                                <div class="d-flex justify-content-center mb-3">
                                                    <a type="button" id="EditInfo" class="py-2 text-primary px-5 Change_Password_Btn" data-bs-toggle="modal" data-bs-target="#ChangePasswordModal">
                                                        Change Password
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            <div class="d-flex">
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Age: <span id="Age" class="text-lg font-medium text-gray-800 ms-1"> {{ $person->age }} </span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Birthday: <span id="Birthday" class="text-lg font-medium text-gray-800 ms-1">{{  date('M d, Y', strtotime($person->birthdate)) }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Gender: <span id="Gender" class="text-lg font-medium text-gray-800 ms-1">{{ $person->gender }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Height: <span id="Height" class="text-lg font-medium text-gray-800 ms-1">{{ $person->height }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Weight: <span id="Weight" class="text-lg font-medium text-gray-800 ms-1">{{ $person->weight }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Blood Type: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $person->blood_type }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Religion: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $person->religion }}</span> </p>
                                                </div>
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Cell No: <span id="Cell_No" class="text-lg font-medium text-gray-800 ms-1">{{ $person->cell_no }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Email: <span id="Email" class="text-lg font-medium text-gray-800 ms-1">{{ $person->email }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Tell No: <span id="Tell_No" class="text-lg font-medium text-gray-800 ms-1">{{ $person->tell_no }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Civil Status: <span id="Civil_Status" class="text-lg font-medium text-gray-800 ms-1">{{ $person->status }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Inname: <span id="Inname" class="text-lg font-medium text-gray-800 ms-1">{{ $person->inname }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Contact: <span id="Contact" class="text-lg font-medium text-gray-800 ms-1">{{ $person->contact }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Relationship: <span id="Relationship" class="text-lg font-medium text-gray-800 ms-1">{{ $person->relationship }}</span> </p>
                                                </div>
                                            </div>
                                            <hr class="mt-4 mb-4 w-90p">
                                            <div class="d-flex">
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Education: <span id="Education" class="text-lg font-medium text-gray-800 ms-1">{{ $person->education }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Currently Enrolled: <span id="Currently_Enrolled" class="text-lg font-medium text-gray-800 ms-1">{{ $person->cur_enrolled }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Occupation: <span id="Occupation" class="text-lg font-medium text-gray-800 ms-1">{{ $person->occupation }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Currently Employed: <span id="Currently_Employed" class="text-lg font-medium text-gray-800 ms-1">{{ $person->cur_employed }}</span> </p>
                                                </div>
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Registered Voter: <span id="Registered_Voter" class="text-lg font-medium text-gray-800 ms-1">{{ $person->is_voter }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Voter's ID: <span id="Voters_ID" class="text-lg font-medium text-gray-800 ms-1">{{ $person->voter_id }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Barangay ID: <span id="Barangay_ID" class="text-lg font-medium text-gray-800 ms-1">{{ $person->citizen_brgy_id }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Residence Type: <span id="Residence_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $person->residence_type }}</span> </p>
                                                </div>
                                            </div>
                                            <hr class="mt-4 mb-4 w-90p">
                                            <div class="d-flex">
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Address: <span id="Address" class="text-lg font-medium text-gray-800 ms-1">{{ $person->address }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Street: <span id="Street" class="text-lg font-medium text-gray-800 ms-1">{{ $person->street }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Purok: <span id="Purok" class="text-lg font-medium text-gray-800 ms-1">{{ $person->purok_name }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Barangay: <span id="Barangay" class="text-lg font-medium text-gray-800 ms-1">{{ $person->brgy_name }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">City: <span id="City" class="text-lg font-medium text-gray-800 ms-1">{{ $person->city_name }}</span> </p>                                                                
                                                </div>
                                                <div class="col">
                                                    <p class="text-gray-600 text-xs font-semibold">Senior Citizen Member: <span id="Senior_Citizen" class="text-lg font-medium text-gray-800 ms-1">{{ $person->senior_citizen }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">PWD: <span id="PWD" class="text-lg font-medium text-gray-800 ms-1">{{ $person->pwd }}</span> </p>
                                                    <p class="text-gray-600 text-xs font-semibold">Deceased: <span id="Deceased" class="text-lg font-medium text-gray-800 ms-1">{{ $person->deceased }}</span> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Walk-in Pane --}}
                                <div class="tab-pane" id="walk-in" role="tabpanel">
                                    <form action="{{ route('addWalkIn') }}" method="GET" id="Add_Walk_In_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="WalkIn_BloodPressure_ToAdd" class="form-label ms-1">Blood Pressure</label>
                                                <input type="text" id="WalkIn_BloodPressure_ToAdd" name="blood_pressure" class="form-control" aria-label="Blood Pressure">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_BloodPressure visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div>
                                                <label for="WalkIn_BloodSugar_ToAdd" class="form-label ms-1">Blood Sugar</label>
                                                <input type="text" id="WalkIn_BloodSugar_ToAdd" name="blood_sugar" class="form-control" aria-label="Blood Sugar">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_BloodSugar visually-hidden">Please input an informartion.</p>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col">
                                                <label for="WalkIn_Consultation_ToAdd" class="form-label ms-1">Consultation</label>
                                                <input type="text" id="WalkIn_Consultation_ToAdd" name="consultation" class="form-control" aria-label="Consultation">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_Consultation visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div class="col">
                                                <label for="WalkIn_Findings_ToAdd" class="form-label ms-1">Findings</label>
                                                <input type="text" id="WalkIn_Findings_ToAdd" name="findings" class="form-control" aria-label="Findings">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_Findings visually-hidden">Please input an informartion.</p>
                                            </div>
                                        </div>
                                        <div class="mb-4">
                                            <label for="WalkIn_Notes_ToAdd" class="form-label ms-1">Notes</label>
                                            <textarea class="form-control" id="WalkIn_Notes_ToAdd" name="notes" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_Notes visually-hidden">Please input an informartion.</p>
                                        </div>
                                        <div class="font-bold text-gray-800 text-2xl">
                                            Medicine
                                        </div>
                                        <hr class="my-3">
                                        <div class="row mb-5">
                                            <div class="col">
                                                <label for="WalkIn_Medicine_ToAdd" class="form-label ms-1">Medicine</label>
                                                <input type="text" id="WalkIn_Medicine_ToAdd" name="medicine" class="form-control" aria-label="Medicine">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_Medicine visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div class="col">
                                                <label for="WalkIn_Quantity_ToAdd" class="form-label ms-1">Quantity</label>
                                                <input type="text" id="WalkIn_Quantity_ToAdd" name="quantity" class="form-control" aria-label="Quantity">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 WalkIn_Error_Quantity visually-hidden">Please input an informartion.</p>
                                            </div>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_Walk_In_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- MCH Pane --}}
                                <div class="tab-pane" id="mch" role="tabpanel">
                                    <form action="{{ route('addMCH') }}" method="GET" id="Add_MCH_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="MCH_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="MCH_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="MCH_RCode_ToAdd" class="form-label ms-1">R code</label>
                                                <input type="text" id="MCH_RCode_ToAdd" name="rcode" class="form-control" aria-label="R code">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_RCode visually-hidden">Please input an informartion.</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <label for="MCH_G_ToAdd" class="form-label ms-1">G</label>
                                                <input type="text" id="MCH_G_ToAdd" name="g" class="form-control" aria-label="G">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_G visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div>
                                                <label for="MCH_P_ToAdd" class="form-label ms-1">P</label>
                                                <input type="text" id="MCH_P_ToAdd" name="p" class="form-control" aria-label="P">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_P visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div>
                                                <label for="MCH_Level_ToAdd" class="form-label ms-1">Level</label>
                                                <input type="text" id="MCH_Level_ToAdd" name="level" class="form-control" aria-label="Level">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_Level visually-hidden">Please input an informartion.</p>
                                            </div>
                                            
                                        </div>
                                        <div>
                                            <div>
                                                <label for="MCH_Range_ToAdd" class="form-label ms-1">Range</label>
                                                <input type="text" id="MCH_Range_ToAdd" name="range" class="form-control" aria-label="Range">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_Range visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div>
                                                <label for="MCH_LMP_ToAdd" class="form-label ms-1">LMP</label>
                                                <input class="date form-control" type="text" id="MCH_LMP_ToAdd" name="lmp" aria-label="lmp">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_LMP visually-hidden">Please input an informartion.</p>
                                            </div>
                                            <div>
                                                <label for="MCH_EDC_ToAdd" class="form-label ms-1">EDC</label>
                                                <input class="date form-control" type="text" id="MCH_EDC_ToAdd" name="edc" aria-label="edc">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_EDC visually-hidden">Please input an informartion.</p>
                                            </div>
                                        </div>
                                        <div class="mb-5">
                                            <label for="MCH_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="MCH_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 MCH_Error_Remarks visually-hidden">Please input an informartion.</p>
                                        </div> 
                                        
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_MCH_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- PP Pane --}}
                                <div class="tab-pane" id="pp" role="tabpanel">
                                    <form action="{{ route('addPP') }}" method="GET" id="Add_PP_Form" class="h-80p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="PP_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="PP_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="PP_Weight_ToAdd" class="form-label ms-1">Weight</label>
                                                <input type="text" id="PP_Weight_ToAdd" name="weight" class="form-control" aria-label="Weight" @php echo "value='".$person->weight."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_Weight visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="PP_Gender_ToAdd" class="form-label ms-1">Gender</label>
                                                <select class="form-select" id="PP_Gender_ToAdd" name="gender">
                                                    <option value="Male">Male</option>
                                                    @if ( $person->gender == "Female")
                                                        <option value="Female" Selected>Female</option>
                                                    @else
                                                        <option value="Female">Female</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <label for="PP_AttendedBy_ToAdd" class="form-label ms-1">Attended by</label>
                                                <input type="text" id="PP_AttendedBy_ToAdd" name="attended_by" class="form-control" aria-label="Attended by">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_AttendedBy visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="PP_PlaceDelivery_ToAdd" class="form-label ms-1">Place of delivery</label>
                                                <input type="text" id="PP_PlaceDelivery_ToAdd" name="place_delivery" class="form-control" aria-label="Place of delivery">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_PlaceDelivery visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="PP_FDG_ToAdd" class="form-label ms-1">FDG</label>
                                                <input type="text" id="PP_FDG_ToAdd" name="fdg" class="form-control" aria-label="">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_FDG visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="PP_Date_ToAdd" class="form-label ms-1">Date of PP</label>
                                                <input class="date form-control" type="text" id="PP_Date_ToAdd" name="date_pp" aria-label="date_pp">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_Date visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="PP_Vitamina_ToAdd" class="form-label ms-1">Vitamina</label>
                                                <input class="date form-control" type="text" id="PP_Vitamina_ToAdd" name="vitamina" aria-label="vitamina">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_Vitamina visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="PP_DOD_ToAdd" class="form-label ms-1">DOD</label>
                                                <input class="date form-control" type="text" id="PP_DOD_ToAdd" name="dod" aria-label="dod">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_DOD visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-5">
                                            <label for="PP_F_ToAdd" class="form-label ms-1">F</label>
                                            <textarea class="form-control" id="PP_F_ToAdd" name="f" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 PP_Error_F visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_PP_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- EPI Pane --}}
                                <div class="tab-pane" id="epi" role="tabpanel">
                                    <form action="{{ route('addEPI') }}" method="GET" id="Add_EPI_Form" class="h-80p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="EPI_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="EPI_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="EPI_Weight_ToAdd" class="form-label ms-1">Weight</label>
                                                <input type="text" id="EPI_Weight_ToAdd" name="weight" class="form-control" aria-label="Weight" @php echo "value='".$person->weight."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_Weight visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="EPI_RCode_ToAdd" class="form-label ms-1">R Code</label>
                                                <input type="text" id="EPI_RCode_ToAdd" name="r_code" class="form-control" aria-label="R Code">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_RCode visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="EPI_Mother_ToAdd" class="form-label ms-1">Mothers Name</label>
                                                <input type="text" id="EPI_Mother_ToAdd" name="mother_name" class="form-control" placeholder="Enter mothers name" aria-label="Mothers Name">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_Mother visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="EPI_Father_ToAdd" class="form-label ms-1">Fathers Name</label>
                                                <input type="text" id="EPI_Father_ToAdd" name="father_name" class="form-control" placeholder="Enter mothers name" aria-label="Fathers Name">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_Father visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="EPI_FDG_ToAdd" class="form-label ms-1">FDG</label>
                                                <input type="text" id="EPI_FDG_ToAdd" name="fdg" class="form-control" aria-label="FDG">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_FDG visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="EPI_Vaccine_ToAdd" class="form-label ms-1">Vaccine</label>
                                                <input type="text" id="EPI_Vaccine_ToAdd" name="vaccine" class="form-control" aria-label="Vaccine">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 EPI_Error_Vaccine visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>
                                        
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_EPI_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- UFC Pane --}}
                                <div class="tab-pane" id="ufc" role="tabpanel">
                                    <form action="{{ route('addUFC') }}" method="GET" id="Add_UFC_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="UFC_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="UFC_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="UFC_Weight_ToAdd" class="form-label ms-1">Weight</label>
                                                <input type="text" id="UFC_Weight_ToAdd" name="weight" class="form-control" aria-label="Weight" @php echo "value='".$person->weight."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_Weight visually-hidden">Please input a valid number.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="UFC_RCode_ToAdd" class="form-label ms-1">R code</label>
                                                <input type="text" id="UFC_RCode_ToAdd" name="r_code" class="form-control" aria-label="R code">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_RCode visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="UFC_FDG_ToAdd" class="form-label ms-1">FDG</label>
                                                <input type="text" id="UFC_FDG_ToAdd" name="fdg" class="form-control" aria-label="FDG">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_FDG visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="UFC_Mother_ToAdd" class="form-label ms-1">Mothers Name</label>
                                                <input type="text" id="UFC_Mother_ToAdd" name="mother_name" class="form-control" aria-label="Mothers Name">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_Mother visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="UFC_Father_ToAdd" class="form-label ms-1">Fathers Name</label>
                                                <input type="text" id="UFC_Father_ToAdd" name="father_name" class="form-control" aria-label="Fathers Name">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_Father visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-5">
                                            <label for="UFC_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="UFC_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 UFC_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_UFC_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- FP Pane --}}
                                <div class="tab-pane" id="fp" role="tabpanel">
                                    <form action="{{ route('addFP') }}" method="GET" id="Add_FP_Form" class="h-80p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="FP_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="FP_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="FP_NumberChild_ToAdd" class="form-label ms-1">Number of child</label>
                                                <input type="text" id="FP_NumberChild_ToAdd" name="num_child" class="form-control" aria-label="Number of child">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_Num_Child visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="FP_ClientType_ToAdd" class="form-label ms-1">Client Type</label>
                                                <input type="text" id="FP_ClientType_ToAdd" name="client_type" class="form-control" aria-label="Client Type">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_Client_Type visually-hidden">Please input an information.</p>
                                            </div>                                              
                                        </div>

                                        <div>
                                            <div>
                                                <label for="FP_MethodAccepted_ToAdd" class="form-label ms-1">Method Accepted</label>
                                                <input type="text" id="FP_MethodAccepted_ToAdd" name="method_accepted" class="form-control" aria-label="Method Accepted">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_Method_Accepted visually-hidden">Please input an information.</p>
                                            </div> 
                                            <div>
                                                <label for="FP_LMP_ToAdd" class="form-label ms-1">LMP</label>
                                                <input class="date form-control" type="text" id="FP_LMP_ToAdd" name="lmp" aria-label="lmp">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_LMP visually-hidden">Please input LMP.</p>
                                            </div>                                                        
                                        </div>

                                        <div class="mb-5">
                                            <label for="FP_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="FP_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 FP_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>
                                        
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">
                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_FP_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- CDD Pane --}}
                                <div class="tab-pane" id="cdd" role="tabpanel">
                                    <form action="{{ route('addCDD') }}" method="GET" id="Add_CDD_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="CDD_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="CDD_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 CDD_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="CDD_ORNumber_ToAdd" class="form-label ms-1">OR Number</label>
                                                <input type="text" id="CDD_ORNumber_ToAdd" name="or_number" class="form-control" aria-label="OR Number">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 CDD_Error_ORNumber visually-hidden">Please input a valid number.</p>
                                            </div>
                                        </div>
                                        
                                        <div class="mb-5">
                                            <label for="CDD_Complaints_ToAdd" class="form-label ms-1">Complaints</label>
                                            <textarea class="form-control" id="CDD_Complaints_ToAdd" name="complaints" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 CDD_Error_Complaints visually-hidden">Please input an information.</p>
                                        </div>
                                        <div class="mb-5">
                                            <label for="CDD_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="CDD_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 CDD_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_CDD_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- Mortality Pane --}}
                                <div class="tab-pane" id="mortality" role="tabpanel">
                                    <form action="{{ route('addMortality') }}" method="GET" id="Add_Mortality_Form" class="h-80p" autocomplete="off">
                                        <div>
                                            <div>
                                                <div class="mb-5">
                                                    <label for="Mortality_Age_ToAdd" class="form-label ms-1">Age</label>
                                                    <input type="text" id="Mortality_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                    <p class="text-danger text-xs m-0 p-0 mt-2 Mortality_Error_Age visually-hidden">Please input a valid number.</p>
                                                </div>
                                                <div>
                                                    <label for="Mortality_DOD_ToAdd" class="form-label ms-1">Date of Death</label>
                                                    <input type="text" id="Mortality_DOD_ToAdd" name="dod" class="form-control date" aria-label="Date of Death">
                                                    <p class="text-danger text-xs m-0 p-0 mt-2 Mortality_Error_DOD visually-hidden">Please input an information.</p>
                                                </div>
                                            </div> 
                                            <div>
                                                <label for="Mortality_COD_ToAdd" class="form-label ms-1">Cause of Death</label>
                                                <textarea class="form-control" id="Mortality_COD_ToAdd" name="cod" rows="6"></textarea>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Mortality_Error_COD visually-hidden">Please input an information.</p>
                                            </div>                                               
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_Mortality_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- CARI Pane --}}
                                <div class="tab-pane" id="cari" role="tabpanel">
                                    <form action="{{ route('addCari') }}" method="GET" id="Add_CARI_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="CARI_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="CARI_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 CARI_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                        </div>
                                        
                                        <div class="mb-5">
                                            <label for="CARI_Complaints_ToAdd" class="form-label ms-1">Complaints</label>
                                            <textarea class="form-control" id="CARI_Complaints_ToAdd" name="complaints" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 CARI_Error_Complaints visually-hidden">Please input an information.</p>
                                        </div>
                                        <div class="mb-5">
                                            <label for="CARI_HOAdvice_ToAdd" class="form-label ms-1">Advice</label>
                                            <textarea class="form-control" id="CARI_HOAdvice_ToAdd" name="ho_advice" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 CARI_Error_HOAdvice visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_CARI_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- GMS Pane --}}
                                <div class="tab-pane" id="gms" role="tabpanel">
                                    <form action="{{ route('addGMS') }}" method="GET" id="Add_GMS_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="GMS_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="GMS_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 GMS_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>                                                       
                                        </div>

                                        <div class="mb-5">
                                            <label for="GMS_Complaints_ToAdd" class="form-label ms-1">Complaints</label>
                                            <textarea class="form-control" id="GMS_Complaints_ToAdd" name="complaints" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 GMS_Error_Complaints visually-hidden">Please input an information.</p>
                                        </div>

                                        <div class="mb-5">
                                            <label for="GMS_HOAdvice_ToAdd" class="form-label ms-1">Advice</label>
                                            <textarea class="form-control" id="GMS_HOAdvice_ToAdd" name="ho_advice" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 GMS_Error_HOAdvice visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_GMS_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- BIP Pane --}}
                                <div class="tab-pane" id="bip" role="tabpanel">
                                    <form action="{{ route('addBIP') }}" method="GET" id="Add_BIP_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="BIP_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="BIP_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 BIP_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                            <div>
                                                <label for="BIP_BloodPressure_ToAdd" class="form-label ms-1">Blood Pressure</label>
                                                <input type="text" id="BIP_BloodPressure_ToAdd" name="blood_pressure" class="form-control" aria-label="Blood Pressure">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 BIP_Error_BloodPressure visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="BIP_ClientType_ToAdd" class="form-label ms-1">Client Type</label>
                                                <input type="text" id="BIP_ClientType_ToAdd" name="client_type" class="form-control" aria-label="Client Type">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 BIP_Error_ClientType visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>
                                        
                                        <div class="mb-5">
                                            <label for="BIP_FHistory_ToAdd" class="form-label ms-1">History</label>
                                            <textarea class="form-control" id="BIP_FHistory_ToAdd" name="f_history" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 BIP_Error_FHistory visually-hidden">Please input an information.</p>
                                        </div>
                                        <div class="mb-5">
                                            <label for="BIP_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="BIP_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 BIP_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_BIP_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- TB SYMP Pane --}}
                                <div class="tab-pane" id="tb-symp" role="tabpanel">
                                    <form action="{{ route('addTBSymp') }}" method="GET" id="Add_TB_Symp_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="TBSymp_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="TBSymp_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <label for="TBSymp_DOXray_ToAdd" class="form-label ms-1">Date of X-ray</label>
                                                <input type="text" id="TBSymp_DOXray_ToAdd" name="dox_ray" class="form-control date" aria-label="Date of X-ray">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_DOXray visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>
                                        <hr class="my-5">
                                        <div>
                                            <div>
                                                <label for="TBSymp_DateFirst_ToAdd" class="form-label ms-1">Date of First</label>
                                                <input type="text" id="TBSymp_DateFirst_ToAdd" name="date_first" class="form-control date" aria-label="Date of First">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_DateFirst visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="TBSymp_Sputum1_ToAdd" class="form-label ms-1">First Sputum</label>
                                                <input type="text" id="TBSymp_Sputum1_ToAdd" name="sputum1" class="form-control" aria-label="First Sputum">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_Sputum1 visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-4">
                                            <label for="TBSymp_Submit3_ToAdd" class="form-label ms-1">Submit 3</label>
                                            <textarea class="form-control" id="TBSymp_Submit3_ToAdd" name="submit3" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_Submit3 visually-hidden">Please input an information.</p>
                                        </div>
                                        <hr class="my-5">
                                        <div>
                                            <div>
                                                <label for="TBSymp_DateSecond_ToAdd" class="form-label ms-1">Date of Second</label>
                                                <input type="text" id="TBSymp_DateSecond_ToAdd" name="date_second" class="form-control date" aria-label="Date of Second">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_DateSecond visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="TBSymp_Sputum2_ToAdd" class="form-label ms-1">Second Sputum</label>
                                                <input type="text" id="TBSymp_Sputum2_ToAdd" name="sputum2" class="form-control" aria-label="Second Sputum">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_Sputum2 visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-5">
                                            <label for="TBSymp_Result_ToAdd" class="form-label ms-1">Result</label>
                                            <textarea class="form-control" id="TBSymp_Result_ToAdd" name="result" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 TBSymp_Error_Result visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_TB_Symp_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- Rabies Pane --}}
                                <div class="tab-pane" id="rabies" role="tabpanel">
                                    <form action="{{ route('addRabies') }}" method="GET" id="Add_Rabies_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="Rabies_Age_ToAdd" class="form-label ms-1">Age</label>
                                                <input type="text" id="Rabies_Age_ToAdd" name="age" class="form-control" aria-label="Age" @php echo "value='".$person->age."'" @endphp>
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Rabies_Error_Age visually-hidden">Please input a valid number.</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <label for="Rabies_Date_ToAdd" class="form-label ms-1">Date</label>
                                                <input type="text" id="Rabies_Date_ToAdd" name="date" class="form-control date" aria-label="Date">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Rabies_Error_Date visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-5">
                                            <label for="Rabies_ComplaintBite_ToAdd" class="form-label ms-1">Complaint Bite</label>
                                            <textarea class="form-control" id="Rabies_ComplaintBite_ToAdd" name="complaint_bite" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 Rabies_Error_ComplaintBite visually-hidden">Please input an information.</p>
                                        </div>

                                        <div class="mb-5">
                                            <label for="Rabies_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="Rabies_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 Rabies_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_Rabies_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                                {{-- Sanitation Pane --}}
                                <div class="tab-pane" id="sanitation" role="tabpanel">
                                    <form action="{{ route('addSanitation') }}" method="GET" id="Add_Sanitation_Form" class="h-85p" autocomplete="off">
                                        <div>
                                            <div>
                                                <label for="Sanitation_NoToilet_ToAdd" class="form-label ms-1">No Toilet</label>
                                                <input type="text" id="Sanitation_NoToilet_ToAdd" name="no_toilet" class="form-control" aria-label="No Toilet">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Sanitation_Error_NoToilet visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="Sanitation_NotProper_ToAdd" class="form-label ms-1">Not Proper</label>
                                                <input type="text" id="Sanitation_NotProper_ToAdd" name="not_proper" class="form-control" aria-label="Not Proper">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Sanitation_Error_NotProper visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <label for="Sanitation_Poor_ToAdd" class="form-label ms-1">Poor</label>
                                                <input type="text" id="Sanitation_Poor_ToAdd" name="poor" class="form-control" aria-label="Poor">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Sanitation_Error_Poor visually-hidden">Please input an information.</p>
                                            </div>
                                            <div>
                                                <label for="Sanitation_Without_ToAdd" class="form-label ms-1">Without</label>
                                                <input type="text" id="Sanitation_Without_ToAdd" name="without" class="form-control" aria-label="Without">
                                                <p class="text-danger text-xs m-0 p-0 mt-2 Sanitation_Error_Without visually-hidden">Please input an information.</p>
                                            </div>
                                        </div>

                                        <div class="mb-5">
                                            <label for="Sanitation_Remarks_ToAdd" class="form-label ms-1">Remarks</label>
                                            <textarea class="form-control" id="Sanitation_Remarks_ToAdd" name="remarks" rows="3"></textarea>
                                            <p class="text-danger text-xs m-0 p-0 mt-2 Sanitation_Error_Remarks visually-hidden">Please input an information.</p>
                                        </div>

                                        {{-- Hidden tag for data storing only --}}
                                        <input name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                                    </form>

                                    <div class="modal-footer row">
                                        <div class="col d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-secondary ">Cancel</button>
                                            <button type="button" class="btn btn-primary text-white me-4 Add_Sanitation_Btn">Save Record</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                
            </div>


        <!-- ------------------------- Edit Info Modal ------------------------- -->
        <div class="modal fade" id="EditInfoModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Edit Info</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('editResident') }}" role="edit" autocomplete="off" enctype="multipart/form-data">
                            @csrf

                            <div class="mx-4">

                                @php
                                    $path = public_path().'/img/'.$person->img_name.'.'.$person->img_extension;
                                @endphp

                                @if (File::exists($path) && $person->img_name != NULL)
                                    <img src="{{URL('img/'.$person->img_name.'.'.$person->img_extension)}}" alt="Resident Image" class="d-block mb-2 mx-auto" style="width:150px;">
                                @else
                                    @if ($person->gender == "Male")
                                        <img src="{{ URL('img/male-avatar.png') }}" alt="Resident Image" class="d-block mb-2 mx-auto" style="width:180px;">
                                    @elseif($person->gender == "Female")
                                        <img src="{{ URL('img/female-avatar.png') }}" alt="Resident Image" class="d-block mb-2 mx-auto" style="width:180px;">
                                    @else
                                        <img src="{{ URL('img/undefined-avatar.png') }}" alt="Resident Image" class="d-block mb-2 mx-auto" style="width:180px;">
                                    @endif
                                    
                                @endif

                                <div class="d-flex justify-content-center row">
                                    <div class="mb-5 col-4">
                                        <input type="file" class="form-control" id="eResidentImage" name="eResidentImage">
                                    </div>
                                </div>
                                

                                <div class="d-flex gap-4">
                                    <div class="col">
                                        <label for="efirstName" class="form-label ms-1">First Name</label>
                                        <input type="text" name="efirstName" id="efirstName" class="form-control" required>
                                    </div>
                                    <div class="col">
                                        <label for="emiddleName" class="form-label ms-1">Middle Name</label>
                                        <input type="text" name="emiddleName" id="emiddleName" class="form-control" required>
                                    </div>
                                    <div class="col">
                                        <label for="elastName" class="form-label ms-1">Last Name</label>
                                        <input type="text" name="elastName" id="elastName" class="form-control" required>
                                    </div>
                                </div>

                                <div class="d-flex mt-4 gap-5">
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Birthday</span>
                                            <input class="date form-control" type="text" id="eBirthday" name="eBirthday" aria-label="Birthday">
                                        </div>
                                        
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Gender</span>
                                            <select class="form-select" id="eGender" name="eGender">
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Height</span>
                                            <input id="eHeight" name="eHeight" type="text" class="form-control" aria-label="Height">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Weight</span>
                                            <input id="eWeight" name="eWeight" type="text" class="form-control" aria-label="Weight">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Blood Type</span>
                                            <select class="form-select" id="eBlood_Type" name="eBlood_Type">
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="Unknown">Unknown</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Religion</span>
                                            <select class="form-select" name="eReligion" id="eReligion">
                                                <option value="Roman Catholic">Roman Catholic</option>
                                                <option value="Islam">Islam</option>
                                                <option value="Protestant">Protestant</option>
                                                <option value="Iglesia ni Cristo">Iglesia ni Cristo</option>
                                                <option value="Jesus Miracle Crusade International Ministry">Jesus Miracle Crusade International Ministry</option>
                                                <option value="Members Church of God International">Members Church of God International</option>
                                                <option value="Most Holy Church of God in Christ Jesus">Most Holy Church of God in Christ Jesus</option>
                                                <option value="Philippine Independent Church">Philippine Independent Church</option>
                                                <option value="Apostolic Catholic Church">Apostolic Catholic Church</option>
                                                <option value="Orthodoxy">Orthodoxy</option>
                                                <option value="The Kingdom of Jesus Christ the Name Above Every Name">The Kingdom of Jesus Christ the Name Above Every Name</option>
                                                <option value="Judaism">Judaism</option>
                                                <option value="Hinduism">Hinduism</option>
                                                <option value="Atheism">Atheism</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Cell No</span>
                                            <input id="eCell_No" name="eCell_No" type="text" class="form-control" aria-label="Cell No">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Email</span>
                                            <input id="eEmail" name="eEmail" type="email" class="form-control" aria-label="Email">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Tell No</span>
                                            <input id="eTell_No" name="eTell_No" type="text" class="form-control" aria-label="Tell No">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Civil Status</span>
                                            <select class="form-select" id="eCivil_Status" name="eCivil_Status">
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Widowed">Widowed</option>
                                                <option value="Separated">Separated</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Inname</span>
                                            <input id="eInname" name="eInname" type="text" class="form-control" aria-label="Inname">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Contact</span>
                                            <input id="eContact" name="eContact" type="text" class="form-control" aria-label="Contact">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Relationship</span>
                                            <input id="eRelationship" name="eRelationship" type="text" class="form-control" aria-label="Relationship">
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-5">
                                <div class="d-flex gap-5">
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Education</span>
                                            <select class="form-select" id="eEducation" name="eEducation">
                                                <option value="Elementary">Elementary</option>
                                                <option value="High School">High School</option>
                                                <option value="Vocational">Vocational</option>
                                                <option value="College">College</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Currently Enrolled</span>
                                            <select class="form-select" id="eCurrently_Enrolled" name="eCurrently_Enrolled">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Occupation</span>
                                            <input id="eOccupation" name="eOccupation" type="text" class="form-control" aria-label="Occupation">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Currently Employed</span>
                                            <select class="form-select" id="eCurrently_Employed" name="eCurrently_Employed">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Registered Voter</span>
                                            <select class="form-select" id="eRegistered_Voter" name="eRegistered_Voter">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Voters ID</span>
                                            <input id="eVoters_ID" name="eVoters_ID" type="text" class="form-control" aria-label="Voters ID">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Barangay ID</span>
                                            <input id="eBarangay_ID" name="eBarangay_ID" type="text" class="form-control" aria-label="Barangay ID">
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Residence Type</span>
                                            <select class="form-select" id="eResidence_Type" name="eResidence_Type">
                                                <option value="Owned">Owned</option>
                                                <option value="Leased">Leased</option>
                                                <option value="Rented">Rented</option>
                                                <option value="Boarder">Boarder</option>
                                                <option value="Otherwise">Otherwise</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-5">
                                <div class="d-flex gap-5">
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Street</span>
                                            <input id="eStreet" name="eStreet" type="text" class="form-control" aria-label="Street">
                                        </div> 
                                        @if (!$purok_infos->isEmpty())
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="basic-addon1">Purok</span>
                                                <select class="form-select" id="ePurok" name="ePurok">
                                                    @foreach ($purok_infos as $purok_info)
                                                        <option value="{{ $purok_info->id }}">Purok {{ $purok_info->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div> 
                                        @else
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="basic-addon1">Purok</span>
                                                <input id="ePurok" name="ePurok" value="" placeholder="There are no purok in the system." class="form-control" readonly>
                                            </div>
                                        @endif
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Barangay</span>
                                            <input id="eBarangay" name="eBarangay" class="form-control" readonly>
                                        </div>                                                             
                                    </div>
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Senior Citizen</span>
                                            <select class="form-select" id="eSenior_Citizen" name="eSenior_Citizen">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">PWD</span>
                                            <select class="form-select" id="ePWD" name="ePWD">
                                                <option value="N/A">N/A</option>
                                                <option value="Blind">Blind</option>
                                                <option value="Deaf">Deaf</option>
                                                <option value="Mute">Mute</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1">Deceased</span>
                                            <select class="form-select" id="eDeceased" name="eDeceased">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="ResidentID" name="ResidentID" class="visually-hidden" value="{{ $person->id }}">

                            <hr class="my-5">
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary text-white px-4 me-3">Save</button>
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            </div>
                    
                    </form>    

                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End Edit Info Modal ========================== -->


        <!-- ------------------------- View Records Modal ------------------------- -->
        <div class="modal fade" id="ViewPersonalRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content h-100">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Medical Records</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="d-flex justify-content-end my-3">
                            <form action="{{ route('printMedicalRecords') }}" method="GET">
                                {{-- Hidden tag for data storing only --}}
                                <input id="Print_Records_ResidentID" name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                <button type="submit"
                                    @php  
                                        if(empty($all_records[0])){ 
                                            echo "class='btn btn-secondary me-3' disabled";
                                        }else{ 

                                            echo "class='btn btn-primary me-3'";
                                        }  
                                    @endphp
                                >
                                    Print Log
                                </button>
                            </form>                            

                            <div class="dropdown me-1">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="printByTypeDropDown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Print Record
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="printByTypeDropDown">
                                    <form action="{{ route('printWalkInRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($WalkIn_Record->isEmpty()) echo "disabled"; @endphp>
                                            Walk-In
                                        </button>
                                    </form>

                                    <form action="{{ route('printMCHRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($MCH_Record->isEmpty()) echo "disabled"; @endphp>
                                            MCH
                                        </button>
                                    </form>

                                    <form action="{{ route('printPPRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($PP_Record->isEmpty()) echo "disabled"; @endphp>
                                            PP
                                        </button>
                                    </form>

                                    <form action="{{ route('printEPIRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($EPI_Record->isEmpty()) echo "disabled"; @endphp>
                                            EPI
                                        </button>
                                    </form>

                                    <form action="{{ route('printUFCRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($UFC_Record->isEmpty()) echo "disabled"; @endphp>
                                            UFC
                                        </button>
                                    </form>

                                    <form action="{{ route('printFPRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($FP_Record->isEmpty()) echo "disabled"; @endphp>
                                            FP
                                        </button>
                                    </form>

                                    <form action="{{ route('printCDDRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($CDD_Record->isEmpty()) echo "disabled"; @endphp>
                                            CDD
                                        </button>
                                    </form>

                                    <form action="{{ route('printMortalityRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($Mortality_Record->isEmpty()) echo "disabled"; @endphp>
                                            Mortality
                                        </button>
                                    </form>

                                    <form action="{{ route('printCariRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($CARI_Record->isEmpty()) echo "disabled"; @endphp>
                                            CARI
                                        </button>
                                    </form>

                                    <form action="{{ route('printGMSRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($GMS_Record->isEmpty()) echo "disabled"; @endphp>
                                            GMS
                                        </button>
                                    </form>

                                    <form action="{{ route('printBIPRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($BIP_Record->isEmpty()) echo "disabled"; @endphp>
                                            BIP
                                        </button>
                                    </form>
                                    
                                    <form action="{{ route('printTBSympRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($TB_Symp_Record->isEmpty()) echo "disabled"; @endphp>
                                            TB Symp
                                        </button>
                                    </form>

                                    <form action="{{ route('printRabiesRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($Rabies_Record->isEmpty()) echo "disabled"; @endphp>
                                            Rabies
                                        </button>
                                    </form>

                                    <form action="{{ route('printSanitationRecords') }}" method="GET">
                                        {{-- Hidden tag for data storing only --}}
                                        <input name="Print_Records_ResidentID" @php echo"value='".$person->id."'" @endphp class="visually-hidden">
                                        <button type="submit" class="dropdown-item" @php if($Sanitation_Record->isEmpty()) echo "disabled"; @endphp>
                                            Sanitation
                                        </button>
                                    </form>
                                </ul>
                            </div>
                        </div>
                        <table id="ressidents_records_table" class="w-100 table table-bordered table-hover table-responsive">
                            <thead class="bg-gray-800 text-white">
                            <tr>
                                <th scope="col" class="p-3 text-center text-xs font-medium text-gray-500 tracking-wider">
                                    Record Type
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Date Created
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Last Date Updated
                                </th>
                                <th scope="col" class="p-3 text-center text-xs font-medium text-gray-500 tracking-wider">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @if (!empty($all_records[0]))
                                @foreach ($all_records as $record)
                                    <tr>
                                        <td class="px-3 align-middle whitespace-nowrap text-center">{{ $record["type"] }}</td>
                                        <td class="px-3 align-middle">{{ date('M d, Y - h:i A', strtotime($record["created_at"])) }}</td>
                                        <td class="px-3 align-middle">{{ date('M d, Y - h:i A', strtotime($record["updated_at"])) }}</td>
                                        <td class="d-flex justify-content-center text-sm font-medium">
                                            <button type="button" @php echo "value='".$record["ID"]."-".$record["type"]."'"; @endphp class="btn btn-primary btn-sm View_Single_Record_Btn w-20" data-bs-dismiss="modal">
                                                View
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach                                    
                            @endif
                            </tbody>
                        </table>
                        @if (empty($all_records[0]))
                            <div class="m-5 text-center text-gray-600 font-bold">
                                No data available.
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Records Modal ========================== -->


        <!-- ------------------------- View Household Belongs Modal ------------------------- -->
        <div class="modal fade" id="ViewResidentsHouseholdModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content border-0 h-100">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Household</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table id="persons_table" class="w-100 table table-bordered table-hover table-responsive">
                            <thead class="bg-gray-800 text-white">
                            <tr>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    House Status
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Family Role
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Role Status
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Address
                                </th>
                                <th scope="col" class="p-3 text-center text-xs font-medium text-gray-500 tracking-wider">
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($resident_household_belongs as $resident_household)
                                @php
                                    $i++;
                                @endphp
                                <tr @php echo "id='".$resident_household->id."'"; @endphp>
                                    <td class="px-3 align-middle">{{ $resident_household->household_Status == NULL ? 'Active' : 'Deleted' }}</td>
                                    <td class="px-3 align-middle">{{ $resident_household->relationship }}</td>
                                    <td class="px-3 align-middle">{{ $resident_household->deleted_at == NULL ? 'Active' : 'Not active' }}</td>
                                    <td class="px-3 align-middle whitespace-nowrap">Purok {{ $resident_household->household_Purok_Name.', '.$resident_household->household_Barangay_Name }}</td>
                                    <td class="d-flex justify-content-center text-sm font-medium">
                                        <button type="button"  value="{{ $resident_household->household_Info_ID }}"
                                            @php  
                                                if($resident_household->household_Status != NULL){ 
                                                    echo "class='btn btn-secondary btn-sm py-2 px-4' disabled";
                                                }else{ 
                                                    echo 'class="btn btn-primary btn-sm py-2 px-4 View_Household_Btn"';
                                                }  
                                            @endphp
                                        >
                                            View Household
                                        </button>   
                                    </td>
        
                                </tr>
                            @endforeach
        
                            </tbody>
                        </table>

                        {{-- Store Household ID in a hidden input tag for data transfey --}}
                        <form method="GET" action="{{ route('viewHousehold') }}" id="Household_ID_Storage" role="View Household" class="visually-hidden">
                            <input type="text" class="visually-hidden" id="selectedHouseholdID" name="selectedHouseholdID">
                        </form>  

                        @if ($resident_household_belongs->isEmpty())
                            <div class="m-5 text-center text-gray-600 font-bold">
                                No data available.
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Household Belongs Modal ========================== -->


        <!-- ------------------------- View Walk-In Record Modal ------------------------- -->
        <div class="modal fade" id="WalkInRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0 ">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Walk-in Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editWalkIn') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-4">
                                <div class="col-4">
                                    <label for="WalkIn_blood_pressure" class="form-label ms-1">Blood Pressure</label>
                                    <input type="text" id="WalkIn_blood_pressure" name="WalkIn_blood_pressure" class="form-control" aria-label="Blood Pressure" disabled required>
                                </div>
                                <div class="col-4">
                                    <label for="WalkIn_blood_sugar" class="form-label ms-1">Blood Sugar</label>
                                    <input type="text" id="WalkIn_blood_sugar" name="WalkIn_blood_sugar" class="form-control" aria-label="Blood Sugar" disabled required>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col">
                                    <label for="WalkIn_consultation" class="form-label ms-1">Consultation</label>
                                    <input type="text" id="WalkIn_consultation" name="WalkIn_consultation" class="form-control" aria-label="Consultation" disabled required>
                                </div>
                                <div class="col">
                                    <label for="WalkIn_findings" class="form-label ms-1">Findings</label>
                                    <input type="text" id="WalkIn_findings" name="WalkIn_findings" class="form-control" aria-label="Findings" disabled required>
                                </div>
                            </div>
                            <div class="mb-4">
                                <label for="WalkIn_notes" class="form-label ms-1">Notes</label>
                                <textarea class="form-control" id="WalkIn_notes" name="WalkIn_notes" rows="3" disabled required></textarea>
                            </div>
                            <div class="font-bold text-gray-800 text-2xl">
                                Medicine
                            </div>
                            <hr class="my-3">
                            <div class="row mb-5">
                                <div class="col">
                                    <label for="WalkIn_medicine" class="form-label ms-1">Medicine</label>
                                    <input type="text" id="WalkIn_medicine" name="WalkIn_medicine" class="form-control" aria-label="Medicine" disabled required>
                                </div>
                                <div class="col">
                                    <label for="WalkIn_quantity" class="form-label ms-1">Quantity</label>
                                    <input type="text" id="WalkIn_quantity" name="WalkIn_quantity" class="form-control" aria-label="Quantity" disabled required>
                                </div>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="WalkIn_Record_ID" name="WalkIn_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_WalkIn_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden WalkIn_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 WalkIn_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Walk-In Record Modal ========================== -->

        <!-- ------------------------- View MCH Record Modal ------------------------- -->
        <div class="modal fade" id="MCHRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">MCH Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editMCH') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="MCH_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="MCH_age" name="MCH_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                                <div class="col-4">
                                    <label for="MCH_rcode" class="form-label ms-1">R code</label>
                                    <input type="text" id="MCH_rcode" name="MCH_rcode" class="form-control" aria-label="R code" required disabled>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-4">
                                    <label for="MCH_g" class="form-label ms-1">G</label>
                                    <input type="text" id="MCH_g" name="MCH_g" class="form-control" aria-label="G" required disabled>
                                </div>
                                <div class="col-4">
                                    <label for="MCH_p" class="form-label ms-1">P</label>
                                    <input type="text" id="MCH_p" name="MCH_p" class="form-control" aria-label="P" required disabled>
                                </div>
                                <div class="col-4">
                                    <label for="MCH_level" class="form-label ms-1">Level</label>
                                    <input type="text" id="MCH_level" name="MCH_level" class="form-control" aria-label="Level" required disabled>
                                </div>
                                
                            </div>
                            <div class="row mb-5">
                                <div class="col-4">
                                    <label for="MCH_range" class="form-label ms-1">Range</label>
                                    <input type="text" id="MCH_range" name="MCH_range" class="form-control" aria-label="Range" required disabled>
                                </div>
                                <div class="col-4">
                                    <label for="MCH_lmp" class="form-label ms-1">LMP</label>
                                    <input class="date form-control" type="text" id="MCH_lmp" name="MCH_lmp" aria-label="lmp" required disabled>
                                </div>
                                <div class="col-4">
                                    <label for="MCH_edc" class="form-label ms-1">EDC</label>
                                    <input class="date form-control" type="text" id="MCH_edc" name="MCH_edc" aria-label="edc" required disabled>
                                </div>
                            </div>
                            <div class="mb-5">
                                <label for="MCH_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="MCH_remarks" name="MCH_remarks" rows="3" required disabled></textarea>
                            </div> 

                            {{-- Hidden tag for data storing only --}}
                            <input id="MCH_Record_ID" name="MCH_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_MCH_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden MCH_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 MCH_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View MCH Record Modal ========================== -->


        <!-- ------------------------- View PP Record Modal ------------------------- -->
        <div class="modal fade" id="PPRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">PP Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editPP') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="PP_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="PP_age" name="PP_age" class="form-control" aria-label="Age" disabled required>
                                </div>
                                <div class="col-2">
                                    <label for="PP_weight" class="form-label ms-1">Weight</label>
                                    <input type="text" id="PP_weight" name="PP_weight" class="form-control" aria-label="Weight" disabled required>
                                </div>
                                <div class="col-2">
                                    <label for="PP_gender" class="form-label ms-1">Gender</label>
                                    <select class="form-select" id="PP_gender" name="PP_gender" disabled required>
                                        <option value="Male">Male</option>
                                        @if ( $person->gender == "Female")
                                            <option value="Female" Selected>Female</option>
                                        @else
                                            <option value="Female">Female</option>
                                        @endif
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col-4">
                                    <label for="PP_attended_by" class="form-label ms-1">Attended by</label>
                                    <input type="text" id="PP_attended_by" name="PP_attended_by" class="form-control" aria-label="Attended by" disabled required>
                                </div>
                                <div class="col-4">
                                    <label for="PP_place_delivery" class="form-label ms-1">Place of delivery</label>
                                    <input type="text" id="PP_place_delivery" name="PP_place_delivery" class="form-control" aria-label="Place of delivery" disabled required>
                                </div>
                                <div class="col-4">
                                    <label for="PP_fdg" class="form-label ms-1">FDG</label>
                                    <input type="text" id="PP_fdg" name="PP_fdg" class="form-control" aria-label="" disabled required>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col-4">
                                    <label for="PP_date_pp" class="form-label ms-1">Date of PP</label>
                                    <input class="date form-control" type="text" id="PP_date_pp" name="PP_date_pp" aria-label="date_pp" disabled required>
                                </div>
                                <div class="col-4">
                                    <label for="PP_vitamina" class="form-label ms-1">Vitamina</label>
                                    <input class="date form-control" type="text" id="PP_vitamina" name="PP_vitamina" aria-label="vitamina" disabled required>
                                </div>
                                <div class="col-4">
                                    <label for="PP_dod" class="form-label ms-1">DOD</label>
                                    <input class="date form-control" type="text" id="PP_dod" name="PP_dod" aria-label="dod" disabled required>
                                </div>
                                
                                
                            </div>

                            <div class="mb-5">
                                <label for="PP_f" class="form-label ms-1">F</label>
                                <textarea class="form-control" id="PP_f" name="PP_f" rows="3" disabled required></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="PP_Record_ID" name="PP_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_PP_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden PP_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 PP_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View PP Record Modal ========================== -->


        <!-- ------------------------- View EPI Record Modal ------------------------- -->
        <div class="modal fade" id="EPIRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">EPI Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editEPI') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="row col">
                                    <div class="col-2">
                                        <label for="EPI_age" class="form-label ms-1">Age</label>
                                        <input type="text" id="EPI_age" name="EPI_age" class="form-control" aria-label="Age" required disabled>
                                    </div>
                                    <div class="col-2">
                                        <label for="EPI_weight" class="form-label ms-1">Weight</label>
                                        <input type="text" id="EPI_weight" name="EPI_weight" class="form-control" aria-label="Weight" required disabled>
                                    </div>
                                    <div class="col-6">
                                        <label for="EPI_r_code" class="form-label ms-1">R Code</label>
                                        <input type="text" id="EPI_r_code" name="EPI_r_code" class="form-control" aria-label="R Code" required disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col">
                                    <label for="EPI_mother_name" class="form-label ms-1">Mothers Name</label>
                                    <input type="text" id="EPI_mother_name" name="EPI_mother_name" class="form-control" placeholder="Enter mothers name" aria-label="Mothers Name" required disabled>
                                </div>
                                <div class="col">
                                    <label for="EPI_father_name" class="form-label ms-1">Fathers Name</label>
                                    <input type="text" id="EPI_father_name" name="EPI_father_name" class="form-control" placeholder="Enter mothers name" aria-label="Fathers Name" required disabled>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col">
                                    <label for="EPI_fdg" class="form-label ms-1">FDG</label>
                                    <input type="text" id="EPI_fdg" name="EPI_fdg" class="form-control" aria-label="FDG" required disabled>
                                </div>
                                <div class="col">
                                    <label for="EPI_vaccine" class="form-label ms-1">Vaccine</label>
                                    <input type="text" id="EPI_vaccine" name="EPI_vaccine" class="form-control" aria-label="Vaccine" required disabled>
                                </div>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="EPI_Record_ID" name="EPI_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_EPI_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden EPI_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 EPI_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View EPI Record Modal ========================== -->


        <!-- ------------------------- View UFC Record Modal ------------------------- -->
        <div class="modal fade" id="UFCRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">UFC Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editUFC') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="UFC_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="UFC_age" name="UFC_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                                <div class="col-2">
                                    <label for="UFC_weight" class="form-label ms-1">Weight</label>
                                    <input type="text" id="UFC_weight" name="UFC_weight" class="form-control" aria-label="Weight" required disabled>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col">
                                    <label for="UFC_r_code" class="form-label ms-1">R code</label>
                                    <input type="text" id="UFC_r_code" name="UFC_r_code" class="form-control" aria-label="R code" required disabled>
                                </div>
                                <div class="col">
                                    <label for="UFC_fdg" class="form-label ms-1">FDG</label>
                                    <input type="text" id="UFC_fdg" name="UFC_fdg" class="form-control" aria-label="FDG" required disabled>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col">
                                    <label for="UFC_mother_name" class="form-label ms-1">Mothers Name</label>
                                    <input type="text" id="UFC_mother_name" name="UFC_mother_name" class="form-control" aria-label="Mothers Name" required disabled>
                                </div>
                                <div class="col">
                                    <label for="UFC_father_name" class="form-label ms-1">Fathers Name</label>
                                    <input type="text" id="UFC_father_name" name="UFC_father_name" class="form-control" aria-label="Fathers Name" required disabled>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label for="UFC_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="UFC_remarks" name="UFC_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="UFC_Record_ID" name="UFC_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_UFC_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden UFC_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 UFC_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View UFC Record Modal ========================== -->


        <!-- ------------------------- View FP Record Modal ------------------------- -->
        <div class="modal fade" id="FPRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">FP Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editFP') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="FP_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="FP_age" name="FP_age" class="form-control" aria-label="Age" required disabled>
                                    <p class="text-danger text-xs m-0 p-0 mt-2 Edit_FP_Error_Age visually-hidden">Please input a valid number.</p>
                                </div>
                                <div class="col-3">
                                    <label for="FP_num_child" class="form-label ms-1">Number of child</label>
                                    <input type="text" id="FP_num_child" name="FP_num_child" class="form-control" aria-label="Number of child" required disabled>
                                    <p class="text-danger text-xs m-0 p-0 mt-2 Edit_FP_Error_Num_Child visually-hidden">Please input a valid number.</p>
                                </div>                                      
                            </div>

                            <div class="row mb-5">
                                <div class="col">
                                    <label for="FP_client_type" class="form-label ms-1">Client Type</label>
                                    <input type="text" id="FP_client_type" name="FP_client_type" class="form-control" aria-label="Client Type" required disabled>
                                </div> 
                                <div class="col">
                                    <label for="FP_method_accepted" class="form-label ms-1">Method Accepted</label>
                                    <input type="text" id="FP_method_accepted" name="FP_method_accepted" class="form-control" aria-label="Method Accepted" required disabled>
                                </div> 
                                <div class="col">
                                    <label for="FP_lmp" class="form-label ms-1">LMP</label>
                                    <input class="date form-control" type="text" id="FP_lmp" name="FP_lmp" aria-label="lmp" required disabled>
                                </div>                                                        
                            </div>

                            <div class="mb-5">
                                <label for="FP_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="FP_remarks" name="FP_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="FP_Record_ID" name="FP_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_FP_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden FP_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 FP_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View FP Record Modal ========================== -->


        <!-- ------------------------- View CDD Record Modal ------------------------- -->
        <div class="modal fade" id="CDDRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">CDD Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editCDD') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="CDD_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="CDD_age" name="CDD_age" class="form-control" aria-label="Age" required disabled>
                                    <p class="text-danger text-xs m-0 p-0 mt-2 Edit_CDD_Error_Age visually-hidden">Please input a valid number.</p>
                                </div>
                                <div class="col-3">
                                    <label for="CDD_or_number" class="form-label ms-1">OR Number</label>
                                    <input type="text" id="CDD_or_number" name="CDD_or_number" class="form-control" aria-label="OR Number" required disabled>
                                    <p class="text-danger text-xs m-0 p-0 mt-2 Edit_CDD_Error_OR_Num visually-hidden">Please input a valid number.</p>
                                </div>
                            </div>
                            
                            <div class="mb-5">
                                <label for="CDD_complaints" class="form-label ms-1">Complaints</label>
                                <textarea class="form-control" id="CDD_complaints" name="CDD_complaints" rows="3" required disabled></textarea>
                            </div>
                            <div class="mb-5">
                                <label for="CDD_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="CDD_remarks" name="CDD_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="CDD_Record_ID" name="CDD_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_CDD_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden CDD_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 CDD_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View CDD Record Modal ========================== -->


        <!-- ------------------------- View Mortality Record Modal ------------------------- -->
        <div class="modal fade" id="MortalityRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Mortality Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editMortality') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5 gap-4">
                                <div class="col-3">
                                    <label for="Mortality_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="Mortality_age" name="Mortality_age" class="form-control" aria-label="Age" required disabled>
                                </div> 
                                <div class="col-3">
                                    <label for="Mortality_dod" class="form-label ms-1">Date of Death</label>
                                    <input type="text" id="Mortality_dod" name="Mortality_dod" class="form-control date" aria-label="Date of Death" required disabled>
                                </div>
                                <div class="col-12">
                                    <label for="Mortality_cod" class="form-label ms-1">Cause of Death</label>
                                    <textarea class="form-control" id="Mortality_cod" name="Mortality_cod" rows="6" required disabled></textarea>
                                </div>
                            </div>  
                            

                            {{-- Hidden tag for data storing only --}}
                            <input id="Mortality_Record_ID" name="Mortality_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_Mortality_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden Mortality_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 Mortality_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Mortality Record Modal ========================== -->


        <!-- ------------------------- View CARI Record Modal ------------------------- -->
        <div class="modal fade" id="CariRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Cari Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editCari') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="Cari_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="Cari_age" name="Cari_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                            </div>
                            
                            <div class="mb-5">
                                <label for="Cari_complaints" class="form-label ms-1">Complaints</label>
                                <textarea class="form-control" id="Cari_complaints" name="Cari_complaints" rows="3" required disabled></textarea>
                            </div>
                            <div class="mb-5">
                                <label for="Cari_ho_advice" class="form-label ms-1">Advice</label>
                                <textarea class="form-control" id="Cari_ho_advice" name="Cari_ho_advice" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="Cari_Record_ID" name="Cari_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_Cari_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden Cari_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 Cari_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View CARI Record Modal ========================== -->


        <!-- ------------------------- View GMS Record Modal ------------------------- -->
        <div class="modal fade" id="GMSRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">GMS Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editGMS') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="GMS_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="GMS_age" name="GMS_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                            </div>
                            
                            <div class="mb-5">
                                <label for="GMS_complaints" class="form-label ms-1">Complaints</label>
                                <textarea class="form-control" id="GMS_complaints" name="GMS_complaints" rows="3" required disabled></textarea>
                            </div>
                            <div class="mb-5">
                                <label for="GMS_ho_advice" class="form-label ms-1">Advice</label>
                                <textarea class="form-control" id="GMS_ho_advice" name="GMS_ho_advice" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="GMS_Record_ID" name="GMS_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_GMS_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden GMS_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 GMS_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View GMS Record Modal ========================== -->


        <!-- ------------------------- View BIP Record Modal ------------------------- -->
        <div class="modal fade" id="BIPRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">BIP Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editBIP') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="BIP_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="BIP_age" name="BIP_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                                <div class="col-3">
                                    <label for="BIP_blood_pressure" class="form-label ms-1">Blood Pressure</label>
                                    <input type="text" id="BIP_blood_pressure" name="BIP_blood_pressure" class="form-control" aria-label="Blood Pressure" required disabled>
                                </div>
                                <div class="col-5">
                                    <label for="BIP_client_type" class="form-label ms-1">Client Type</label>
                                    <input type="text" id="BIP_client_type" name="BIP_client_type" class="form-control" aria-label="Client Type" required disabled>
                                </div>
                            </div>
                            
                            <div class="mb-5">
                                <label for="BIP_f_history" class="form-label ms-1">History</label>
                                <textarea class="form-control" id="BIP_f_history" name="BIP_f_history" rows="3" required disabled></textarea>
                            </div>
                            <div class="mb-5">
                                <label for="BIP_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="BIP_remarks" name="BIP_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="BIP_Record_ID" name="BIP_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_BIP_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden BIP_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 BIP_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View BIP Record Modal ========================== -->


        <!-- ------------------------- View TB Symp Record Modal ------------------------- -->
        <div class="modal fade" id="TBSympRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">TB Symp Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editTBSymp') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="TBSymp_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="TBSymp_age" name="TBSymp_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <label for="TBSymp_dox_ray" class="form-label ms-1">Date of X-ray</label>
                                    <input type="text" id="TBSymp_dox_ray" name="TBSymp_dox_ray" class="form-control date" aria-label="Date of X-ray" required disabled>
                                </div>
                            </div>
                            <hr class="my-5">
                            <div class="row mb-4">
                                <div class="col-3">
                                    <label for="TBSymp_date_first" class="form-label ms-1">Date of First</label>
                                    <input type="text" id="TBSymp_date_first" name="TBSymp_date_first" class="form-control date" aria-label="Date of First" required disabled>
                                </div>
                                <div class="col">
                                    <label for="TBSymp_sputum1" class="form-label ms-1">First Sputum</label>
                                    <input type="text" id="TBSymp_sputum1" name="TBSymp_sputum1" class="form-control" aria-label="First Sputum" required disabled>
                                </div>
                            </div>

                            <div class="mb-4">
                                <label for="TBSymp_submit3" class="form-label ms-1">Submit 3</label>
                                <textarea class="form-control" id="TBSymp_submit3" name="TBSymp_submit3" rows="3" required disabled></textarea>
                            </div>
                            <hr class="my-5">
                            <div class="row mb-4">
                                <div class="col-3">
                                    <label for="TBSymp_date_second" class="form-label ms-1">Date of Second</label>
                                    <input type="text" id="TBSymp_date_second" name="TBSymp_date_second" class="form-control date" aria-label="Date of Second" required disabled>
                                </div>
                                <div class="col">
                                    <label for="TBSymp_sputum2" class="form-label ms-1">Second Sputum</label>
                                    <input type="text" id="TBSymp_sputum2" name="TBSymp_sputum2" class="form-control" aria-label="Second Sputum" required disabled>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label for="TBSymp_result" class="form-label ms-1">Result</label>
                                <textarea class="form-control" id="TBSymp_result" name="TBSymp_result" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="TBSymp_Record_ID" name="TBSymp_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_TBSymp_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden TBSymp_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 TBSymp_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View TB Symp Record Modal ========================== -->


        <!-- ------------------------- View Rabies Record Modal ------------------------- -->
        <div class="modal fade" id="RabiesRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Rabies Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editRabies') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-2">
                                    <label for="Rabies_age" class="form-label ms-1">Age</label>
                                    <input type="text" id="Rabies_age" name="Rabies_age" class="form-control" aria-label="Age" required disabled>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-3">
                                    <label for="Rabies_date" class="form-label ms-1">Date</label>
                                    <input type="text" id="Rabies_date" name="Rabies_date" class="form-control date" aria-label="Date" required disabled>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label for="Rabies_complaint_bite" class="form-label ms-1">Complaint Bite</label>
                                <textarea class="form-control" id="Rabies_complaint_bite" name="Rabies_complaint_bite" rows="3" required disabled></textarea>
                            </div>

                            <div class="mb-5">
                                <label for="Rabies_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="Rabies_remarks" name="Rabies_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="Rabies_Record_ID" name="Rabies_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_Rabies_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden Rabies_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 Rabies_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Rabies Record Modal ========================== -->


        <!-- ------------------------- View Sanitation Record Modal ------------------------- -->
        <div class="modal fade" id="SanitationRecordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header bg-gray-800 text-white">
                        <h5 class="modal-title" id="staticBackdropLabel">Sanitation Record</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('editSanitation') }}" method="POST" class="h-85p mt-3" autocomplete="off">
                            @csrf
                            <div class="row mb-5">
                                <div class="col-3">
                                    <label for="Sanitation_no_toilet" class="form-label ms-1">No Toilet</label>
                                    <input type="text" id="Sanitation_no_toilet" name="Sanitation_no_toilet" class="form-control" aria-label="No Toilet" required disabled>
                                </div>
                                <div class="col-3">
                                    <label for="Sanitation_not_proper" class="form-label ms-1">Not Proper</label>
                                    <input type="text" id="Sanitation_not_proper" name="Sanitation_not_proper" class="form-control" aria-label="Not Proper" required disabled>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <div class="col-3">
                                    <label for="Sanitation_poor" class="form-label ms-1">Poor</label>
                                    <input type="text" id="Sanitation_poor" name="Sanitation_poor" class="form-control" aria-label="Poor" required disabled>
                                </div>
                                <div class="col-3">
                                    <label for="Sanitation_without" class="form-label ms-1">Without</label>
                                    <input type="text" id="Sanitation_without" name="Sanitation_without" class="form-control" aria-label="Without" required disabled>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label for="Sanitation_remarks" class="form-label ms-1">Remarks</label>
                                <textarea class="form-control" id="Sanitation_remarks" name="Sanitation_remarks" rows="3" required disabled></textarea>
                            </div>

                            {{-- Hidden tag for data storing only --}}
                            <input id="Sanitation_Record_ID" name="Sanitation_Record_ID" class="visually-hidden">

                            <div class="modal-footer row">
                                <div class="col d-flex flex-row-reverse">
                                    <button type="button" class="btn btn-primary Edit_Sanitation_Record">Edit</button>
                                    <button type="button" class="btn btn-secondary visually-hidden Sanitation_Cancel_Btn">Cancel</button>
                                    <button type="submit" class="btn btn-primary text-white me-4 Sanitation_Save_Btn visually-hidden">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========================== End View Sanitation Record Modal ========================== -->


        <!-- ------------------------- Change Password Modal ------------------------- -->
        <div class="modal fade" id="ChangePasswordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0">
                <div class="modal-header bg-gray-800 text-white">
                    <h5 class="modal-title" id="staticBackdropLabel">Change Password</h5>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('changePassword') }}" method="POST" autocomplete="off" role="Remove Household Member">
                        @csrf
                        <div class="col pt-2">
                            <label for="current_password" class="form-label ms-1">Please enter current password:</label>
                            <input type="password" name="current_password" class="form-control" aria-label="current_password" required>
                        </div>

                        <div class="col pt-4">
                            <label for="new_password" class="form-label ms-1">Please enter new password:</label>
                            <input type="password" name="new_password" class="form-control" aria-label="new_password" required>
                        </div>

                        <div class="col pt-2">
                            <label for="confirmation_password" class="form-label ms-1">Please re-enter new password:</label>
                            <input type="password" name="confirmation_password" class="form-control" aria-label="confirmation_password" required>
                        </div>
        
                        <div class="modal-footer row mt-4">
                            <div class="col d-flex flex-row-reverse p-0">
                                <button type="submit" class="btn btn-danger text-white me-4 w-24">Continue</button>
                            </div>
                        </div>
                    </form>
                
                </div>
            </div>
            </div>
        </div>
        <!-- ========================== End Change Password Modal ========================== -->


    </div>


    {{-- --------------------------- Custom Scripts --------------------------- --}}
    <script type="text/javascript">

        // ----------- Date Picker ------------ 
            $('.date').datepicker({  
               format: 'yyyy-mm-dd'
            }); 
        // ============== END ==============

            

            $(document).ready(function(){

            // -------- Drop Down Nav Tablet Screen Size --------

                const activeNav = document.getElementById('showDropDownNaveBtn');
                const dropDownNavUL = document.getElementById('DropDownNavUL');

                $(document).on('click','.showDropDownNaveBtn', function(event){
                    
                    if (activeNav.classList.contains('show')) {
                        $('.DropDownNavUL').removeClass('visually-hidden');
                    } else {
                        dropDownNavUL.classList.add('visually-hidden');
                    }
                });

                $(document).on('click','.drop-down-nav-link', function(event){
                    if (!activeNav.classList.contains('show')) {
                        dropDownNavUL.classList.add('visually-hidden');
                    }
                });

                $(document).click(function(event) { 
                    var $target = $(event.target);
                    if(!$target.closest('#showDropDownNaveBtn').length && 
                    $('#DropDownNavUL').is(":visible")) {
                        dropDownNavUL.classList.add('visually-hidden');
                    }        
                });

            // === END Drop Down Nav ===
                
            // ------------- Get Age via Birthdate ----------------
                function getAge(dateString) {
                    var today = new Date();
                    var birthDate = new Date(dateString);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    return age;
                }
            // ============== END Get Age via Birthdate ==============
                

            // -------------- Show Edit Info Modal and close Add Info Modal --------------
                $(document).on('click','.Edit_Info_Btn', function(event){

                    var residence_info = {!! json_encode($person, JSON_HEX_TAG) !!};

                    document.getElementById("efirstName").value = residence_info['first_name'];
                    document.getElementById("emiddleName").value = residence_info['middle_name'];
                    document.getElementById("elastName").value = residence_info['last_name'];

                    document.getElementById("eGender").value = (!residence_info['gender'] ? "" : residence_info['gender']);
                    document.getElementById("eBirthday").value = (!residence_info['birthdate'] ? "" : residence_info['birthdate']);
                    document.getElementById("eHeight").value = (!residence_info['height'] ? "" : residence_info['height']);
                    document.getElementById("eWeight").value = (!residence_info['weight'] ? "" : residence_info['weight']);
                    document.getElementById("eBlood_Type").value = (!residence_info['blood_type'] ? "" : residence_info['blood_type']);
                    document.getElementById("eReligion").value = (!residence_info['religion'] ? "" : residence_info['religion']);

                    document.getElementById("eCell_No").value = (!residence_info['cell_no'] ? "" : residence_info['cell_no']);
                    document.getElementById("eEmail").value = (!residence_info['email'] ? "" : residence_info['email']);
                    document.getElementById("eTell_No").value = (!residence_info['tell_no'] ? "" : residence_info['tell_no']);
                    document.getElementById("eCivil_Status").value = (!residence_info['status'] ? "" : residence_info['status']);
                    document.getElementById("eInname").value = (!residence_info['inname'] ? "" : residence_info['inname']);
                    document.getElementById("eContact").value = (!residence_info['contact'] ? "" : residence_info['contact']);
                    document.getElementById("eRelationship").value = (!residence_info['relationship'] ? "" : residence_info['relationship']);

                    document.getElementById("eEducation").value = (!residence_info['education'] ? "" : residence_info['education']);
                    document.getElementById("eCurrently_Enrolled").value = (!residence_info['cur_enrolled'] ? "" : residence_info['cur_enrolled']);
                    document.getElementById("eOccupation").value = (!residence_info['occupation'] ? "" : residence_info['occupation']);
                    document.getElementById("eCurrently_Employed").value = (!residence_info['cur_employed'] ? "" : residence_info['cur_employed']);

                    document.getElementById("eRegistered_Voter").value = (!residence_info['is_voter'] ? "" : residence_info['is_voter']);
                    document.getElementById("eVoters_ID").value = (!residence_info['voter_id'] ? "" : residence_info['voter_id']);
                    document.getElementById("eBarangay_ID").value = (!residence_info['citizen_brgy_id'] ? "" : residence_info['citizen_brgy_id']);
                    document.getElementById("eResidence_Type").value = (!residence_info['residence_type'] ? "" : residence_info['residence_type']);
                    
                    document.getElementById("eStreet").value = (!residence_info['street'] ? "" : residence_info['street']);
                    document.getElementById("ePurok").value = (!residence_info['purok_ID'] ? "" : residence_info['purok_ID']);
                    document.getElementById("eBarangay").value = (!residence_info['brgy_name'] ? "" : residence_info['brgy_name']);

                    document.getElementById("eSenior_Citizen").value = (!residence_info['senior_citizen'] ? "" : residence_info['senior_citizen']);
                    document.getElementById("ePWD").value = (!residence_info['pwd'] ? "" : residence_info['pwd']);
                    document.getElementById("eDeceased").value = (!residence_info['deceased'] ? "" : residence_info['deceased']);

                });
            // ============== END Show Edit Info Modal and populate field ==============


            // ----------------- Add Walk In Record Btn -----------------
                $(document).on('click','.Add_Walk_In_Btn', function(event){
                    let blood_pressure = document.forms["Add_Walk_In_Form"]["WalkIn_BloodPressure_ToAdd"].value;
                    let blood_sugar = document.forms["Add_Walk_In_Form"]["WalkIn_BloodSugar_ToAdd"].value;
                    let consultation = document.forms["Add_Walk_In_Form"]["WalkIn_Consultation_ToAdd"].value;
                    let findings = document.forms["Add_Walk_In_Form"]["WalkIn_Findings_ToAdd"].value;
                    let notes = document.forms["Add_Walk_In_Form"]["WalkIn_Notes_ToAdd"].value;
                    let medicine = document.forms["Add_Walk_In_Form"]["WalkIn_Medicine_ToAdd"].value;
                    let quantity = document.forms["Add_Walk_In_Form"]["WalkIn_Quantity_ToAdd"].value;

                    if(blood_pressure != "" && blood_sugar != "" && consultation != "" && findings != "" && notes != "" && medicine != "" && quantity != ""){
                        document.getElementById("Add_Walk_In_Form").submit();
                    }

                    if(blood_pressure == ""){
                        $('#WalkIn_BloodPressure_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_BloodPressure').removeClass('visually-hidden');
                    }
                    
                    if(blood_sugar == ""){
                        $('#WalkIn_BloodSugar_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_BloodSugar').removeClass('visually-hidden');
                    }

                    if(consultation == ""){
                        $('#WalkIn_Consultation_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_Consultation').removeClass('visually-hidden');
                    }
                    
                    if(findings == ""){
                        $('#WalkIn_Findings_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_Findings').removeClass('visually-hidden');
                    }

                    if(notes == ""){
                        $('#WalkIn_Notes_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_Notes').removeClass('visually-hidden');
                    }
                    
                    if(medicine == ""){
                        $('#WalkIn_Medicine_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_Medicine').removeClass('visually-hidden');
                    }

                    if(quantity == ""){
                        $('#WalkIn_Quantity_ToAdd').css('border','1px solid red');
                        $('.WalkIn_Error_Quantity').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END Add Walk In Record Btn ==============


            // ----------------- Add MCH Record Btn -----------------
                $(document).on('click','.Add_MCH_Btn', function(event){

                    let age = document.forms["Add_MCH_Form"]["MCH_Age_ToAdd"].value;
                    let g = document.forms["Add_MCH_Form"]["MCH_G_ToAdd"].value;
                    let p = document.forms["Add_MCH_Form"]["MCH_P_ToAdd"].value;
                    let rcode = document.forms["Add_MCH_Form"]["MCH_RCode_ToAdd"].value;
                    let level = document.forms["Add_MCH_Form"]["MCH_Level_ToAdd"].value;
                    let range = document.forms["Add_MCH_Form"]["MCH_Range_ToAdd"].value;
                    let lmp = document.forms["Add_MCH_Form"]["MCH_LMP_ToAdd"].value;
                    let edc = document.forms["Add_MCH_Form"]["MCH_EDC_ToAdd"].value;
                    let remarks = document.forms["Add_MCH_Form"]["MCH_Remarks_ToAdd"].value;

                    
                    if((age/age) && age != "" && g != "" && p != "" && rcode != "" && level != "" && range != "" && lmp != "" && edc != "" && remarks != ""){
                        document.getElementById("Add_MCH_Form").submit();
                    }

                    if(!(age/age)){
                        $('#MCH_Age_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_Age').removeClass('visually-hidden');
                    }

                    if(g == ""){
                        $('#MCH_G_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_G').removeClass('visually-hidden');
                    }
                    
                    if(p == ""){
                        $('#MCH_P_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_P').removeClass('visually-hidden');
                    }

                    if(rcode == ""){
                        $('#MCH_RCode_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_RCode').removeClass('visually-hidden');
                    }
                    
                    if(level == ""){
                        $('#MCH_Level_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_Level').removeClass('visually-hidden');
                    }

                    if(range == ""){
                        $('#MCH_Range_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_Range').removeClass('visually-hidden');
                    }

                    if(lmp == ""){
                        $('#MCH_LMP_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_LMP').removeClass('visually-hidden');
                    }

                    if(edc == ""){
                        $('#MCH_EDC_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_EDC').removeClass('visually-hidden');
                    }
                    
                    if(remarks == ""){
                        $('#MCH_Remarks_ToAdd').css('border','1px solid red');
                        $('.MCH_Error_Remarks').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END MCH Record Btn ==============


            // ----------------- Add PP Record Btn -----------------
                $(document).on('click','.Add_PP_Btn', function(event){

                    let age = document.forms["Add_PP_Form"]["PP_Age_ToAdd"].value;
                    let place_delivery = document.forms["Add_PP_Form"]["PP_PlaceDelivery_ToAdd"].value;
                    let attended_by = document.forms["Add_PP_Form"]["PP_AttendedBy_ToAdd"].value;
                    let gender = document.forms["Add_PP_Form"]["PP_Gender_ToAdd"].value;
                    let fdg = document.forms["Add_PP_Form"]["PP_FDG_ToAdd"].value;
                    let weight = document.forms["Add_PP_Form"]["PP_Weight_ToAdd"].value;
                    let date_pp = document.forms["Add_PP_Form"]["PP_Date_ToAdd"].value;
                    let vitamina = document.forms["Add_PP_Form"]["PP_Vitamina_ToAdd"].value;
                    let dod = document.forms["Add_PP_Form"]["PP_DOD_ToAdd"].value;
                    let f = document.forms["Add_PP_Form"]["PP_F_ToAdd"].value;
                    

                    if((age/age) && age != "" && (weight/weight) && weight != "" && place_delivery != "" && attended_by != "" && gender != "" && fdg != "" && date_pp != "" && vitamina != "" && dod != "" && f != ""){
                        document.getElementById("Add_PP_Form").submit();
                    }

                    if(!(age/age)){
                        $('#PP_Age_ToAdd').css('border','1px solid red');
                        $('.PP_Error_Age').removeClass('visually-hidden');
                    }

                    if(!(weight/weight)){
                        $('#PP_Weight_ToAdd').css('border','1px solid red');
                        $('.PP_Error_Weight').removeClass('visually-hidden');
                    }

                    if(place_delivery == ""){
                        $('#PP_PlaceDelivery_ToAdd').css('border','1px solid red');
                        $('.PP_Error_PlaceDelivery').removeClass('visually-hidden');
                    }
                    
                    if(attended_by == ""){
                        $('#PP_AttendedBy_ToAdd').css('border','1px solid red');
                        $('.PP_Error_AttendedBy').removeClass('visually-hidden');
                    }
                    
                    if(fdg == ""){
                        $('#PP_FDG_ToAdd').css('border','1px solid red');
                        $('.PP_Error_FDG').removeClass('visually-hidden');
                    }

                    if(date_pp == ""){
                        $('#PP_Date_ToAdd').css('border','1px solid red');
                        $('.PP_Error_Date').removeClass('visually-hidden');
                    }

                    if(vitamina == ""){
                        $('#PP_Vitamina_ToAdd').css('border','1px solid red');
                        $('.PP_Error_Vitamina').removeClass('visually-hidden');
                    }

                    if(dod == ""){
                        $('#PP_DOD_ToAdd').css('border','1px solid red');
                        $('.PP_Error_DOD').removeClass('visually-hidden');
                    }
                    
                    if(f == ""){
                        $('#PP_F_ToAdd').css('border','1px solid red');
                        $('.PP_Error_F').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END PP Record Btn ==============


            // ----------------- Add EPI Record Btn -----------------
                $(document).on('click','.Add_EPI_Btn', function(event){

                    let age = document.forms["Add_EPI_Form"]["EPI_Age_ToAdd"].value;
                    let mother_name = document.forms["Add_EPI_Form"]["EPI_Mother_ToAdd"].value;
                    let father_name = document.forms["Add_EPI_Form"]["EPI_Father_ToAdd"].value;
                    let fdg = document.forms["Add_EPI_Form"]["EPI_FDG_ToAdd"].value;
                    let weight = document.forms["Add_EPI_Form"]["EPI_Weight_ToAdd"].value;
                    let r_code = document.forms["Add_EPI_Form"]["EPI_RCode_ToAdd"].value;
                    let vaccine = document.forms["Add_EPI_Form"]["EPI_Vaccine_ToAdd"].value;

                    if((age/age) && age != "" && (weight/weight) && weight != "" && mother_name != "" && father_name != "" && fdg != "" && r_code != "" && vaccine != ""){
                        document.getElementById("Add_EPI_Form").submit();
                    }

                    if(!(age/age)){
                        $('#EPI_Age_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_Age').removeClass('visually-hidden');
                    }

                    if(!(weight/weight)){
                        $('#EPI_Weight_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_Weight').removeClass('visually-hidden');
                    }

                    if(mother_name == ""){
                        $('#EPI_Mother_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_Mother').removeClass('visually-hidden');
                    }
                    
                    if(father_name == ""){
                        $('#EPI_Father_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_Father').removeClass('visually-hidden');
                    }
                    
                    if(fdg == ""){
                        $('#EPI_FDG_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_FDG').removeClass('visually-hidden');
                    }

                    if(r_code == ""){
                        $('#EPI_RCode_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_RCode').removeClass('visually-hidden');
                    }

                    if(vaccine == ""){
                        $('#EPI_Vaccine_ToAdd').css('border','1px solid red');
                        $('.EPI_Error_Vaccine').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END EPI Record Btn ==============


            // ----------------- Add UFC Record Btn -----------------
                $(document).on('click','.Add_UFC_Btn', function(event){

                    let age = document.forms["Add_UFC_Form"]["UFC_Age_ToAdd"].value;
                    let mother_name = document.forms["Add_UFC_Form"]["UFC_Mother_ToAdd"].value;
                    let father_name = document.forms["Add_UFC_Form"]["UFC_Father_ToAdd"].value;
                    let fdg = document.forms["Add_UFC_Form"]["UFC_FDG_ToAdd"].value;
                    let weight = document.forms["Add_UFC_Form"]["UFC_Weight_ToAdd"].value;
                    let r_code = document.forms["Add_UFC_Form"]["UFC_RCode_ToAdd"].value;
                    let remarks = document.forms["Add_UFC_Form"]["UFC_Remarks_ToAdd"].value;


                    if((age/age) && age != "" && (weight/weight) && weight != "" && mother_name != "" && father_name != "" && fdg != "" && r_code != "" && remarks != ""){
                        document.getElementById("Add_UFC_Form").submit();
                    }

                    if(!(age/age)){
                        $('#UFC_Age_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_Age').removeClass('visually-hidden');
                    }

                    if(!(weight/weight)){
                        $('#UFC_Weight_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_Weight').removeClass('visually-hidden');
                    }

                    if(mother_name == ""){
                        $('#UFC_Mother_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_Mother').removeClass('visually-hidden');
                    }
                    
                    if(father_name == ""){
                        $('#UFC_Father_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_Father').removeClass('visually-hidden');
                    }
                    
                    if(fdg == ""){
                        $('#UFC_FDG_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_FDG').removeClass('visually-hidden');
                    }

                    if(r_code == ""){
                        $('#UFC_RCode_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_RCode').removeClass('visually-hidden');
                    }

                    if(remarks == ""){
                        $('#UFC_Remarks_ToAdd').css('border','1px solid red');
                        $('.UFC_Error_Remarks').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END UFC Record Btn ==============


            // ----------------- Add FP Record Btn -----------------
                $(document).on('click','.Add_FP_Btn', function(event){

                    let age = document.forms["Add_FP_Form"]["FP_Age_ToAdd"].value;
                    let num_child = document.forms["Add_FP_Form"]["FP_NumberChild_ToAdd"].value;
                    let client_type = document.forms["Add_FP_Form"]["FP_ClientType_ToAdd"].value;
                    let lmp = document.forms["Add_FP_Form"]["FP_LMP_ToAdd"].value;
                    let method_accepted = document.forms["Add_FP_Form"]["FP_MethodAccepted_ToAdd"].value;
                    let remarks = document.forms["Add_FP_Form"]["FP_Remarks_ToAdd"].value;

                    if((age/age) && age != "" && (num_child/num_child) && num_child != "" && client_type != "" && lmp != "" && method_accepted != "" && remarks != ""){
                        document.getElementById("Add_FP_Form").submit();
                    }

                    if(!(age/age)){
                        $('#FP_Age_ToAdd').css('border','1px solid red');
                        $('.FP_Error_Age').removeClass('visually-hidden');
                    }
                    
                    if(!(num_child/num_child)){
                        $('#FP_NumberChild_ToAdd').css('border','1px solid red');
                        $('.FP_Error_Num_Child').removeClass('visually-hidden');
                    }

                    if(client_type == ""){
                        $('#FP_ClientType_ToAdd').css('border','1px solid red');
                        $('.FP_Error_Client_Type').removeClass('visually-hidden');
                    }
                    
                    if(method_accepted == ""){
                        $('#FP_MethodAccepted_ToAdd').css('border','1px solid red');
                        $('.FP_Error_Method_Accepted').removeClass('visually-hidden');
                    }

                    if(lmp == ""){
                        $('#FP_LMP_ToAdd').css('border','1px solid red');
                        $('.FP_Error_LMP').removeClass('visually-hidden');
                    }
                    
                    if(remarks == ""){
                        $('#FP_Remarks_ToAdd').css('border','1px solid red');
                        $('.FP_Error_Remarks').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END FP Record Btn ==============


            // ----------------- Add CDD Record Btn -----------------
                $(document).on('click','.Add_CDD_Btn', function(event){

                    let age = document.forms["Add_CDD_Form"]["CDD_Age_ToAdd"].value;
                    let complaints = document.forms["Add_CDD_Form"]["CDD_Complaints_ToAdd"].value;
                    let or_number = document.forms["Add_CDD_Form"]["CDD_ORNumber_ToAdd"].value;
                    let remarks = document.forms["Add_CDD_Form"]["CDD_Remarks_ToAdd"].value;

                    if((age/age) && age != "" && complaints != "" && (or_number/or_number) && or_number != "" && remarks != ""){
                        document.getElementById("Add_CDD_Form").submit();
                    }

                    if(!(age/age)){
                        $('#CDD_Age_ToAdd').css('border','1px solid red');
                        $('.CDD_Error_Age').removeClass('visually-hidden');
                    }

                    if(complaints == ""){
                        $('#CDD_Complaints_ToAdd').css('border','1px solid red');
                        $('.CDD_Error_Complaints').removeClass('visually-hidden');
                    }
                    
                    if(!(or_number/or_number)){
                        $('#CDD_ORNumber_ToAdd').css('border','1px solid red');
                        $('.CDD_Error_ORNumber').removeClass('visually-hidden');
                    }
                    
                    if(remarks == ""){
                        $('#CDD_Remarks_ToAdd').css('border','1px solid red');
                        $('.CDD_Error_Remarks').removeClass('visually-hidden');
                    }

                    
                });
            // ============== END CDD Record Btn ==============


            // ----------------- Add Mortality Record Btn -----------------
                $(document).on('click','.Add_Mortality_Btn', function(event){

                    let age = document.forms["Add_Mortality_Form"]["Mortality_Age_ToAdd"].value;
                    let dod = document.forms["Add_Mortality_Form"]["Mortality_DOD_ToAdd"].value;
                    let cod = document.forms["Add_Mortality_Form"]["Mortality_COD_ToAdd"].value;

                    if((age/age) && age != "" && dod != "" && cod != ""){
                        document.getElementById("Add_Mortality_Form").submit();
                    }

                    if(!(age/age)){
                        $('#Mortality_Age_ToAdd').css('border','1px solid red');
                        $('.Mortality_Error_Age').removeClass('visually-hidden');
                    }

                    if(dod == ""){
                        $('#Mortality_DOD_ToAdd').css('border','1px solid red');
                        $('.Mortality_Error_DOD').removeClass('visually-hidden');
                    }
                    
                    if(cod == ""){
                        $('#Mortality_COD_ToAdd').css('border','1px solid red');
                        $('.Mortality_Error_COD').removeClass('visually-hidden');
                    }
                    
                });
            // ============== END Mortality Record Btn ==============


            // ----------------- Add CARI Record Btn -----------------
                $(document).on('click','.Add_CARI_Btn', function(event){

                    let age = document.forms["Add_CARI_Form"]["CARI_Age_ToAdd"].value;
                    let complaints = document.forms["Add_CARI_Form"]["CARI_Complaints_ToAdd"].value;
                    let ho_advice = document.forms["Add_CARI_Form"]["CARI_HOAdvice_ToAdd"].value;

                    if((age/age) && age != "" && complaints != "" && ho_advice != ""){
                        document.getElementById("Add_CARI_Form").submit();
                    }

                    if(!(age/age)){
                        $('#CARI_Age_ToAdd').css('border','1px solid red');
                        $('.CARI_Error_Age').removeClass('visually-hidden');
                    }

                    if(complaints == ""){
                        $('#CARI_Complaints_ToAdd').css('border','1px solid red');
                        $('.CARI_Error_Complaints').removeClass('visually-hidden');
                    }
                    
                    if(ho_advice == ""){
                        $('#CARI_HOAdvice_ToAdd').css('border','1px solid red');
                        $('.CARI_Error_HOAdvice').removeClass('visually-hidden');
                    }

                });
            // ============== END CARI Record Btn ==============


            // ----------------- Add GMS Record Btn -----------------
                $(document).on('click','.Add_GMS_Btn', function(event){

                    let age = document.forms["Add_GMS_Form"]["GMS_Age_ToAdd"].value;
                    let complaints = document.forms["Add_GMS_Form"]["GMS_Complaints_ToAdd"].value;
                    let ho_advice = document.forms["Add_GMS_Form"]["GMS_HOAdvice_ToAdd"].value;

                    if((age/age) && age != "" && complaints != "" && ho_advice != ""){
                        document.getElementById("Add_GMS_Form").submit();
                    }

                    if(!(age/age)){
                        $('#GMS_Age_ToAdd').css('border','1px solid red');
                        $('.GMS_Error_Age').removeClass('visually-hidden');
                    }

                    if(complaints == ""){
                        $('#GMS_Complaints_ToAdd').css('border','1px solid red');
                        $('.GMS_Error_Complaints').removeClass('visually-hidden');
                    }
                    
                    if(ho_advice == ""){
                        $('#GMS_HOAdvice_ToAdd').css('border','1px solid red');
                        $('.GMS_Error_HOAdvice').removeClass('visually-hidden');
                    }
                });
            // ============== END GMS Record Btn ==============


            // ----------------- Add BIP Record Btn -----------------
                $(document).on('click','.Add_BIP_Btn', function(event){

                    let age = document.forms["Add_BIP_Form"]["BIP_Age_ToAdd"].value;
                    let blood_pressure = document.forms["Add_BIP_Form"]["BIP_BloodPressure_ToAdd"].value;
                    let client_type = document.forms["Add_BIP_Form"]["BIP_ClientType_ToAdd"].value;
                    let f_history = document.forms["Add_BIP_Form"]["BIP_FHistory_ToAdd"].value;
                    let remarks = document.forms["Add_BIP_Form"]["BIP_Remarks_ToAdd"].value;

                    if((age/age) && age != "" && blood_pressure != "" && client_type != "" && f_history != "" && remarks != ""){
                        document.getElementById("Add_BIP_Form").submit();
                    }

                    if(!(age/age)){
                        $('#BIP_Age_ToAdd').css('border','1px solid red');
                        $('.BIP_Error_Age').removeClass('visually-hidden');
                    }

                    if(blood_pressure == ""){
                        $('#BIP_BloodPressure_ToAdd').css('border','1px solid red');
                        $('.BIP_Error_BloodPressure').removeClass('visually-hidden');
                    }
                    
                    if(client_type == ""){
                        $('#BIP_ClientType_ToAdd').css('border','1px solid red');
                        $('.BIP_Error_ClientType').removeClass('visually-hidden');
                    }

                    if(f_history == ""){
                        $('#BIP_FHistory_ToAdd').css('border','1px solid red');
                        $('.BIP_Error_FHistory').removeClass('visually-hidden');
                    }
                    
                    if(remarks == ""){
                        $('#BIP_Remarks_ToAdd').css('border','1px solid red');
                        $('.BIP_Error_Remarks').removeClass('visually-hidden');
                    }
                });
            // ============== END BIP Record Btn ==============


            // ----------------- Add TB_Symp Record Btn -----------------
                $(document).on('click','.Add_TB_Symp_Btn', function(event){

                    let age = document.forms["Add_TB_Symp_Form"]["TBSymp_Age_ToAdd"].value;
                    let dox_ray = document.forms["Add_TB_Symp_Form"]["TBSymp_DOXray_ToAdd"].value;
                    let date_first = document.forms["Add_TB_Symp_Form"]["TBSymp_DateFirst_ToAdd"].value;
                    let sputum1 = document.forms["Add_TB_Symp_Form"]["TBSymp_Sputum1_ToAdd"].value;
                    let submit3 = document.forms["Add_TB_Symp_Form"]["TBSymp_Submit3_ToAdd"].value;
                    let date_second = document.forms["Add_TB_Symp_Form"]["TBSymp_DateSecond_ToAdd"].value;
                    let sputum2 = document.forms["Add_TB_Symp_Form"]["TBSymp_Sputum2_ToAdd"].value;
                    let result = document.forms["Add_TB_Symp_Form"]["TBSymp_Result_ToAdd"].value;

                    if((age/age) && age != "" && dox_ray != "" && date_first != "" && sputum1 != "" && submit3 != "" && date_second != "" && sputum2 != "" && result != ""){
                        document.getElementById("Add_TB_Symp_Form").submit();
                    }

                    if(!(age/age)){
                        $('#TBSymp_Age_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_Age').removeClass('visually-hidden');
                    }

                    if(dox_ray == ""){
                        $('#TBSymp_DOXray_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_DOXray').removeClass('visually-hidden');
                    }
                    
                    if(date_first == ""){
                        $('#TBSymp_DateFirst_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_DateFirst').removeClass('visually-hidden');
                    }

                    if(sputum1 == ""){
                        $('#TBSymp_Sputum1_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_Sputum1').removeClass('visually-hidden');
                    }
                    
                    if(submit3 == ""){
                        $('#TBSymp_Submit3_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_Submit3').removeClass('visually-hidden');
                    }

                    if(date_second == ""){
                        $('#TBSymp_DateSecond_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_DateSecond').removeClass('visually-hidden');
                    }

                    if(sputum2 == ""){
                        $('#TBSymp_Sputum2_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_Sputum2').removeClass('visually-hidden');
                    }
                    
                    if(result == ""){
                        $('#TBSymp_Result_ToAdd').css('border','1px solid red');
                        $('.TBSymp_Error_Result').removeClass('visually-hidden');
                    }

                });
            // ============== END TB_Symp Record Btn ==============


            // ----------------- Add Rabies Record Btn -----------------
                $(document).on('click','.Add_Rabies_Btn', function(event){

                    let age = document.forms["Add_Rabies_Form"]["Rabies_Age_ToAdd"].value;
                    let date = document.forms["Add_Rabies_Form"]["Rabies_Date_ToAdd"].value;
                    let complaint_bite = document.forms["Add_Rabies_Form"]["Rabies_ComplaintBite_ToAdd"].value;
                    let remarks = document.forms["Add_Rabies_Form"]["Rabies_Remarks_ToAdd"].value;

                    if((age/age) && age != "" && date != "" && complaint_bite != "" && remarks != ""){
                        document.getElementById("Add_Rabies_Form").submit();
                    }

                    if(!(age/age)){
                        $('#Rabies_Age_ToAdd').css('border','1px solid red');
                        $('.Rabies_Error_Age').removeClass('visually-hidden');
                    }

                    if(date == ""){
                        $('#Rabies_Date_ToAdd').css('border','1px solid red');
                        $('.Rabies_Error_Date').removeClass('visually-hidden');
                    }
                    
                    if(complaint_bite == ""){
                        $('#Rabies_ComplaintBite_ToAdd').css('border','1px solid red');
                        $('.Rabies_Error_ComplaintBite').removeClass('visually-hidden');
                    }
                    
                    if(remarks == ""){
                        $('#Rabies_Remarks_ToAdd').css('border','1px solid red');
                        $('.Rabies_Error_Remarks').removeClass('visually-hidden');
                    }
                });
            // ============== END Rabies Record Btn ==============


            // ----------------- Add Sanitation Record Btn -----------------
                $(document).on('click','.Add_Sanitation_Btn', function(event){

                    let no_toilet = document.forms["Add_Sanitation_Form"]["Sanitation_NoToilet_ToAdd"].value;
                    let not_proper = document.forms["Add_Sanitation_Form"]["Sanitation_NotProper_ToAdd"].value;
                    let poor = document.forms["Add_Sanitation_Form"]["Sanitation_Poor_ToAdd"].value;
                    let without = document.forms["Add_Sanitation_Form"]["Sanitation_Without_ToAdd"].value;
                    let remarks = document.forms["Add_Sanitation_Form"]["Sanitation_Remarks_ToAdd"].value;

                    if(no_toilet != "" && not_proper != "" && poor != "" && without != "" && remarks != ""){
                        document.getElementById("Add_Sanitation_Form").submit();
                    }

                    if(no_toilet == ""){
                        $('#Sanitation_NoToilet_ToAdd').css('border','1px solid red');
                        $('.Sanitation_Error_NoToilet').removeClass('visually-hidden');
                    }

                    if(not_proper == ""){
                        $('#Sanitation_NotProper_ToAdd').css('border','1px solid red');
                        $('.Sanitation_Error_NotProper').removeClass('visually-hidden');
                    }
                    
                    if(poor == ""){
                        $('#Sanitation_Poor_ToAdd').css('border','1px solid red');
                        $('.Sanitation_Error_Poor').removeClass('visually-hidden');
                    }
                    
                    if(without == ""){
                        $('#Sanitation_Without_ToAdd').css('border','1px solid red');
                        $('.Sanitation_Error_Without').removeClass('visually-hidden');
                    }

                    if(remarks == ""){
                        $('#Sanitation_Remarks_ToAdd').css('border','1px solid red');
                        $('.Sanitation_Error_Remarks').removeClass('visually-hidden');
                    }
                });
            // ============== END Sanitation Record Btn ==============
            

            // ----------------- View Single Record Btn -----------------
                $(document).on('click','.View_Single_Record_Btn', function(event){
                    let split_value = this.value.split("-");
                    let record_id = split_value[0];
                    let record_type = split_value[1];

                    if(record_type == "Walk In"){
                        var records = {!! json_encode($WalkIn_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("WalkIn_Record_ID").value = record["id"];
                                document.getElementById("WalkIn_blood_pressure").value = record["blood_pressure"];
                                document.getElementById("WalkIn_blood_sugar").value = record["blood_sugar"];
                                document.getElementById("WalkIn_consultation").value = record["consultation"];
                                document.getElementById("WalkIn_findings").value = record["findings"];
                                document.getElementById("WalkIn_notes").value = record["notes"];
                                document.getElementById("WalkIn_medicine").value = record["medicine"];
                                document.getElementById("WalkIn_quantity").value = record["medicine_quantity"];
                            }
                        });
                        $("#WalkInRecordModal").modal('show');
                    }else if(record_type == "MCH"){
                        var records = {!! json_encode($MCH_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("MCH_Record_ID").value = record["id"];
                                document.getElementById("MCH_age").value = record["age"];
                                document.getElementById("MCH_rcode").value = record["rcode"];
                                document.getElementById("MCH_g").value = record["g"];
                                document.getElementById("MCH_p").value = record["p"];
                                document.getElementById("MCH_level").value = record["level"];
                                document.getElementById("MCH_range").value = record["range"];
                                document.getElementById("MCH_lmp").value = record["lmp"];
                                document.getElementById("MCH_edc").value = record["edc"];
                                document.getElementById("MCH_remarks").value = record["remarks"];
                            }
                        });
                        $("#MCHRecordModal").modal('show');
                    }else if(record_type == "PP"){
                        var records = {!! json_encode($PP_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("PP_Record_ID").value = record["id"];
                                document.getElementById("PP_age").value = record["age"];
                                document.getElementById("PP_place_delivery").value = record["PlaceOfDelivery"];
                                document.getElementById("PP_attended_by").value = record["attended_by"];
                                document.getElementById("PP_gender").value = record["gender"];
                                document.getElementById("PP_fdg").value = record["fdg"];
                                document.getElementById("PP_weight").value = record["weight"];
                                document.getElementById("PP_date_pp").value = record["date_of_pp"];
                                document.getElementById("PP_vitamina").value = record["vitamina"];
                                document.getElementById("PP_dod").value = record["dod"];
                                document.getElementById("PP_f").value = record["F"];
                            }
                        });
                        $("#PPRecordModal").modal('show');
                    }else if(record_type == "EPI"){
                        var records = {!! json_encode($EPI_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("EPI_Record_ID").value = record["id"];
                                document.getElementById("EPI_age").value = record["age"];
                                document.getElementById("EPI_mother_name").value = record["mother_name"];
                                document.getElementById("EPI_father_name").value = record["father_name"];
                                document.getElementById("EPI_fdg").value = record["fdg"];
                                document.getElementById("EPI_weight").value = record["weight"];
                                document.getElementById("EPI_r_code").value = record["r_code"];
                                document.getElementById("EPI_vaccine").value = record["vaccine"];
                            }
                        });
                        $("#EPIRecordModal").modal('show');
                    }else if(record_type == "UFC"){
                        var records = {!! json_encode($UFC_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("UFC_Record_ID").value = record["id"];
                                document.getElementById("UFC_age").value = record["age"];
                                document.getElementById("UFC_mother_name").value = record["mother_name"];
                                document.getElementById("UFC_father_name").value = record["father_name"];
                                document.getElementById("UFC_fdg").value = record["fdg"];
                                document.getElementById("UFC_weight").value = record["weight"];
                                document.getElementById("UFC_r_code").value = record["r_code"];
                                document.getElementById("UFC_remarks").value = record["remarks"];
                            }
                        });
                        $("#UFCRecordModal").modal('show');
                    }else if(record_type == "FP"){
                        var records = {!! json_encode($FP_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("FP_Record_ID").value = record["id"];
                                document.getElementById("FP_age").value = record["age"];
                                document.getElementById("FP_num_child").value = record["num_child"];
                                document.getElementById("FP_lmp").value = record["lmp"];
                                document.getElementById("FP_client_type").value = record["client_type"];
                                document.getElementById("FP_method_accepted").value = record["method_accepted"];
                                document.getElementById("FP_remarks").value = record["remarks"];
                            }
                        });
                        $("#FPRecordModal").modal('show');
                    }else if(record_type == "CDD"){
                        var records = {!! json_encode($CDD_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("CDD_Record_ID").value = record["id"];
                                document.getElementById("CDD_age").value = record["age"];
                                document.getElementById("CDD_complaints").value = record["complaints"];
                                document.getElementById("CDD_or_number").value = record["num_OR"];
                                document.getElementById("CDD_remarks").value = record["remarks"];
                            }
                        });
                        $("#CDDRecordModal").modal('show');
                    }else if(record_type == "Mortality"){
                        var records = {!! json_encode($Mortality_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("Mortality_Record_ID").value = record["id"];
                                document.getElementById("Mortality_age").value = record["age"];
                                document.getElementById("Mortality_dod").value = record["DOD"];
                                document.getElementById("Mortality_cod").value = record["COD"];
                            }
                        });
                        $("#MortalityRecordModal").modal('show');
                    }else if(record_type == "CARI"){
                        var records = {!! json_encode($CARI_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("Cari_Record_ID").value = record["id"];
                                document.getElementById("Cari_age").value = record["age"];
                                document.getElementById("Cari_complaints").value = record["complaints"];
                                document.getElementById("Cari_ho_advice").value = record["HO_advice"];
                            }
                        });
                        $("#CariRecordModal").modal('show');
                    }else if(record_type == "GMS"){
                        var records = {!! json_encode($GMS_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("GMS_Record_ID").value = record["id"];
                                document.getElementById("GMS_age").value = record["age"];
                                document.getElementById("GMS_complaints").value = record["complaints"];
                                document.getElementById("GMS_ho_advice").value = record["HO_advice"];
                            }
                        });
                        $("#GMSRecordModal").modal('show');
                    }else if(record_type == "BIP"){
                        var records = {!! json_encode($BIP_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("BIP_Record_ID").value = record["id"];
                                document.getElementById("BIP_age").value = record["age"];
                                document.getElementById("BIP_blood_pressure").value = record["BP"];
                                document.getElementById("BIP_client_type").value = record["client_type"];
                                document.getElementById("BIP_f_history").value = record["f_history"];
                                document.getElementById("BIP_remarks").value = record["remarks"];
                            }
                        });
                        $("#BIPRecordModal").modal('show');
                    }else if(record_type == "TB Symp"){
                        var records = {!! json_encode($TB_Symp_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("TBSymp_Record_ID").value = record["id"];
                                document.getElementById("TBSymp_age").value = record["age"];
                                document.getElementById("TBSymp_dox_ray").value = record["DOX_ray"];
                                document.getElementById("TBSymp_date_first").value = record["date_first"];
                                document.getElementById("TBSymp_sputum1").value = record["sputum"];
                                document.getElementById("TBSymp_submit3").value = record["submit3"];
                                document.getElementById("TBSymp_date_second").value = record["date_first2"];
                                document.getElementById("TBSymp_sputum2").value = record["sputum2"];
                                document.getElementById("TBSymp_result").value = record["result3"];
                            }
                        });
                        $("#TBSympRecordModal").modal('show');
                    }else if(record_type == "Rabies"){
                        var records = {!! json_encode($Rabies_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("Rabies_Record_ID").value = record["id"];
                                document.getElementById("Rabies_age").value = record["age"];
                                document.getElementById("Rabies_date").value = record["date"];
                                document.getElementById("Rabies_complaint_bite").value = record["complaint_bite"];
                                document.getElementById("Rabies_remarks").value = record["remarks"];
                            }
                        });
                        $("#RabiesRecordModal").modal('show');
                    }else if(record_type == "Sanitation"){
                        var records = {!! json_encode($Sanitation_Record, JSON_HEX_TAG) !!};
                        records.forEach(record => {
                            if(record["id"] == parseInt(record_id)){
                                document.getElementById("Sanitation_Record_ID").value = record["id"];
                                document.getElementById("Sanitation_no_toilet").value = record["no_toilet"];
                                document.getElementById("Sanitation_not_proper").value = record["not_proper"];
                                document.getElementById("Sanitation_poor").value = record["poor"];
                                document.getElementById("Sanitation_without").value = record["without"];
                                document.getElementById("Sanitation_remarks").value = record["remarks"];
                            }
                        });
                        $("#SanitationRecordModal").modal('show');
                    }
                });
            // ============== END View Single Record Btn ==============

            // ----------------- Edit WalkIn Record Btn -----------------
                $(document).on('click','.Edit_WalkIn_Record', function(event){
                    $('.Edit_WalkIn_Record').addClass('visually-hidden');
                    $('.WalkIn_Save_Btn').removeClass('visually-hidden');
                    $('.WalkIn_Cancel_Btn').removeClass('visually-hidden');

                    $('#WalkIn_blood_pressure').removeAttr( "disabled" );
                    $('#WalkIn_blood_sugar').removeAttr( "disabled" );
                    $('#WalkIn_consultation').removeAttr( "disabled" );
                    $('#WalkIn_findings').removeAttr( "disabled" );
                    $('#WalkIn_notes').removeAttr( "disabled" );
                    $('#WalkIn_medicine').removeAttr( "disabled" );
                    $('#WalkIn_quantity').removeAttr( "disabled" );
                });
            // ============== END Edit WalkIn Record Btn ==============
            

            // ----------------- Close WalkIn Record Btn -----------------
                $(document).on('click','.WalkIn_Cancel_Btn', function(event){
                    $("#WalkInRecordModal").modal('hide');
                    $('.Edit_WalkIn_Record').removeClass('visually-hidden');
                    $('.WalkIn_Save_Btn').addClass('visually-hidden');
                    $('.WalkIn_Cancel_Btn').addClass('visually-hidden');

                    $('#WalkIn_blood_pressure').attr( "disabled", true );
                    $('#WalkIn_blood_sugar').attr( "disabled", true );
                    $('#WalkIn_consultation').attr( "disabled", true );
                    $('#WalkIn_findings').attr( "disabled", true );
                    $('#WalkIn_notes').attr( "disabled", true );
                    $('#WalkIn_medicine').attr( "disabled", true );
                    $('#WalkIn_quantity').attr( "disabled", true );
                });
            // ============== END Close WalkIn Record Btn ==============


            // ----------------- Edit MCH Record Btn -----------------
                $(document).on('click','.Edit_MCH_Record', function(event){
                    $('.Edit_MCH_Record').addClass('visually-hidden');
                    $('.MCH_Save_Btn').removeClass('visually-hidden');
                    $('.MCH_Cancel_Btn').removeClass('visually-hidden');

                    $('#MCH_age').removeAttr( "disabled" );
                    $('#MCH_g').removeAttr( "disabled" );
                    $('#MCH_p').removeAttr( "disabled" );
                    $('#MCH_rcode').removeAttr( "disabled" );
                    $('#MCH_level').removeAttr( "disabled" );
                    $('#MCH_range').removeAttr( "disabled" );
                    $('#MCH_lmp').removeAttr( "disabled" );
                    $('#MCH_edc').removeAttr( "disabled" );
                    $('#MCH_remarks').removeAttr( "disabled" );
                });
            // ============== END Edit MCH Record Btn ==============
            

            // ----------------- Close MCH Record Btn -----------------
                $(document).on('click','.MCH_Cancel_Btn', function(event){
                    $("#MCHRecordModal").modal('hide');
                    $('.Edit_MCH_Record').removeClass('visually-hidden');
                    $('.MCH_Save_Btn').addClass('visually-hidden');
                    $('.MCH_Cancel_Btn').addClass('visually-hidden');

                    $('#MCH_age').attr( "disabled", true );
                    $('#MCH_g').attr( "disabled", true );
                    $('#MCH_p').attr( "disabled", true );
                    $('#MCH_rcode').attr( "disabled", true );
                    $('#MCH_level').attr( "disabled", true );
                    $('#MCH_range').attr( "disabled", true );
                    $('#MCH_lmp').attr( "disabled", true );
                    $('#MCH_edc').attr( "disabled", true );
                    $('#MCH_remarks').attr( "disabled", true );
                });
            // ============== END Close MCH Record Btn ==============


            // ----------------- Edit PP Record Btn -----------------
                $(document).on('click','.Edit_PP_Record', function(event){
                    $('.Edit_PP_Record').addClass('visually-hidden');
                    $('.PP_Save_Btn').removeClass('visually-hidden');
                    $('.PP_Cancel_Btn').removeClass('visually-hidden');

                    $("#PP_age").removeAttr("disabled");
                    $("#PP_place_delivery").removeAttr("disabled");
                    $("#PP_attended_by").removeAttr("disabled");
                    $("#PP_gender").removeAttr("disabled");
                    $("#PP_fdg").removeAttr("disabled");
                    $("#PP_weight").removeAttr("disabled");
                    $("#PP_date_pp").removeAttr("disabled");
                    $("#PP_vitamina").removeAttr("disabled");
                    $("#PP_dod").removeAttr("disabled");
                    $("#PP_f").removeAttr("disabled");
                });
            // ============== END Edit PP Record Btn ==============
            

            // ----------------- Close PP Record Btn -----------------
                $(document).on('click','.PP_Cancel_Btn', function(event){
                    $("#PPRecordModal").modal('hide');
                    $('.Edit_PP_Record').removeClass('visually-hidden');
                    $('.PP_Save_Btn').addClass('visually-hidden');
                    $('.PP_Cancel_Btn').addClass('visually-hidden');

                    $("#PP_age").attr( "disabled", true );
                    $("#PP_place_delivery").attr( "disabled", true );
                    $("#PP_attended_by").attr( "disabled", true );
                    $("#PP_gender").attr( "disabled", true );
                    $("#PP_fdg").attr( "disabled", true );
                    $("#PP_weight").attr( "disabled", true );
                    $("#PP_date_pp").attr( "disabled", true );
                    $("#PP_vitamina").attr( "disabled", true );
                    $("#PP_dod").attr( "disabled", true );
                    $("#PP_f").attr( "disabled", true );
                });
            // ============== END Close PP Record Btn ==============


            // ----------------- Edit EPI Record Btn -----------------
                $(document).on('click','.Edit_EPI_Record', function(event){
                    $('.Edit_EPI_Record').addClass('visually-hidden');
                    $('.EPI_Save_Btn').removeClass('visually-hidden');
                    $('.EPI_Cancel_Btn').removeClass('visually-hidden');

                    $("#EPI_age").removeAttr("disabled");
                    $("#EPI_mother_name").removeAttr("disabled");
                    $("#EPI_father_name").removeAttr("disabled");
                    $("#EPI_fdg").removeAttr("disabled");
                    $("#EPI_weight").removeAttr("disabled");
                    $("#EPI_r_code").removeAttr("disabled");
                    $("#EPI_vaccine").removeAttr("disabled");
                });
            // ============== END Edit EPI Record Btn ==============
            

            // ----------------- Close EPI Record Btn -----------------
                $(document).on('click','.EPI_Cancel_Btn', function(event){
                    $("#EPIRecordModal").modal('hide');
                    $('.Edit_EPI_Record').removeClass('visually-hidden');
                    $('.EPI_Save_Btn').addClass('visually-hidden');
                    $('.EPI_Cancel_Btn').addClass('visually-hidden');

                    $("#EPI_age").attr( "disabled", true );
                    $("#EPI_mother_name").attr( "disabled", true );
                    $("#EPI_father_name").attr( "disabled", true );
                    $("#EPI_fdg").attr( "disabled", true );
                    $("#EPI_weight").attr( "disabled", true );
                    $("#EPI_r_code").attr( "disabled", true );
                    $("#EPI_vaccine").attr( "disabled", true );
                });
            // ============== END Close EPI Record Btn ==============


            // ----------------- Edit UFC Record Btn -----------------
                $(document).on('click','.Edit_UFC_Record', function(event){
                    $('.Edit_UFC_Record').addClass('visually-hidden');
                    $('.UFC_Save_Btn').removeClass('visually-hidden');
                    $('.UFC_Cancel_Btn').removeClass('visually-hidden');

                    $("#UFC_age").removeAttr("disabled");
                    $("#UFC_mother_name").removeAttr("disabled");
                    $("#UFC_father_name").removeAttr("disabled");
                    $("#UFC_fdg").removeAttr("disabled");
                    $("#UFC_weight").removeAttr("disabled");
                    $("#UFC_r_code").removeAttr("disabled");
                    $("#UFC_remarks").removeAttr("disabled");
                });
            // ============== END Edit UFC Record Btn ==============
            

            // ----------------- Close UFC Record Btn -----------------
                $(document).on('click','.UFC_Cancel_Btn', function(event){
                    $("#UFCRecordModal").modal('hide');
                    $('.Edit_UFC_Record').removeClass('visually-hidden');
                    $('.UFC_Save_Btn').addClass('visually-hidden');
                    $('.UFC_Cancel_Btn').addClass('visually-hidden');

                    $("#UFC_age").attr( "disabled", true );
                    $("#UFC_mother_name").attr( "disabled", true );
                    $("#UFC_father_name").attr( "disabled", true );
                    $("#UFC_fdg").attr( "disabled", true );
                    $("#UFC_weight").attr( "disabled", true );
                    $("#UFC_r_code").attr( "disabled", true );
                    $("#UFC_remarks").attr( "disabled", true );
                });
            // ============== END Close UFC Record Btn ==============


            // ----------------- Edit FP Record Btn -----------------
                $(document).on('click','.Edit_FP_Record', function(event){
                    $('.Edit_FP_Record').addClass('visually-hidden');
                    $('.FP_Save_Btn').removeClass('visually-hidden');
                    $('.FP_Cancel_Btn').removeClass('visually-hidden');

                    $("#FP_age").removeAttr("disabled");
                    $("#FP_num_child").removeAttr("disabled");
                    $("#FP_lmp").removeAttr("disabled");
                    $("#FP_client_type").removeAttr("disabled");
                    $("#FP_method_accepted").removeAttr("disabled");
                    $("#FP_remarks").removeAttr("disabled");
                });
            // ============== END Edit FP Record Btn ==============
            

            // ----------------- Close FP Record Btn -----------------
                $(document).on('click','.FP_Cancel_Btn', function(event){
                    $("#FPRecordModal").modal('hide');
                    $('.Edit_FP_Record').removeClass('visually-hidden');
                    $('.FP_Save_Btn').addClass('visually-hidden');
                    $('.FP_Cancel_Btn').addClass('visually-hidden');

                    $("#FP_age").attr( "disabled", true );
                    $("#FP_num_child").attr( "disabled", true );
                    $("#FP_lmp").attr( "disabled", true );
                    $("#FP_client_type").attr( "disabled", true );
                    $("#FP_method_accepted").attr( "disabled", true );
                    $("#FP_remarks").attr( "disabled", true );
                });
            // ============== END Close FP Record Btn ==============


            // ----------------- Edit CDD Record Btn -----------------
                $(document).on('click','.Edit_CDD_Record', function(event){
                    $('.Edit_CDD_Record').addClass('visually-hidden');
                    $('.CDD_Save_Btn').removeClass('visually-hidden');
                    $('.CDD_Cancel_Btn').removeClass('visually-hidden');

                    $("#CDD_age").removeAttr("disabled");
                    $("#CDD_complaints").removeAttr("disabled");
                    $("#CDD_or_number").removeAttr("disabled");
                    $("#CDD_remarks").removeAttr("disabled");
                });
            // ============== END Edit CDD Record Btn ==============
            

            // ----------------- Close CDD Record Btn -----------------
                $(document).on('click','.CDD_Cancel_Btn', function(event){
                    $("#CDDRecordModal").modal('hide');
                    $('.Edit_CDD_Record').removeClass('visually-hidden');
                    $('.CDD_Save_Btn').addClass('visually-hidden');
                    $('.CDD_Cancel_Btn').addClass('visually-hidden');

                    $("#CDD_age").attr( "disabled", true );
                    $("#CDD_complaints").attr( "disabled", true );
                    $("#CDD_or_number").attr( "disabled", true );
                    $("#CDD_remarks").attr( "disabled", true );
                });
            // ============== END Close CDD Record Btn ==============


            // ----------------- Edit Mortality Record Btn -----------------
                $(document).on('click','.Edit_Mortality_Record', function(event){
                    $('.Edit_Mortality_Record').addClass('visually-hidden');
                    $('.Mortality_Save_Btn').removeClass('visually-hidden');
                    $('.Mortality_Cancel_Btn').removeClass('visually-hidden');

                    $("#Mortality_age").removeAttr("disabled");
                    $("#Mortality_dod").removeAttr("disabled");
                    $("#Mortality_cod").removeAttr("disabled");
                });
            // ============== END Edit Mortality Record Btn ==============
            

            // ----------------- Close Mortality Record Btn -----------------
                $(document).on('click','.Mortality_Cancel_Btn', function(event){
                    $("#MortalityRecordModal").modal('hide');
                    $('.Edit_Mortality_Record').removeClass('visually-hidden');
                    $('.Mortality_Save_Btn').addClass('visually-hidden');
                    $('.Mortality_Cancel_Btn').addClass('visually-hidden');

                    $("#Mortality_age").attr( "disabled", true );
                    $("#Mortality_dod").attr( "disabled", true );
                    $("#Mortality_cod").attr( "disabled", true );
                });
            // ============== END Close Mortality Record Btn ==============


            // ----------------- Edit CARI Record Btn -----------------
            $(document).on('click','.Edit_Cari_Record', function(event){
                    $('.Edit_Cari_Record').addClass('visually-hidden');
                    $('.Cari_Save_Btn').removeClass('visually-hidden');
                    $('.Cari_Cancel_Btn').removeClass('visually-hidden');

                    $("#Cari_age").removeAttr("disabled");
                    $("#Cari_complaints").removeAttr("disabled");
                    $("#Cari_ho_advice").removeAttr("disabled");
                });
            // ============== END Edit CARI Record Btn ==============
            

            // ----------------- Close CARI Record Btn -----------------
                $(document).on('click','.Cari_Cancel_Btn', function(event){
                    $("#CariRecordModal").modal('hide');
                    $('.Edit_Cari_Record').removeClass('visually-hidden');
                    $('.Cari_Save_Btn').addClass('visually-hidden');
                    $('.Cari_Cancel_Btn').addClass('visually-hidden');

                    $("#Cari_age").attr( "disabled", true );
                    $("#Cari_complaints").attr( "disabled", true );
                    $("#Cari_ho_advice").attr( "disabled", true );
                });
            // ============== END Close CARI Record Btn ==============


            // ----------------- Edit GMS Record Btn -----------------
            $(document).on('click','.Edit_GMS_Record', function(event){
                    $('.Edit_GMS_Record').addClass('visually-hidden');
                    $('.GMS_Save_Btn').removeClass('visually-hidden');
                    $('.GMS_Cancel_Btn').removeClass('visually-hidden');

                    $("#GMS_age").removeAttr("disabled");
                    $("#GMS_complaints").removeAttr("disabled");
                    $("#GMS_ho_advice").removeAttr("disabled");
                });
            // ============== END Edit GMS Record Btn ==============
            

            // ----------------- Close GMS Record Btn -----------------
                $(document).on('click','.GMS_Cancel_Btn', function(event){
                    $("#GMSRecordModal").modal('hide');
                    $('.Edit_GMS_Record').removeClass('visually-hidden');
                    $('.GMS_Save_Btn').addClass('visually-hidden');
                    $('.GMS_Cancel_Btn').addClass('visually-hidden');

                    $("#GMS_age").attr( "disabled", true );
                    $("#GMS_complaints").attr( "disabled", true );
                    $("#GMS_ho_advice").attr( "disabled", true );
                });
            // ============== END Close GMS Record Btn ==============


            // ----------------- Edit BIP Record Btn -----------------
            $(document).on('click','.Edit_BIP_Record', function(event){
                    $('.Edit_BIP_Record').addClass('visually-hidden');
                    $('.BIP_Save_Btn').removeClass('visually-hidden');
                    $('.BIP_Cancel_Btn').removeClass('visually-hidden');

                    $("#BIP_age").removeAttr("disabled");
                    $("#BIP_blood_pressure").removeAttr("disabled");
                    $("#BIP_client_type").removeAttr("disabled");
                    $("#BIP_f_history").removeAttr("disabled");
                    $("#BIP_remarks").removeAttr("disabled");
                });
            // ============== END Edit BIP Record Btn ==============
            

            // ----------------- Close BIP Record Btn -----------------
                $(document).on('click','.BIP_Cancel_Btn', function(event){
                    $("#BIPRecordModal").modal('hide');
                    $('.Edit_BIP_Record').removeClass('visually-hidden');
                    $('.BIP_Save_Btn').addClass('visually-hidden');
                    $('.BIP_Cancel_Btn').addClass('visually-hidden');

                    $("#BIP_age").attr( "disabled", true );
                    $("#BIP_blood_pressure").attr( "disabled", true );
                    $("#BIP_client_type").attr( "disabled", true );
                    $("#BIP_f_history").attr( "disabled", true );
                    $("#BIP_remarks").attr( "disabled", true );
                });
            // ============== END Close BIP Record Btn ==============


            // ----------------- Edit TB symp Record Btn -----------------
            $(document).on('click','.Edit_TBSymp_Record', function(event){
                    $('.Edit_TBSymp_Record').addClass('visually-hidden');
                    $('.TBSymp_Save_Btn').removeClass('visually-hidden');
                    $('.TBSymp_Cancel_Btn').removeClass('visually-hidden');

                    $("#TBSymp_age").removeAttr("disabled");
                    $("#TBSymp_dox_ray").removeAttr("disabled");
                    $("#TBSymp_date_first").removeAttr("disabled");
                    $("#TBSymp_sputum1").removeAttr("disabled");
                    $("#TBSymp_submit3").removeAttr("disabled");
                    $("#TBSymp_date_second").removeAttr("disabled");
                    $("#TBSymp_sputum2").removeAttr("disabled");
                    $("#TBSymp_result").removeAttr("disabled");
                });
            // ============== END Edit TB symp Record Btn ==============
            

            // ----------------- Close TB symp Record Btn -----------------
                $(document).on('click','.TBSymp_Cancel_Btn', function(event){
                    $("#TBSympRecordModal").modal('hide');
                    $('.Edit_TBSymp_Record').removeClass('visually-hidden');
                    $('.TBSymp_Save_Btn').addClass('visually-hidden');
                    $('.TBSymp_Cancel_Btn').addClass('visually-hidden');

                    $("#TBSymp_age").attr( "disabled", true );
                    $("#TBSymp_dox_ray").attr( "disabled", true );
                    $("#TBSymp_date_first").attr( "disabled", true );
                    $("#TBSymp_sputum1").attr( "disabled", true );
                    $("#TBSymp_submit3").attr( "disabled", true );
                    $("#TBSymp_date_second").attr( "disabled", true );
                    $("#TBSymp_sputum2").attr( "disabled", true );
                    $("#TBSymp_result").attr( "disabled", true );
                });
            // ============== END Close TB symp Record Btn ==============


            // ----------------- Edit Rabies Record Btn -----------------
            $(document).on('click','.Edit_Rabies_Record', function(event){
                    $('.Edit_Rabies_Record').addClass('visually-hidden');
                    $('.Rabies_Save_Btn').removeClass('visually-hidden');
                    $('.Rabies_Cancel_Btn').removeClass('visually-hidden');

                    $("#Rabies_age").removeAttr("disabled");
                    $("#Rabies_date").removeAttr("disabled");
                    $("#Rabies_complaint_bite").removeAttr("disabled");
                    $("#Rabies_remarks").removeAttr("disabled");
                });
            // ============== END Edit Rabies Record Btn ==============
            

            // ----------------- Close Rabies Record Btn -----------------
                $(document).on('click','.Rabies_Cancel_Btn', function(event){
                    $("#RabiesRecordModal").modal('hide');
                    $('.Edit_Rabies_Record').removeClass('visually-hidden');
                    $('.Rabies_Save_Btn').addClass('visually-hidden');
                    $('.Rabies_Cancel_Btn').addClass('visually-hidden');

                    $("#Rabies_age").attr( "disabled", true );
                    $("#Rabies_date").attr( "disabled", true );
                    $("#Rabies_complaint_bite").attr( "disabled", true );
                    $("#Rabies_remarks").attr( "disabled", true );
                });
            // ============== END Close Rabies Record Btn ==============


            // ----------------- Edit Sanitation Record Btn -----------------
            $(document).on('click','.Edit_Sanitation_Record', function(event){
                    $('.Edit_Sanitation_Record').addClass('visually-hidden');
                    $('.Sanitation_Save_Btn').removeClass('visually-hidden');
                    $('.Sanitation_Cancel_Btn').removeClass('visually-hidden');

                    $("#Sanitation_no_toilet").removeAttr("disabled");
                    $("#Sanitation_not_proper").removeAttr("disabled");
                    $("#Sanitation_poor").removeAttr("disabled");
                    $("#Sanitation_without").removeAttr("disabled");
                    $("#Sanitation_remarks").removeAttr("disabled");
                });
            // ============== END Edit Sanitation Record Btn ==============
            

            // ----------------- Close Sanitation Record Btn -----------------
                $(document).on('click','.Sanitation_Cancel_Btn', function(event){
                    $("#SanitationRecordModal").modal('hide');
                    $('.Edit_Sanitation_Record').removeClass('visually-hidden');
                    $('.Sanitation_Save_Btn').addClass('visually-hidden');
                    $('.Sanitation_Cancel_Btn').addClass('visually-hidden');

                    $("#Sanitation_no_toilet").attr( "disabled", true );
                    $("#Sanitation_not_proper").attr( "disabled", true );
                    $("#Sanitation_poor").attr( "disabled", true );
                    $("#Sanitation_without").attr( "disabled", true );
                    $("#Sanitation_remarks").attr( "disabled", true );
                });
            // ============== END Close Sanitation Record Btn ==============


            // ------------------------ View Household Info ------------------------
            $(".View_Household_Btn").click(function(){
                document.getElementById("selectedHouseholdID").value = this.value;
                document.getElementById("Household_ID_Storage").submit();
            });
            // =========================== END ==============================

            });
        </script>
        {{-- END Jquery for adding selected row data to Modal --}}
    
@endsection