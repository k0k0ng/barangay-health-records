@extends('layouts.app')

@section('content')

<div class="h-65p">
    <div class="w-100 pt-2 pb-3 font-semibold text-sm text-gray-600">
        Barangay Information
    </div>
    <div class="barangay-info-content rounded">

        <div class="rounded p-4 border">
            <div class="population-div py-1 ps-3">
                <p class="text-gray-800 text-2xl m-0">Listed barangay workers in this system</p>
            </div>
            <hr>
            <p class="text-xs text-gray-600 text-center mb-4">BHRS Record</p>
            @if (session('RecordingSucess'))
                <div class="alert alert-success d-flex justify-content-between" role="alert">
                    <p class="text-sm mb-0">{{ session('RecordingSucess') }}</p>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div> 
            @endif
            @if (session('RecordingFailed'))
                <div class="alert alert-danger d-flex justify-content-between" role="alert">
                    <p class="text-sm mb-0">{{ session('RecordingFailed') }}</p>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div> 
            @endif
            <form class="mb-3" autocomplete="off">
                @csrf
                <div class="d-flex">
                    <input type="search" name="searchedToAdd" id="searchedToAdd" class="form-control" placeholder="Add Barangay Worker" aria-label="Add Barangay Worker" aria-describedby="add-worker-btn" list="residenceList">
                </div>
            </form>
            <div class="position-absolute w-55p">
                <table class="w-100 table table-bordered table-hover table-responsive">
                    <tbody class="bg-white" id="suggested_on_search">

                    </tbody>
                </table>
            </div>
            <div class="mb-2">
                <table id="persons_table" class="w-100 table table-bordered table-hover table-responsive">
                    <thead class="bg-gray-800 text-white">
                    <tr>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Role
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Name
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($barangay_workers as $barangay_worker)
                        @php
                            $i++;
                        @endphp
                        <tr @php echo "id='".$barangay_worker->id."'"; @endphp>
                            <td class="px-3 align-middle whitespace-nowrap">{{ $barangay_worker->type }}</td>
                            <td class="px-3 align-middle">{{ $barangay_worker->firstname.' '.$barangay_worker->lastname }}</td>
                            <td class="d-flex justify-content-center text-sm font-medium">
                                <button type="button" @php echo "value='".$barangay_worker->id.'-'.$barangay_worker->firstname.'-'.$barangay_worker->lastname.'-'.$barangay_worker->type.'-'.$barangay_worker->contactNumber."'"; @endphp class="btn btn-outline-primary btn-sm Edit_Worker_Btn w-16" data-bs-toggle="modal" data-bs-target="#EditWorkerModal">
                                    Edit
                                </button>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="mt-3 d-flex justify-content-evenly">
                    {!! $barangay_workers->appends(\Request::except('page'))->render() !!}
                </div>
                @if ($barangay_workers->isEmpty())
                    <div class="m-5 text-center text-gray-600 font-bold">
                        No data available.
                    </div>
                @endif
            </div>
        </div>

        <div class="rounded p-4 border">
            <div class="household-div p-1 ps-4">
                <h2 class="text-gray-800 text-2xl m-0">Barangay {{ $barangay_info->brgy_name }} Officials</h2>
            </div>
            <hr>
            <p class="text-xs text-gray-600 text-center mb-4">BRMS Record</p>
            <div>
                @if ($barangay_officials[0] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Barangay Captain:</span> {{ $barangay_officials[0]->last_name }}, {{ $barangay_officials[0]->first_name }} {{ $barangay_officials[0]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Barangay Captain:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[1] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[1]->last_name }}, {{ $barangay_officials[1]->first_name }} {{ $barangay_officials[1]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[2] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[2]->last_name }}, {{ $barangay_officials[2]->first_name }} {{ $barangay_officials[2]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[3] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[3]->last_name }}, {{ $barangay_officials[3]->first_name }} {{ $barangay_officials[3]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[4] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[4]->last_name }}, {{ $barangay_officials[4]->first_name }} {{ $barangay_officials[4]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[5] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[5]->last_name }}, {{ $barangay_officials[5]->first_name }} {{ $barangay_officials[5]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[6] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[6]->last_name }}, {{ $barangay_officials[6]->first_name }} {{ $barangay_officials[6]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[7] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> {{ $barangay_officials[7]->last_name }}, {{ $barangay_officials[7]->first_name }} {{ $barangay_officials[7]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Kagawad:</span> *Vacant*</p>
                @endif
                @if ($barangay_officials[0] != "")
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Pangkat Secretary:</span> {{ $barangay_officials[8]->last_name }}, {{ $barangay_officials[8]->first_name }} {{ $barangay_officials[8]->middle_name[0] }}.</p>
                @else
                    <p class="m-0 my-2 text-lg"> <span class="text-gray-600 text-sm pe-1">Pangkat Secretary:</span> *Vacant*</p>
                @endif
            </div>
        </div>

    </div>
</div>

    <!-- --------------------- Add Worker Modal ------------------------- -->
    <div class="modal fade" id="AddWorker" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Add New Worker</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('addNewWorker') }}" method="GET" autocomplete="off">
                    @csrf

                    <div class="col mb-3">
                        <label for="name" class="form-label ms-1">Name</label>
                        <input type="text" id="name" name="name" class="form-control" aria-label="name" readonly>
                    </div>

                    <div class="col mb-3">
                        <label for="position" class="form-label ms-1">Position</label>
                        <input type="text" id="position" name="position" class="form-control" aria-label="position" required>
                    </div>
                    <div class="col">
                        <label for="contactNumber" class="form-label ms-1">Contact Number</label>
                        <input type="text" id="contactNumber" name="contactNumber" class="form-control" aria-label="contactNumber" required>
                    </div>
    
                    {{-- Hidden tag for data storing only --}}
                    <input id="ResidentID" name="ResidentID" class="visually-hidden">
                    <input id="BarangayID" name="BarangayID" class="visually-hidden">
                    <input id="FirstName" name="FirstName" class="visually-hidden">
                    <input id="LastName" name="LastName" class="visually-hidden">

                    <div class="modal-footer mt-5">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary text-white w-24">Save</button>
                    </div>
                </form>
            </div>
            
        </div>
        </div>
    </div>
    <!-- ======================== END Add Worker Modal ======================== -->


    <!-- --------------------- Edit Worker Modal ------------------------- -->
    <div class="modal fade" id="EditWorkerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header bg-gray-800 text-white">
            <h5 class="modal-title" id="staticBackdropLabel">Edit Worker</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('editWorker') }}" method="POST" autocomplete="off">
                    @csrf

                    <div class="col mb-3">
                        <label for="edit_name" class="form-label ms-1">Name</label>
                        <input type="text" id="edit_name" name="edit_name" class="form-control" aria-label="edit_name" readonly>
                    </div>

                    <div class="col mb-3">
                        <label for="edit_position" class="form-label ms-1">Position</label>
                        <input type="text" id="edit_position" name="edit_position" class="form-control" aria-label="edit_position" required>
                    </div>
                    <div class="col">
                        <label for="edit_contactNumber" class="form-label ms-1">Contact Number</label>
                        <input type="text" id="edit_contactNumber" name="edit_contactNumber" class="form-control" aria-label="edit_contactNumber" required>
                    </div>
    
                    {{-- Hidden tag for data storing only --}}
                    <input id="worker_ID" name="worker_ID" class="visually-hidden">

                    <div class="modal-footer row mt-5">
                        <div class="col p-0">
                            <button type="button" id="Trash_Worker_Record_Btn" class="btn btn-outline-danger Trash_Worker_Record_Btn" data-bs-toggle="modal" data-bs-target="#DeleteConfirmationModal">
                                Trash
                            </button>
                        </div>
                        <div class="col d-flex flex-row-reverse p-0">
                            <button type="submit" class="btn btn-primary text-white me-4 w-24">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
        </div>
    </div>
    <!-- ======================== END Edit Worker Modal ======================== -->


    <!-- --------------------- Trash Worker Modal ------------------------- -->
    <div class="modal fade" id="DeleteConfirmationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0">
            <div class="modal-header bg-gray-800 text-white">
                <h5 class="modal-title" id="staticBackdropLabel">Trash Worker Info?</h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('trashWorker') }}" method="GET" autocomplete="off">
                    @csrf
                    
                    <div class="col pt-2">
                        <label for="confirmation_password" class="form-label ms-1">Please enter password to continue delete worker info</label>
                        <input type="password" id="confirmation_password" name="confirmation_password" class="form-control" aria-label="confirmation_password" required>
                    </div>

                    {{-- Hidden tag for data storing only --}}
                    <input id="worker_ID2" name="worker_ID2" class="visually-hidden">

                    <div class="modal-footer row mt-4">
                        <div class="col d-flex flex-row-reverse p-0">
                            <button type="submit" class="btn btn-danger text-white me-4 w-24">Continue</button>
                        </div>
                    </div>
                </form>
            
            </div>
        </div>
        </div>
    </div>
    <!-- ======================== END Trash Worker Modal ======================== -->



<script>

    $(document).ready(function(){


        // ---------------------------------- Show Suggestion when searching function ----------------------------------
        const searchedResident = document.getElementById('searchedToAdd');
        const suggestions = document.getElementById('suggested_on_search');

        const searchProfile = async searchText => {

            var residence_info = {!! json_encode($resident_infos, JSON_HEX_TAG) !!};

            let matches = residence_info.filter(person => {
                const regex = new RegExp(`^${searchText}`, 'gi');
                return person.first_name.match(regex) || person.last_name.match(regex);
            })

            if (searchText.length === 0){
                matches = [];
                suggestions.innerHTML = '';
            }

            showSuggestions(matches);

        };

        const showSuggestions = matches =>{
            if(matches.length > 0){
                const html = matches.map(match =>`
                <tr class="bg-gray-800 text-white">
                    <td class="align-middle whitespace-nowrap border-end-0">
                        ${match.first_name} ${match.last_name}
                    </td>
                    <td class="w-16 border-start-0">
                        <button type="button" value='${match.id}-${match.first_name}-${match.last_name}-${match.brgy_id}' id="addBTN" class="text-white btn btn-primary addBTN" data-bs-toggle="modal" data-bs-target="#AddWorker">
                            Add
                        </button>
                    </td>
                </tr>
                `).join('');

                suggestions.innerHTML = html;
            }
        }

        searchedResident.addEventListener('input', () => searchProfile(searchedResident.value));
        // ===================================== END Show Suggestion when searching function =====================================


        // ---------------------- Add Chosen Profile --------------------- 
        $(document).on('click','.addBTN', function(event){
            //alert(this.value);
            let btnValue = this.value;
            let myArr = btnValue.split("-");
            let residentID = myArr[0];
            let residentFirstName = myArr[1];
            let residentLastName = myArr[2];
            let residentBrgyID = myArr[3];
            document.getElementById('name').value = residentFirstName + ' ' + residentLastName;
            document.getElementById('FirstName').value = residentFirstName;
            document.getElementById('LastName').value = residentLastName;
            document.getElementById('ResidentID').value = residentID;
            document.getElementById('BarangayID').value = residentBrgyID;
        });
        // =================== END Add Chosen Profile =================== 


        // ---------------------- Edit Chosen Profile --------------------- 
        $(document).on('click','.Edit_Worker_Btn', function(event){
            let value_holder = this.value.split('-');
            document.getElementById('worker_ID').value = value_holder[0];
            document.getElementById('worker_ID2').value = value_holder[0];
            document.getElementById('edit_name').value = value_holder[1]+ ' ' + value_holder[2];
            document.getElementById('edit_position').value = value_holder[3];
            document.getElementById('edit_contactNumber').value = value_holder[4];
        });
        // =================== END Edit Chosen Profile =================== 


        // ---------------------- Trash Chosen Profile --------------------- 
        $(document).on('click','.Trash_Worker_Record_Btn', function(event){
            $('#EditWorkerModal').modal('hide');
        });
        // =================== END Add Chosen Profile =================== 



    });
    

</script>

@endsection