@extends('layouts.app')

@section('content')
    
    <div class="d-block">
        <div class="w-100 pt-2 pb-3 font-semibold text-sm text-gray-600">
            Report Summary
        </div>
        <div class="dashboard-content">
            <div>
                <div class="population-div">
                    <p class="text-3xl font-semibold">Population</p>
                    <p class="text-gray-600 text-sm m-0">{{ $residents }} Residence</p>
                </div>
                <hr class="mb-4">
                <div class="p-1 mb-5">
                    <div class="d-flex">
                        <div class="w-50p">
                            <p class="text-gray-600 text-sm">Male</p>
                        </div>
                        <div class="w-50p d-flex justify-content-end">
                            <p class="text-gray-600 text-sm">{{ number_format(($male/$residents)*100) }}%</p>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar population-bar-color" style="width: {{ number_format(($male/$residents)*100) }}%" aria-valuenow="{{ number_format(($male/$residents)*100) }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="p-1 mb-5">
                    <div class="d-flex">
                        <div class="w-50p">
                            <p class="text-gray-600 text-sm">Female</p>
                        </div>
                        <div class="w-50p d-flex justify-content-end">
                            <p class="text-gray-600 text-sm">{{ number_format(($female/$residents)*100) }}%</p>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar population-bar-color" style="width: {{ number_format(($female/$residents)*100) }}%" aria-valuenow="{{ number_format(($female/$residents)*100) }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="p-1 mb-2">
                    <div class="d-flex">
                        <div class="w-50p">
                            <p class="text-gray-600 text-sm">Undefined</p>
                        </div>
                        <div class="w-50p d-flex justify-content-end">
                            <p class="text-gray-600 text-sm">{{ number_format(($undifined/$residents)*100) }}%</p>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar population-bar-color" style="width: {{ number_format(($undifined/$residents)*100) }}%" aria-valuenow="{{ number_format(($undifined/$residents)*100) }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div>
                <div class="household-div">
                    <p class="text-3xl font-semibold">Household</p>
                    <p class="text-gray-600 text-sm m-0">Recently Added</p>
                </div>
                <hr class="mb-4">
                <div class="p-1 mb-2">
                    <table id="persons_table" class="w-100 table table-hover table-responsive border">
                        <thead class="bg-gray-800 text-white">
                            <tr>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    House Head
                                </th>
                                <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                    Address
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white " >
                            @foreach ($households as $household)
                                <tr>
                                    <td class="px-3 align-middle">{{ $household->first_name.' '.$household->last_name }}</td>
                                    <td class="px-3 align-middle">{{ 'Purok '.$household->name.', '.$household->brgy_name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if ($households->isEmpty())
                        <div class="m-5 text-center text-gray-600 font-bold">
                            No data available.
                        </div>
                    @endif
                </div>
            </div>

            <div>
                <div class="mortality-div">
                    <p class="text-3xl font-semibold">Mortality Rate</p>
                    <p class="text-gray-600 text-sm m-0">Death rate in the last 6 months</p>
                </div>
                <hr class="mb-4">
                <div class="p-1 mb-2">
                    <canvas id="mortality_chart" height="250"></canvas>
                </div>
            </div>

        </div>
    </div>

  
    <script>

        // ------------------------ Get previous 6 months range ------------------------ 
        let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let today = new Date();
        let d;
        let month;
        let month_range = [];

        for(var i = 6; i > 0; i -= 1) {
            d = new Date(today.getFullYear(), today.getMonth() - i, 1);
            month = monthNames[d.getMonth() + 1];
            month_range.push(month);
        }
        // ======================= END Get previous 6 months range =======================

        

        // ------------------------ Mortality Chart Scripts ------------------------
        let residence_info = {!! json_encode($deathPerMonth, JSON_HEX_TAG) !!};
        var ctx = document.getElementById('mortality_chart').getContext('2d');
        var mortality_chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: month_range,
                datasets: [{
                    label: 'Deaths per Month',
                    data: residence_info,
                    backgroundColor: 'rgba(44, 60, 91, 0.2)',
                    borderColor: 'rgba(44, 60, 91, 1)',
                    borderWidth: 2,
                    lineTension: 0.4,
                    pointBackgroundColor: 'rgba(255, 255, 255, 1)',
                    pointStyle: 'rectRounded'
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            },
            
        });
        // ======================= END Mortality Chart Scripts =======================


    </script>
    
@endsection