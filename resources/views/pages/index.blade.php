@extends('layouts.app')

@section('content')

    @auth
        <div class="block px-10">
            <div class="bg-white p-6 rounded-lg">
                Home Page
            </div>
        </div>
    @endauth
    
    @guest
        <div class="d-flex justify-content-center align-items-center h-128">
            <div class="login-left-box w-50p bg-white h-100">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="mb-4">
                        <p class="text-4xl font-semibold mb-2">Login</p>
                        <p class="text-md text-gray-600">Sign In to your account</p>
                    </div>

                    @if (session('status'))
                        <p class="text-sm text-white text-center font-medium bg-red-400 rounded p-3 mb-3">{{ session('status') }}</p>
                    @endif

                    <div class="mb-3">
                        <label for="username" class="sr-only">Username</label>
                        <input type="text" name="username" id="username" placeholder="Username" class="form-control w-100 p-3 @error('username') border border-danger  @enderror" value="{{ old('username') }}">

                        @error('username')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="password" class="sr-only">Password</label>
                        <input type="password" name="password" id="password" placeholder="Password" class="form-control w-100 p-3 @error('password') border border-danger  @enderror" value="">

                        @error('password')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-2 d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary text-white px-5 rounded font-medium">Login</button>

                        <a href="" class="py-3 text-blue-500">Forgot password?</a>
                    </div>
                </form>
            </div>
            <div class="w-50p login-righ-box bg-blue-400 h-100">
                <img src="{{URL('img/login-logo.png')}}" alt="Brangay Health Records thumbnail" class="w-100">
            </div>
        </div>
    @endguest
@endsection