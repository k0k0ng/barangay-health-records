@extends('layouts.app')

@section('content')
    
    <div class="h-full p-4 mb-5 bg-white rounded">            
        <div class="d-flex justify-content-between">
            <div class="font-semibold text-sm text-gray-600 p-1">
                List of residents
            </div>
            <form method="POST" action="{{ route('searchResidentProfile') }}" role="search" class="search-resident-form d-flex w-30p mb-3 px-2" autocomplete="off">
                @csrf
                <input type="search" name="term" id="term" class="form-control" placeholder="Search Resident" aria-label="Search Resident" aria-describedby="search-name-btn">
                <button class="btn btn-primary text-white" type="submit" id="search-name-btn">Search</button>
            </form>
        </div>

        <div class="d-block overflow-auto">
            <table id="persons_table" class="w-100 table table-bordered table-hover table-responsive">
                <thead class="bg-gray-800 text-white">
                    <tr>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Barangay ID
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Firstname
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Middle Initial
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            Lastname
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            @sortablelink('Gender')
                        </th>
                        <th scope="col" class="p-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                            @sortablelink('Address')
                        </th>
                        <th scope="col" class="p-3 text-center text-xs font-medium text-gray-500 tracking-wider">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @php
                        $i = 0;
                    @endphp
                    
                    @foreach ($persons as $person)
                        @php
                            $i++;
                        @endphp
                        <form id="ViewProfileForm" method="GET" action="{{ route('ViewResidentProfile') }}" role="View Resident Profile">
                            <input class="visually-hidden" id="residentProfileSelectedID" name="residentProfileSelectedID" @php echo "value='".$person->id."'"; @endphp >
                            <tr @php echo "id='".$person->id."'"; @endphp>
                                <td class="px-3 align-middle text-nowrap">{{ $person->citizen_brgy_id }}</td>
                                <td class="px-3 align-middle text-nowrap">{{ $person->first_name }}</td>
                                <td class="px-3 align-middle text-nowrap">{{ $person->middle_name[0] }}.</td>
                                <td class="px-3 align-middle text-nowrap">{{ $person->last_name }}</td>
                                <td class="px-3 align-middle">{{ $person->gender }}</td>
                                <td class="px-3 align-middle text-sm text-gray-500 text-nowrap">{{ $person->address }}</td>
                                <td class="d-flex justify-content-center text-sm font-medium">
                                    <button type="submit" class='btn btn-primary ViewResidentProfileBtn'>
                                        View
                                    </button>
                                </td>
                            </tr>
                        </form>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="mt-3 d-flex justify-content-evenly">
            {!! $persons->appends(\Request::except('page'))->render() !!}
        </div>

        @if ($persons->isEmpty())
            <div class="m-5 text-center text-gray-600 font-bold">
                No data available.
            </div>
        @endif
    </div>

@endsection