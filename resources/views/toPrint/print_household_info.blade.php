<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To Print Resident Medical Records</title>

    <style>
        body
        {
            font-family: 'Helvetica', 'Arial', sans-serif;
            color: #444444;
            font-size: 12pt;
        }

        body .main_div{
            display: flex;
            justify-content: center;
            height: 100%;
        }

        .header_p{
            font-size: 28px;
            padding: 0px;
            margin: 0px;
            margin-bottom: 1.2rem;
        }

        .household_table{
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            border-collapse: collapse;
        }

        .household_table thead {
            color: white;
            background-color: #343a40;
        }

        .household_table thead tr th {
            padding-top: 18px;
            padding-bottom: 18px;
            border:1px solid white;
            border-top: none;
        }

        .household_table thead tr th:first-child {
            border-left:1px solid #343a40;
        }

        .household_table thead tr th:last-child {
            border-right:1px solid #343a40;
        }

        .household_table tbody{
            background-color: white;
        }

        .household_table tbody tr td{
            padding: 15px;
            border: 1px solid #c2c2c2;
            text-align: center;
        }

        .information_div{
            display: flex;
            
            padding-left: 10px;
        }

        .information_div div p{
            color: #646464;
            font-weight: 600;
            font-size: 12pt;
        }

        .information_div div p span{
            color: #343a40;
            font-weight: 500;
            font-size: 16pt;
        }
    </style>


</head>
<body>
    <div class="main_div">

            <div>
                <h3>Household Info</h3>
            </div>
            <div class="information_div">
             
                <div>
                    <p class="text-gray-600 text-xs font-semibold">Address: <span id="Age" class="text-lg font-medium text-gray-800 ms-1">Purok {{ $household_infos[0]->purok_name.', '.$household_infos[0]->brgy_name.', '.$household_infos[0]->district }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Date profiled: <span id="Birthday" class="text-lg font-medium text-gray-800 ms-1">{{ date("M d, Y", strtotime($household_infos[0]->date_profiled)) }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Midwife: <span id="Gender" class="text-lg font-medium text-gray-800 ms-1">{{ $midwife_info->firstname.' '.$midwife_info->lastname }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Interviewed by: <span id="Height" class="text-lg font-medium text-gray-800 ms-1">{{ $interviewer_info->firstname.' '.$interviewer_info->lastname }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Encoder: <span id="Weight" class="text-lg font-medium text-gray-800 ms-1">{{ $encoder_info->first_name.' '.$encoder_info->last_name }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Philhealth No.: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->philhealth_no }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">CCT: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->cct }}</span> </p>
                </div>

                <div>
                    <p class="text-gray-600 text-xs font-semibold">Sanitation Score: <span id="Cell_No" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->score }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Water source: 
                        <span id="Email" class="text-lg font-medium text-gray-800 ms-1">
                            @php
                                $counter = 0;
                            @endphp
                            @if ($household_infos[0]->community_water == "Yes")
                                Community Water
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->developed_spring == "Yes")
                                @if ($counter>0) - @endif
                                Developed Spring
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->protected_well == "Yes")
                                @if ($counter>0) - @endif
                                Protected Well
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->truck_tanker_peddler == "Yes")
                                @if ($counter>0) - @endif
                                Truck/Tanker Peddler
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->bottled_water == "Yes")
                                @if ($counter>0) - @endif
                                Bottled Water
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->undeveloped_spring == "Yes")
                                @if ($counter>0) - @endif
                                Undeveloped Spring
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->undeveloped_well == "Yes")
                                @if ($counter>0) - @endif
                                Undeveloped Well
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->rainwater == "Yes")
                                @if ($counter>0) - @endif
                                Rainwater
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->river_stream_dam == "Yes")
                                @if ($counter>0) - @endif
                                River/Stream/Dam
                            @endif
                        </span>
                    <p class="text-gray-600 text-xs font-semibold">Toilet: 
                        <span id="Tell_No" class="text-lg font-medium text-gray-800 ms-1">
                            @php
                                $counter = 0;
                            @endphp
                            @if ($household_infos[0]->flush_toilet == "Yes")
                                Water Flush Toilet
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->closed_pit_pervy == "Yes")
                                @if ($counter>0) - @endif
                                Closed Pit Pervy
                                @php $counter++; @endphp
                            @endif
                            @if($household_infos[0]->communal_toilet == "Yes")
                                @if ($counter>0) - @endif
                                Communal Toilet
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->drop_overhung == "Yes")
                                @if ($counter>0) - @endif
                                Drop/Overhung
                                @php $counter++; @endphp
                            @endif
                            
                            @if($household_infos[0]->field_bodyOfWater == "Yes")
                                @if ($counter>0) - @endif
                                Field/Body of Water
                                @php $counter++; @endphp
                            @endif
                        </span> </p>
                    <p class="text-gray-600 text-xs font-semibold">NHTS: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->nhts }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">NHTS No.: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->nhts_no }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">IP: <span id="Religion" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->ip }}</span> </p>
                    <p class="text-gray-600 text-xs font-semibold">Tribe: <span id="Blood_Type" class="text-lg font-medium text-gray-800 ms-1">{{ $household_infos[0]->tribe }}</span> </p>
                </div>
                
            </div>
    
            <div style="margin-top: 3rem">
                <h3>Household Members</h3>
            </div>
            <div class="px-4">
                <table id="household_table" class="household_table">
                    <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Occupation
                        </th>
                        <th>
                            Gender
                        </th>
                        <th>
                            Role
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($household_infos as $household_info)
                        <tr @php echo "id='".$household_info->id."'"; @endphp>
                            <td class="px-3 align-middle whitespace-nowrap border-0 border-start">{{ $household_info->last_name.', '.$household_info->first_name.' '.$household_info->middle_name }}</td>
                            <td class="px-3 align-middle border-0">{{ $household_info->occupation }}</td>
                            <td class="px-3 align-middle border-0">{{ $household_info->gender }}</td>
                            <td class="px-3 align-middle border-0">{{ $household_info->relationship }}
                                @if ($household_info->family_head == "Yes")
                                    (Head)
                                @endif
                            </td>
                        </tr>
                    @endforeach   
                    </tbody>
                </table>
            </div>
    
   
    </div>
</body>
</html>