
@if ( isset($type) )
    <p class="header_p">{{ $type.' Record(s): '.$resident->first_name.' '.$resident->last_name }}</p>
@else
    <p class="header_p">{{ 'Medical Records Log: '.$resident->first_name.' '.$resident->last_name }}</p>
@endif

