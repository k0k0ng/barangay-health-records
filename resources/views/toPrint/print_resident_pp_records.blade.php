<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To Print Resident Medical Records</title>

    <style>
        body
        {
            font-family: 'Helvetica', 'Arial', sans-serif;
            color: #444444;
            font-size: 12pt;
        }

        body .main_div{
            display: flex;
            justify-content: center;
            height: 100%;
        }

        .header_p{
            font-size: 28px;
            padding: 0px;
            margin: 0px;
            margin-bottom: 1.2rem;
        }

        .ressidents_records_table{
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            border-collapse: collapse;
        }

        .ressidents_records_table thead {
            color: white;
            background-color: #343a40;
        }

        .ressidents_records_table thead tr th {
            padding-top: 18px;
            padding-bottom: 18px;
            border:1px solid white;
            border-top: none;
        }

        .ressidents_records_table thead tr th:first-child {
            border-left:1px solid #343a40;
        }

        .ressidents_records_table thead tr th:last-child {
            border-right:1px solid #343a40;
        }

        .ressidents_records_table tbody{
            background-color: white;
        }

        .ressidents_records_table tbody tr td{
            padding: 15px;
            border: 1px solid #c2c2c2;
            text-align: center;
        }
 
    </style>


</head>
<body>
    <div class="main_div">
        @include('toPrint._header_resident_medical_records')
        <table id="ressidents_records_table" class="ressidents_records_table">
            <thead>
            <tr>
                <th>
                    Age
                </th>
                <th>
                    Weight
                </th>
                <th>
                    Gender
                </th>
                <th>
                    Attended by
                </th>
                <th>
                    Place of Delivery
                </th>
                <th>
                    FDG
                </th>
                <th>
                    Date of PP
                </th>
                <th>
                    Vitamina
                </th>
                <th>
                    DOD
                </th>
                <th>
                    F
                </th>
                <th>
                    Date Created
                </th>
                <th>
                    Last Date Updated
                </th>
            </tr>
            </thead>
            <tbody>
                @if (!empty($all_records))
                    @foreach ($all_records as $record)
                        <tr>
                            <td>{{ $record['age'] }}</td>
                            <td>{{ $record['weight'] }}</td>
                            <td>{{ $record['gender'] }}</td>
                            <td>{{ $record['attended_by'] }}</td>
                            <td>{{ $record["PlaceOfDelivery"] }}</td>
                            <td>{{ $record["fdg"] }}</td>
                            <td>{{ date('M d, Y', strtotime($record["date_of_pp"])) }}</td>
                            <td>{{ date('M d, Y', strtotime($record["vitamina"])) }}</td>
                            <td>{{ date('M d, Y', strtotime($record["dod"])) }}</td>
                            <td>{{ $record["F"] }}</td>
                            <td>{{ date('M d, Y', strtotime($record["created_at"])) }}</td>
                            <td>{{ date('M d, Y', strtotime($record["updated_at"])) }}</td>
                        </tr>
                    @endforeach                                    
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>