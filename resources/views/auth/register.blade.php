@extends('layouts.app')

@section('content')

    <div class="w-30p bg-white p-5 rounded">
        <form action="{{ route('register') }}" method="POST">
            @csrf
            <div class="mb-4 d-flex justify-content-center">
                <h1 class="text-lg font-bold">Create a BHRS account</h1>
            </div>

            <div class="mb-4">
                <label for="username" class="sr-only">Username</label>
                <input type="text" name="username" id="username" placeholder="Username" class="form-control w-100 p-3 @error('username') border border-danger  @enderror" value="{{ old('username') }}">

                @error('username')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="mb-4">
                <label for="password" class="sr-only">Password</label>
                <input type="password" name="password" id="password" placeholder="Password" class="form-control w-100 p-3 @error('password') border border-danger  @enderror" value="">

                @error('password')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="mb-5">
                <label for="password_confirmation" class="sr-only">Repeat Password</label>
                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Repeat Password" class="form-control w-100 p-3" value="">
            </div>

            <div class="mb-2">
                <button type="submit" class="btn btn-primary text-white py-3 rounded font-medium w-100">Register</button>
            </div>
        </form>
    </div>
    
@endsection